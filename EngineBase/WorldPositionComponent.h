#pragma once

#include "Component.h"

#include <DirectXMath.h>

namespace core
{
	class WorldPositionComponent : public Component
	{
	public:
		WorldPositionComponent(EntityId id);
		~WorldPositionComponent();

		DirectX::XMFLOAT3&	getPosition();

	private:
		DirectX::XMFLOAT3	m_position;
	};
}
