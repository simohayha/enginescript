#include "IdentityComponent.h"
#include "EngineData.hpp"
#include "GameObject.h"

core::GameObject::GameObject()
{
	setAssetType(E_GAMEOBjECT);
}

core::GameObject::~GameObject()
{
}

void core::GameObject::encodeAsset(std::ofstream& stream) const
{
	YAML::Emitter	out;

	out << YAML::BeginDoc;

	out << YAML::BeginMap;
	out << YAML::Key << "GameObject";
	out << YAML::BeginMap;

	out << YAML::Key << "m_fileId" << YAML::Value << getFileId();
	out << YAML::Key << "m_type" << YAML::Value << getAssetType();

	std::vector<Component*>	components;
	EngineData::getAllComponents(components, getId());

	out << YAML::Key << "Components";
	out << YAML::BeginSeq;
	for (auto& component : components)
	{
		out << YAML::BeginMap;
		out << YAML::Key << "Component";
		out << YAML::Value << component->getFileId();
		out << YAML::EndMap;
	}
	out << YAML::EndSeq;
	out << YAML::EndMap;
	out << YAML::EndMap;

	out << YAML::Newline;

	stream << out.c_str();

	for (auto& component : components)
	{
		component->encodeAsset(stream);
	}
}

void core::GameObject::decodeAsset(YAML::Node& node)
{
	m_fileId = node["m_fileId"].as<unsigned long long>();
	m_assetType = static_cast<AssetType>(node["m_type"].as<int>());
}

//void core::GameObject::decode(YAML::Node node)
//{
//}

std::string core::GameObject::getName() const
{
	auto	identity = EngineData::getComponent<IdentityComponent>(getId());
	if (identity)
	{
		return identity->getName();
	}
	return "";
}

void core::GameObject::setName(const std::string& name)
{
	auto	identity = EngineData::getComponent<IdentityComponent>(getId());
	if (identity)
	{
		identity->setName(name);
	}
}
