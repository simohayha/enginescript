#pragma once

#include "mono/metadata/object.h"

namespace core
{
	void Debug_Log(int logType, MonoString* message, void* object);
}