#include <DirectXColors.h>

#include "TransformComponent.h"
#include "EngineData.hpp"
#include "CameraComponent.h"

core::CameraComponent::CameraComponent(EntityId id) :
	Component(id),
	m_aspect(0.0f),
	m_near(0.0f),
	m_far(0.0f),
	m_fov(0.0f),
	m_backgroundColor(DirectX::Colors::MidnightBlue)
{
	setAssetType(E_CAMERA);
}

core::CameraComponent::~CameraComponent()
{
}

void core::CameraComponent::getViewMatrix(DirectX::SimpleMath::Matrix& matrix)
{
	auto	transform = EngineData::getComponent<TransformComponent>(getOwner());

	DirectX::SimpleMath::Vector3	position;
	DirectX::SimpleMath::Vector3	lookAt;
	DirectX::SimpleMath::Vector3	up;

	if (transform)
	{
		up = DirectX::SimpleMath::Vector3(0.0f, 1.0f, 0.0f);

		position = transform->getPosition();

		lookAt = DirectX::SimpleMath::Vector3(0.0f, 0.0f, -1.0f);

		auto	rotationMatrix = DirectX::SimpleMath::Matrix::CreateFromQuaternion(transform->getRotation());

		lookAt = DirectX::SimpleMath::Vector3::Transform(lookAt, rotationMatrix);
		up = DirectX::SimpleMath::Vector3::Transform(up, rotationMatrix);

		lookAt = position + lookAt;
	}

	m_view = DirectX::SimpleMath::Matrix::CreateLookAt(position, lookAt, up);
	matrix = m_view;
}

void core::CameraComponent::getProjectionMatrix(DirectX::SimpleMath::Matrix& matrix)
{
	m_projection = DirectX::XMMatrixPerspectiveFovLH(m_fov, m_aspect, m_near, m_far);
	matrix = m_projection;
}

void core::CameraComponent::setAspect(float aspect)
{
	m_aspect = aspect;
}

float core::CameraComponent::getAspect() const
{
	return m_aspect;
}

void core::CameraComponent::setNear(float cameraNear)
{
	m_near = cameraNear;
}

float core::CameraComponent::getNear() const
{
	return m_near;
}

void core::CameraComponent::setFar(float cameraFar)
{
	m_far = cameraFar;
}

float core::CameraComponent::getFar() const
{
	return m_far;
}

void core::CameraComponent::setFov(float fov)
{
	m_fov = fov;
}

float core::CameraComponent::getFov() const
{
	return m_fov;
}

void core::CameraComponent::setBackgroundColor(DirectX::SimpleMath::Color color)
{
	m_backgroundColor = color;
}

DirectX::SimpleMath::Color core::CameraComponent::getBackgroundColor() const
{
	return m_backgroundColor;
}

void core::CameraComponent::encodeAsset(std::ofstream& stream) const
{
	YAML::Emitter	out;

	out << YAML::BeginDoc;

	out << YAML::BeginMap;
	out << YAML::Key << "Camera";

	out << YAML::BeginMap;
	out << YAML::Key << "m_fileId" << YAML::Value << getFileId();
	out << YAML::Key << "m_type" << YAML::Value << getAssetType();
	out << YAML::Key << "m_gameObject" << YAML::Value << EngineData::getObject(getOwner())->getFileId();

	out << YAML::Key << "m_aspect" << YAML::Value << m_aspect;
	out << YAML::Key << "m_near" << YAML::Value << m_near;
	out << YAML::Key << "m_far" << YAML::Value << m_far;
	out << YAML::Key << "m_fov" << YAML::Value << m_fov;
	out << YAML::Key << "m_backgroundColor";
	out << YAML::BeginMap;
	out << YAML::Key << "r" << YAML::Value << m_backgroundColor.R();
	out << YAML::Key << "g" << YAML::Value << m_backgroundColor.G();
	out << YAML::Key << "b" << YAML::Value << m_backgroundColor.B();
	out << YAML::Key << "a" << YAML::Value << m_backgroundColor.A();
	out << YAML::EndMap;

	out << YAML::EndMap;
	out << YAML::EndMap;

	out << YAML::Newline;

	stream << out.c_str();
}

void core::CameraComponent::decodeAsset(YAML::Node& node)
{
}
