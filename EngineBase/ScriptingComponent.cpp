#include <string>

#include "MonoScript.h"

#include "Log.h"
#include "Scripting.h"
#include "ScriptingComponent.h"

core::ScriptingComponent::ScriptingComponent(EntityId id, Scripting* scripting) :
	Component(id),
	m_scripting(scripting),
	m_gameObject(nullptr)
{
}

core::ScriptingComponent::~ScriptingComponent()
{
}

bool core::ScriptingComponent::initialize()
{
	MonoClass*	klass = mono_class_from_name(m_scripting->getImage("Scripting.dll"), "Scripting", "GameObject");
	if (!klass)
	{
		return false;
	}

	m_gameObject = mono_object_new(m_scripting->getDomain(), klass);
	if (!m_gameObject)
	{
		return false;
	}
	mono_gchandle_new(m_gameObject, false);

	MonoClassField*	field = mono_class_get_field_from_name(klass, "instanceID");
	if (!field)
	{
		return false;
	}
	mono_field_set_value(m_gameObject, field, &m_owner);

	// TODO Dunno if I should make this work or not
	mono_runtime_object_init(m_gameObject);

	return true;
}

bool core::ScriptingComponent::attachMonoScript(const std::string& script)
{
	MonoClass*	klass = mono_class_from_name(m_scripting->getImage("Assembly-CSharp.dll"), "Assembly_CSharp", script.c_str());
	if (!klass)
	{
		return false;
	}

	MonoObject*	managedScript = mono_object_new(m_scripting->getDomain(), klass);
	if (!managedScript)
	{
		return false;
	}
	mono_gchandle_new(managedScript, false);

	MonoScript*	monoScript = new(std::nothrow) MonoScript(klass, managedScript, m_gameObject);
	if (!monoScript)
	{
		return false;
	}

	if (!monoScript->initialize())
	{
		return false;
	}

	m_scripts.push_back(monoScript);

	MonoClassField*	field = mono_class_get_field_from_name(klass, "instanceID");
	if (!field)
	{
		return false;
	}
	mono_field_set_value(managedScript, field, &m_owner);

	mono_runtime_object_init(managedScript);

	return true;
}

void core::ScriptingComponent::callStart()
{
	for (auto& script : m_scripts)
	{
		script->start();
	}
}

void core::ScriptingComponent::callUpdate()
{
	for (auto& script : m_scripts)
	{
		script->update();
	}
}

MonoObject* core::ScriptingComponent::getGameObject() const
{
	return m_gameObject;
}

void core::ScriptingComponent::encodeAsset(std::ofstream& stream) const
{
}

void core::ScriptingComponent::decodeAsset(std::vector<YAML::Node>& nodes)
{
}
