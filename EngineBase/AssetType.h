#pragma once

namespace core
{
	enum AssetType
	{
		E_OBJECT,
		E_GAMEOBjECT,
		E_COMPONENT,
		E_ASSET,
		E_TEXTURE,
		E_MONOSCRIPT,
		E_DIRECTORY,
		E_MATERIAL,
		E_TEXTASSET,
		E_SHADER,
		E_SCENE,
		E_TRANSFORM,
		E_RENDERER,
		E_CAMERA,
		E_IDENTITY,
		E_MESHFILTER,
	};

	static const char* AssetTypeName[] =
	{
		"Object",
		"GameObject",
		"Component",
		"Asset",
		"Texture",
		"MonoScript",
		"Directory",
		"Material",
		"Text Asset",
		"Shader",
		"Scene",
		"Transform",
		"Renderer",
		"Camera",
		"Identity",
		"Mesh Filter",
	};
}