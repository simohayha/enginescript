#include "Scripting.h"
#include "Log.h"
#include "EngineData.hpp"
#include "TransformComponent.h"

extern core::Scripting*	instance;

int core::TransformComponent::Id()
{
	return 2;
}

core::TransformComponent::TransformComponent(EntityId id) :
	Component(id),
	m_position(0.0f, 0.0f, 0.0f),
	m_rotation(0.0f, 0.0f, 0.0f, 1.0f),
	m_eulerAngle(0.0f, 0.0f, 0.0f),
	m_scale(1.0f, 1.0f, 1.0f)
{
	setAssetType(E_TRANSFORM);
}

core::TransformComponent::~TransformComponent()
{
}

core::Vector3& core::TransformComponent::getPosition()
{
	return m_position;
}

core::Quaternion& core::TransformComponent::getRotation()
{
	return m_rotation;
}

core::Vector3 core::TransformComponent::getEulerAngle()
{
	Vector3	eulerAngle;
	m_rotation.toEulerAngle(eulerAngle);
	return eulerAngle;
}

core::Vector3& core::TransformComponent::getScale()
{
	return m_scale;
}

void core::TransformComponent::addChild(int childId)
{
	m_childs.push_back(childId);
}

void core::TransformComponent::removeChild(int childId)
{
	m_childs.remove(childId);
}

void core::TransformComponent::setParent(int parentId)
{
	auto	parent = EngineData::getComponent<TransformComponent>(getParent());

	if (parent)
	{
		parent->removeChild(m_owner);
	}

	parent = EngineData::getComponent<TransformComponent>(parentId);
	if (parent)
	{
		parent->addChild(m_owner);
	}

	m_parent = parentId;
}

std::list<int>& core::TransformComponent::getChilds()
{
	return m_childs;
}

const std::list<int>& core::TransformComponent::getChilds() const
{
	return m_childs;
}

int core::TransformComponent::getParent() const
{
	return m_parent;
}

void core::TransformComponent::encodeAsset(std::ofstream& stream) const
{
	YAML::Emitter	out;

	out << YAML::BeginDoc;

	out << YAML::BeginMap;
	out << YAML::Key << "Transform";
	out << YAML::BeginMap;

	out << YAML::Key << "m_fileId" << YAML::Value << getFileId();
	out << YAML::Key << "m_type" << YAML::Value << getAssetType();
	out << YAML::Key << "m_gameObject" << YAML::Value << EngineData::getObject(getOwner())->getFileId();

	out << YAML::Key << "m_localPosition";
	out << YAML::BeginMap;
	out << YAML::Key << "x" << YAML::Value << m_position.x;
	out << YAML::Key << "y" << YAML::Value << m_position.y;
	out << YAML::Key << "z" << YAML::Value << m_position.z;
	out << YAML::EndMap;

	out << YAML::Key << "m_localRotation";
	out << YAML::BeginMap;
	out << YAML::Key << "x" << YAML::Value << m_rotation.x;
	out << YAML::Key << "y" << YAML::Value << m_rotation.y;
	out << YAML::Key << "z" << YAML::Value << m_rotation.z;
	out << YAML::Key << "w" << YAML::Value << m_rotation.w;
	out << YAML::EndMap;

	out << YAML::Key << "m_localScale";
	out << YAML::BeginMap;
	out << YAML::Key << "x" << YAML::Value << m_scale.x;
	out << YAML::Key << "y" << YAML::Value << m_scale.y;
	out << YAML::Key << "z" << YAML::Value << m_scale.z;
	out << YAML::EndMap;

	out << YAML::Key << "m_children";
	out << YAML::BeginSeq;
	for (auto child : m_childs)
	{
		out << YAML::BeginMap;
		out << YAML::Key << "id";
		out << YAML::Value << child;
		out << YAML::EndMap;
	}
	out << YAML::EndSeq;

	out << YAML::Key << "m_father";
	out << YAML::Value << m_parent;

	out << YAML::EndMap;
	out << YAML::EndMap;
	out << YAML::Newline;

	stream << out.c_str();
}

void core::TransformComponent::decodeAsset(YAML::Node& node)
{
}

void core::TransformComponent::getLocalTransformMatrix(Matrix& transform)
{
	auto	translation = DirectX::SimpleMath::Matrix::CreateTranslation(getPosition());
	auto	rotation = DirectX::SimpleMath::Matrix::CreateFromQuaternion(getRotation());
	auto	scale = DirectX::SimpleMath::Matrix::CreateScale(getScale());

	transform = rotation * translation * scale;
}

void core::TransformComponent::getWorldTransformMatrix(Matrix& transform)
{
	Matrix	tmp;

	getLocalTransformMatrix(tmp);
	
	transform *= tmp;

	auto	parent = EngineData::getComponent<TransformComponent>(getParent());
	if (parent)
	{
		parent->getWorldTransformMatrix(transform);
	}
}

void core::scripting::INTERNAL_get_position(MonoObject* thisObject, Vector3* position)
{
	MonoClassField*	field = mono_class_get_field_from_name(instance->getClass("Transform"), "instanceID");
	if (!field)
	{
		return;
	}

	int id = 0;
	mono_field_get_value(thisObject, field, &id);

	auto	transform = EngineData::getComponent<TransformComponent>(id);
	if (transform)
	{
		*position = transform->getPosition();
	}
}

void core::scripting::INTERNAL_set_position(MonoObject* thisObject, Vector3* position)
{
	MonoClassField*	field = mono_class_get_field_from_name(instance->getClass("Transform"), "instanceID");
	if (!field)
	{
		return;
	}

	int id = 0;
	mono_field_get_value(thisObject, field, &id);

	auto	transform = EngineData::getComponent<TransformComponent>(id);
	if (transform)
	{
		transform->getPosition() = *position;
	}
}

void core::scripting::INTERNAL_get_rotation(MonoObject* thisObject, Quaternion* rotation)
{
	MonoClassField*	field = mono_class_get_field_from_name(instance->getClass("Transform"), "instanceID");
	if (!field)
	{
		return;
	}

	int id = 0;
	mono_field_get_value(thisObject, field, &id);

	auto	transform = EngineData::getComponent<TransformComponent>(id);
	if (transform)
	{
		*rotation = transform->getRotation();
	}
}

void core::scripting::INTERNAL_set_rotation(MonoObject* thisObject, Quaternion* rotation)
{
	MonoClassField*	field = mono_class_get_field_from_name(instance->getClass("Transform"), "instanceID");
	if (!field)
	{
		return;
	}

	int id = 0;
	mono_field_get_value(thisObject, field, &id);

	auto	transform = EngineData::getComponent<TransformComponent>(id);
	if (transform)
	{
		transform->getRotation() = *rotation;
	}
}

void core::scripting::INTERNAL_get_localScale(MonoObject* thisObject, Vector3* localScale)
{
	MonoClassField*	field = mono_class_get_field_from_name(instance->getClass("Transform"), "instanceID");
	if (!field)
	{
		return;
	}

	int id = 0;
	mono_field_get_value(thisObject, field, &id);

	auto	transform = EngineData::getComponent<TransformComponent>(id);
	if (transform)
	{
		*localScale = transform->getScale();
	}
}

void core::scripting::INTERNAL_set_localScale(MonoObject* thisObject, Vector3* localScale)
{
	MonoClassField*	field = mono_class_get_field_from_name(instance->getClass("Transform"), "instanceID");
	if (!field)
	{
		return;
	}

	int id = 0;
	mono_field_get_value(thisObject, field, &id);

	auto	transform = EngineData::getComponent<TransformComponent>(id);
	if (transform)
	{
		transform->getScale() = *localScale;
	}
}

void core::scripting::INTERNAL_CALL_TransformDirection(MonoObject* self, Vector3* direction, Vector3* result)
{
	MonoClassField*	field = mono_class_get_field_from_name(instance->getClass("Transform"), "instanceID");
	if (!field)
	{
		return;
	}

	int id = 0;
	mono_field_get_value(self, field, &id);

	auto	transform = EngineData::getComponent<TransformComponent>(id);
	if (transform)
	{
		auto	rotation = DirectX::SimpleMath::Matrix::CreateFromQuaternion(transform->getRotation());
		*result = DirectX::SimpleMath::Vector3::Transform(*direction, rotation);
	}
}

void core::scripting::INTERNAL_CALL_RotateAroundInternal(MonoObject* self, Vector3* axis, float angle)
{
	MonoClassField*	field = mono_class_get_field_from_name(instance->getClass("Transform"), "instanceID");
	if (!field)
	{
		return;
	}

	int id = 0;
	mono_field_get_value(self, field, &id);

	auto	transform = EngineData::getComponent<TransformComponent>(id);
	if (transform)
	{
		transform->getRotation() *= DirectX::SimpleMath::Quaternion::CreateFromAxisAngle(*axis, angle);
	}
}
