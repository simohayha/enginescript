#include "Vertex.h"

core::Vertex::Vertex()
{
}

core::Vertex::Vertex(const XMFLOAT4& p, const XMFLOAT3& n, const XMFLOAT3& t, const XMFLOAT2& uv) :
	Position(p),
	Normal(n),
	Tangent(t),
	UV(uv)
{
}

core::Vertex::Vertex(const XMFLOAT3 & p) :
	Position(p.x, p.y, p.z, 1.0f),
	Normal(0.0f, 0.0f, 0.0f),
	Tangent(0.0f, 0.0f, 0.0f),
	UV(0.0f, 0.0f)
{
}

core::Vertex::Vertex(float px, float py, float pz, float nx, float ny, float nz, float tx, float ty, float tz, float u, float v) :
	Position(px, py, pz, 1.0f),
	Normal(nx, ny, nz),
	Tangent(tx, ty, tz),
	UV(u, v)
{
}
