#pragma once

#include <vector>

#include "Asset.h"

namespace core
{
	class AbstractScript : public Asset
	{
	public:
		AbstractScript();
		virtual ~AbstractScript();

		virtual bool	initialize();

		void	start();
		void	update();

		void	setEnabled(bool enabled);
		bool	getEnabled() const;

	protected:
		bool	m_enabled;

		virtual void	onStart() = 0;
		virtual void	onUpdate() = 0;
	};
}