#include "mono/metadata/appdomain.h"
#include "mono/metadata/class.h"
#include "mono/metadata/object.h"

#include "Object.h"

core::Object::Object() :
	m_dirty(false),
	m_id(0ul),
	m_fileId(0ull)
{
	setAssetType(E_OBJECT);
}

core::Object::~Object()
{
}

void core::Object::setId(EntityId id)
{
	m_id = id;
}

EntityId core::Object::getId() const
{
	return m_id;
}

void core::Object::setAssetType(AssetType type)
{
	m_assetType = type;
}

core::AssetType core::Object::getAssetType() const
{
	return m_assetType;
}

std::string core::Object::getName() const
{
	return "Object";
}

void core::Object::setName(const std::string& name)
{
}

bool core::Object::isDirty() const
{
	return m_dirty;
}

void core::Object::setIsDirty(bool dirty)
{
	m_dirty = dirty;
}

unsigned long long core::Object::getFileId() const
{
	return m_fileId;
}

void core::Object::setFileId(unsigned long long fileId)
{
	m_fileId = fileId;
}
