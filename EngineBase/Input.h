#pragma once

#include "Vector3.h"

namespace core
{
	class Input
	{
	public:
		Input();
		~Input();

		void	initialize();

		void	keyDown(unsigned int key);
		void	keyUp(unsigned int key);
		void	mouseButtonDown(unsigned int button);
		void	mouseButtonUp(unsigned int button);
		void	setMousePosition(Vector3 position);
		void	setMousePosition(float x, float y, float z);

		bool	isAnyKeyDown() const;
		bool	isAnyKey() const;
		bool	isKeyDown(unsigned int key) const;
		bool	isKey(unsigned int key) const;
		bool	isKeyUp(unsigned int key) const;
		bool	isMouseButtonDown(unsigned int button);
		bool	isMouseButton(unsigned int button);
		bool	isMouseButtonUp(unsigned int button);
		Vector3	getMousePosition() const;

		void	reset();

	private:
		bool	m_keys[256];
		bool	m_previousKeys[256];
		Vector3	m_mousePosition;

		bool	m_mouseButtons[3];
		bool	m_previousMouseButtons[3];
	};

	namespace scripting
	{
		bool	INTERNAL_get_AnyKeyDown();
		bool	INTERNAL_get_AnyKey();
		bool	INTERNAL_GetKeyDownInt(int key);
		bool	INTERNAL_GetKeyInt(int key);
		bool	INTERNAL_GetKeyUpInt(int key);
		bool	INTERNAL_GetMouseButtonDown(int button);
		bool	INTERNAL_GetMouseButton(int button);
		bool	INTERNAL_GetMouseButtonUp(int button);
		void	INTERNAL_GetMousePosition(Vector3* mousePosition);
	}
}
