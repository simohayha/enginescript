#include "Model3D.h"
#include "D3D.h"
#include "EngineData.hpp"
#include "RenderComponent.h"

int core::RenderComponent::Id()
{
	return 1;
}

core::RenderComponent::RenderComponent(EntityId id) :
	Component(id),
	m_material(nullptr),
	m_renderMode(E_RENDERMODE_SOLID)
{
	setAssetType(E_RENDERER);
}

core::RenderComponent::~RenderComponent()
{
}

bool core::RenderComponent::render(Graphic& graphics)
{
	graphics.frame(this);

	return true;
}

core::Material* core::RenderComponent::getMaterial()
{
	return m_material;
}

void core::RenderComponent::setMaterial(Material* material)
{
	m_material = material;
}

void core::RenderComponent::setRenderMode(RenderMode renderMode)
{
	m_renderMode = renderMode;
}

core::RenderMode core::RenderComponent::getRenderMode() const
{
	return m_renderMode;
}

void core::RenderComponent::encodeAsset(std::ofstream& stream) const
{
	YAML::Emitter	out;

	out << YAML::BeginDoc;
	out << YAML::BeginMap;
	out << YAML::Key << "Render";
	out << YAML::BeginMap;

	out << YAML::Key << "m_fileId" << YAML::Value << getFileId();
	out << YAML::Key << "m_type" << YAML::Value << getAssetType();
	out << YAML::Key << "m_gameObject" << YAML::Value << EngineData::getObject(getOwner())->getFileId();

	out << YAML::Key << "m_material";
	out << YAML::BeginMap;
	if (m_material)
	{
		out << YAML::Key << "fileId" << YAML::Value << m_material->getFileId();
	}
	else
	{
		out << YAML::Key << "fileId" << YAML::Value << 0;
	}
	out << YAML::EndMap;

	out << YAML::EndMap;
	out << YAML::EndMap;
	out << YAML::Newline;

	stream << out.c_str();
}
