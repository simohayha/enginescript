#include "Log.h"
#include "UnmanagedLog.h"

UnmanagedLog::UnmanagedLog(Feedback* feedback)
{
	m_feedback = feedback;
	m_log = core::Log::get();
	m_log->setUnmanaged(this);
	m_log->setLevel(core::LOGLEVEL_DEBUG);
}

void UnmanagedLog::getValue(char* buffer)
{
	std::stringbuf&	buf = m_log->getBuffer();
	std::string		str = buf.str();
	size_t			size = str.size();

	strcpy_s(buffer, size + 1u, str.c_str());
	buffer[size] = '\0';
	m_log->clearBuffer();
}

size_t UnmanagedLog::getLen()
{
	return m_log->getBuffer().str().size();
}

core::Log* UnmanagedLog::getLog()
{
	return m_log;
}

void UnmanagedLog::setLog(core::Log* log)
{
	m_log = log;
}

UnmanagedLog::Feedback* UnmanagedLog::getFeedback()
{
	return m_feedback;
}

void UnmanagedLog::setFeedback(Feedback* feedback)
{
	m_feedback = feedback;
}
