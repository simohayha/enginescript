#include <iostream>
#include <random>

#include "DirectXTex.h"

#include "CameraComponent.h"
#include "D3D.h"
#include "Model3D.h"
#include "Log.h"
#include "Async.h"
#include "EngineData.hpp"
#include "AssetLoader.h"
#include "Scene.h"
#include "Game.h"

core::Game* core::Game::m_instance = nullptr;

core::Game::Game() :
	m_paused(true),
	m_timeScale(1.0f),
	m_currentScene(nullptr)
{
	m_instance = this;
}

core::Game::~Game()
{
}

core::Game* core::Game::instance()
{
	return m_instance;
}

bool core::Game::initialize(HWND parent, size_t width, size_t height)
{
	bool	result = true;

	if (!EngineData::initialize())
	{
		return false;
	}

	AssetLoader	loader;
	if (!loader.loadAssets("./Assets"))
	{
		return false;
	}

	m_input.reset(new(std::nothrow) Input);
	if (!m_input)
	{
		LogCall(LOGLEVEL_CRITICAL) << "Failed to allocate input" << LOGFLUSH_FLUSH;
		return false;
	}

	m_input->initialize();

	m_graphic.reset(new(std::nothrow) Graphic);
	if (!m_graphic)
	{
		LogCall(LOGLEVEL_CRITICAL) << "Failed to allocate graphics" << LOGFLUSH_FLUSH;
		return false;
	}

	result = m_graphic->initialize(parent, static_cast<int>(width), static_cast<int>(height));
	if (!result)
	{
		LogCall(LOGLEVEL_CRITICAL) << "Failed to initialize graphics" << LOGFLUSH_FLUSH;
		return false;
	}

	m_scripting.reset(new(std::nothrow) Scripting);
	if (!m_scripting)
	{
		LogCall(LOGLEVEL_CRITICAL) << "Failed to allocate scripts" << LOGFLUSH_FLUSH;
	}

	result = m_scripting->initialize();
	if (!result)
	{
		LogCall(LOGLEVEL_CRITICAL) << "Failed to initialize scripts" << LOGFLUSH_FLUSH;
	}

	std::list<Asset*>	shaders;
	EngineData::getAllObjectOfType<Asset>(shaders, E_SHADER);
	if (shaders.size() == 0)
	{
		return false;
	}
	for (auto& s : shaders)
	{
		Shader*	shader = static_cast<Shader*>(s);

		std::stringstream	ss;
		ss << shader->getFilepath() << "/" << shader->getFilename();
		if (!shader->initialize(m_graphic->getDirect3D()->getDevice(), parent, ss.str()))
		{
			LogCall(LOGLEVEL_CRITICAL) << "Failed to initialize shader" << LOGFLUSH_FLUSH;
			return false;
		}
	}

	std::list<Asset*>	materials;
	EngineData::getAllObjectOfType<Asset>(materials, E_MATERIAL);
	for (auto& m : materials)
	{
		Material*	material = static_cast<Material*>(m);
		material->m_shader = static_cast<Shader*>(shaders.front());
	}

	Model3D* grid = ObjectFactory::instantiate<Model3D>();
	grid->createFromShape(GeometryGenerator::Shape::E_GRID);
	if (!grid->initialize(m_graphic->getDirect3D()->getDevice()))
	{
		LogCall(LOGLEVEL_CRITICAL) << "Failed to initialize model3d" << LOGFLUSH_FLUSH;
		return false;
	}

	Model3D* cube = ObjectFactory::instantiate<Model3D>();
	cube->createFromShape(GeometryGenerator::Shape::E_BOX);
	if (!cube->initialize(m_graphic->getDirect3D()->getDevice()))
	{
		LogCall(LOGLEVEL_CRITICAL) << "Failed to initialize model3d" << LOGFLUSH_FLUSH;
		return false;
	}

	if (!loadTexture(L"stone01.tga"))
	{
		LogCall(LOGLEVEL_CRITICAL) << "Failed to initialize load texture" << LOGFLUSH_FLUSH;
	}
	if (!loadTexture(L"wood.jpg"))
	{
		LogCall(LOGLEVEL_CRITICAL) << "Failed to initialize load texture" << LOGFLUSH_FLUSH;
	}

	m_timer.setFixedTimeStep(true);

	//m_currentScene = ObjectFactory::createAsset(E_SCENE)->as<Scene>();

	// Add root
	int object = addRoot();
	assert(object == 0);

	//auto gridObj = addGameObject();
	//auto render = addComponent<RenderComponent>(gridObj);
	//auto identity = addComponent<IdentityComponent>(gridObj);
	//auto meshFilter = addComponent<MeshFilterComponent>(gridObj);
	//meshFilter->setModel(grid);
	//if (render->getMaterial())
	//{
	//	render->getMaterial()->m_shader = static_cast<Shader*>(shaders.front());
	//	render->getMaterial()->Color = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	//	render->setRenderMode(E_RENDERMODE_WIREFRAME);
	//}
	////getComponent<ScriptingComponent>(gridObj)->attachMonoScript("PrintPosition");
	//identity->setName("Grid");

	//auto mainCamera = addGameObject();
	//identity = addComponent<IdentityComponent>(mainCamera);
	//auto camera = addComponent<CameraComponent>(mainCamera);
	//auto transform = getComponent<TransformComponent>(mainCamera);
	//transform->getPosition() = Vector3(0.0f, 1.0f, -5.0f);
	//camera->setNear(0.1f);
	//camera->setFar(1000.0f);
	//camera->setFov(DirectX::XM_PIDIV2);
	//camera->setAspect(static_cast<float>(width) / static_cast<float>(height));
	////getComponent<ScriptingComponent>(object)->attachMonoScript("PrintPosition");
	//identity->setName("Main camera");

	//auto cube1 = addGameObject(gridObj);
	//transform = getComponent<TransformComponent>(cube1);
	//transform->getPosition() = Vector3(2.0f, 1.1f, 0.1f);
	//identity = addComponent<IdentityComponent>(cube1);
	//render = addComponent<RenderComponent>(cube1);
	//meshFilter = addComponent<MeshFilterComponent>(cube1);
	//meshFilter->setModel(cube);
	//if (render->getMaterial())
	//{
	//	render->getMaterial()->m_shader = static_cast<Shader*>(shaders.front());
	//	render->getMaterial()->Color = XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);
	//	//render->getMaterial()->Resources.push_back(&m_resources[0]);
	//	render->setRenderMode(E_RENDERMODE_SOLID);
	//}
	////getComponent<ScriptingComponent>(cube1)->attachMonoScript("PrintPosition");
	//identity->setName("Cube");

	//auto cube2 = addGameObject();
	//transform = getComponent<TransformComponent>(cube2);
	//identity = addComponent<IdentityComponent>(cube2);
	//render = addComponent<RenderComponent>(cube2);
	//meshFilter = addComponent<MeshFilterComponent>(cube2);
	//meshFilter->setModel(cube);
	//if (render->getMaterial())
	//{
	//	render->getMaterial()->m_shader = static_cast<Shader*>(shaders.front());
	//	render->getMaterial()->Color = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	//	//render->getMaterial()->Resources.push_back(&m_resources[2]);
	//	render->setRenderMode(E_RENDERMODE_SOLID);
	//}
	////getComponent<ScriptingComponent>(object)->attachMonoScript("PrintPosition");
	//identity->setName("Cube");

	//for (int i = 0; i < 3000; ++i)
	//{
	//	float	LO = -20.0f;
	//	float	HI = 20.0f;

	//	float	x = LO + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (HI - LO)));
	//	float	y = LO + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (HI - LO)));
	//	float	z = LO + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (HI - LO)));

	//	int object = addGameObject();
	//	auto transform = addComponent<TransformComponent>(object);
	//	transform->getPosition().x = x;
	//	transform->getPosition().y = y;
	//	transform->getPosition().z = z;
	//	auto identity = addComponent<IdentityComponent>(object);
	//	auto render = addComponent<RenderComponent>(object);
	//	meshFilter = addComponent<MeshFilterComponent>(object);
	//	meshFilter->setModel(cube);
	//	if (render->getMaterial())
	//	{
	//		render->getMaterial()->m_shader = static_cast<Shader*>(shaders.front());
	//		render->getMaterial()->Color = XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);
	//		render->getMaterial()->Resources.push_back(&m_resources[0]);
	//		render->setRenderMode(E_RENDERMODE_SOLID);
	//	}
	//	//getComponent<ScriptingComponent>(object)->attachMonoScript("PrintPosition");
	//	identity->setName("Cube");
	//}

	LogCall(LOGLEVEL_DEBUG) << "Init Game" << LOGFLUSH_FLUSH;

	auto&	systems = EngineData::getSystem<ScriptingComponent>();
	for (auto& system : systems.components())
	{
		static_cast<ScriptingComponent*>(system.second)->callStart();
	}

	//YAML::Emitter	out;
	//for (auto& gameObject : m_gameObjects)
	//{
	//	gameObject.second->encode(out);

	//	LogCall(LOGLEVEL_DEBUG) << out.c_str() << LOGFLUSH_FLUSH;
	//}

	return result;
}

void core::Game::shutdown()
{
	if (m_graphic)
	{
		m_graphic->shutdown();
		m_graphic.reset();
	}

	if (m_input)
	{
		m_input.reset();
	}
}

core::Graphic& core::Game::graphic()
{
	return *m_graphic.get();
}

const core::Graphic& core::Game::graphic() const
{
	return *m_graphic.get();
}

core::Input& core::Game::input()
{
	return *m_input.get();
}

const core::Input& core::Game::input() const
{
	return *m_input.get();
}

bool core::Game::isInitialized() const
{
	return m_graphic.get() != nullptr;
}

void core::Game::pause()
{
	m_paused = !m_paused;
}

bool core::Game::isPaused() const
{
	return m_paused;
}

int core::Game::addGameObject(int parent)
{
	GameObject*	gameObject = ObjectFactory::instantiate<GameObject>();
	gameObject->setAssetType(E_GAMEOBjECT);

	auto transform = addComponent<TransformComponent>(gameObject->getId());
	getComponent<TransformComponent>(gameObject->getId());

	//auto scripting = addComponent<ScriptingComponent>(gameObject->getId(), m_scripting.get());
	//if (!scripting->initialize())
	//{
	//	LogCall(LOGLEVEL_CRITICAL) << "Failed to initialize scripting component" << LOGFLUSH_FLUSH;
	//}


	if (gameObject->getId() != 0)
	{
		auto parentTransform = getComponent<TransformComponent>(parent);
		transform->setParent(parent);
	}
	else
	{
		transform->setParent(-1);
	}

	if (gameObject->getId() != 0)
	{
		m_currentScene->addGameObject(gameObject);
	}

	return gameObject->getId();
}

int core::Game::addRoot()
{
	GameObject*	gameObject = ObjectFactory::instantiate<GameObject>(0);
	gameObject->setAssetType(E_GAMEOBjECT);

	auto transform = addComponent<TransformComponent>(gameObject->getId());
	getComponent<TransformComponent>(gameObject->getId());

	auto scripting = addComponent<ScriptingComponent>(gameObject->getId(), m_scripting.get());
	if (!scripting->initialize())
	{
		LogCall(LOGLEVEL_CRITICAL) << "Failed to initialize scripting component" << LOGFLUSH_FLUSH;
	}

	transform->setParent(-1);

	return gameObject->getId();
}

void core::Game::getRootGameObjects(std::vector<int>& gameObjects)
{
	auto	transform = getComponent<TransformComponent>(0);
	auto	childs = transform->getChilds();
	gameObjects.reserve(childs.size());
	for (auto& child : childs)
	{
		gameObjects.push_back(child);
	}
}

core::AssetType core::Game::getAssetType(int objectId)
{
	auto	obj = EngineData::getObject(objectId);
	return obj->getAssetType();
}

core::Object* core::Game::getObject(int objectId)
{
	return EngineData::getObject(objectId);
}

void core::Game::update(bool ignorePause)
{
	m_timer.tick([&]()
	{
		if (!isPaused() || (isPaused() && ignorePause))
		{
			onUpdate();
		}
	});
}

void core::Game::render()
{
	onRender();
}

float core::Game::getDeltaTime() const
{
	return getUnscaledTime() * getTimeScale();
}

float core::Game::getTimeScale() const
{
	return m_timeScale;
}

void core::Game::setTimeScale(float ts)
{
	m_timeScale = ts;
}

float core::Game::getUnscaledTime() const
{
	return static_cast<float>(m_timer.getElapsedSeconds());
}

float core::Game::getTotalTime() const
{
	return static_cast<float>(m_timer.getTotalSeconds());
}

bool core::Game::onUpdate()
{
	bool	result = true;
	float	dt = getDeltaTime();
	float	t = static_cast<float>(m_timer.getTotalSeconds());

	auto&	systems = EngineData::getSystem<ScriptingComponent>();
	for (auto& system : systems.components())
	{
		static_cast<ScriptingComponent*>(system.second)->callUpdate();
	}

	m_input->reset();

	return result;
}

bool core::Game::onRender()
{
	bool	result = true;

	auto&	cameraSystems = EngineData::getSystem<CameraComponent>();
	auto&	renderSysyems = EngineData::getSystem<RenderComponent>();
	for (auto& camera : cameraSystems.components())
	{
		auto	cameraComponent = static_cast<CameraComponent*>(camera.second);
		m_graphic->beginScene(cameraComponent->getBackgroundColor());
		m_graphic->setCurrentCamera(cameraComponent);

		for (auto& renderer : renderSysyems.components())
		{
			bool	result = static_cast<RenderComponent*>(renderer.second)->render(*m_graphic);
			if (!result)
			{
				return false;
			}
		}

		m_graphic->endScene();
	}

	return result;
}

core::Asset* core::Game::getAssetFolder()
{
	return EngineData::getAssetDirectoryPtr();
}

bool core::Game::loadTexture(const wchar_t* name)
{
	ScratchImage	image;
	HRESULT			hr = S_OK;
	Resource		resource;
	TexMetadata		meta;

	std::wstring	ext(name);
	if (ext.rfind(L".tga") != std::wstring::npos)
	{
		hr = GetMetadataFromTGAFile
		(
			name,
			meta
		);
		if (FAILED(hr))
		{
			std::cerr << "Failed to get meta from " << name << '\n';
			return false;
		}

		hr = LoadFromTGAFile(name, nullptr, image);
		if (FAILED(hr))
		{
			std::cerr << "Failed to load texture " << name << '\n';
			return false;
		}
	}
	else if (ext.rfind(L".jpg") != std::wstring::npos)
	{
		hr = GetMetadataFromWICFile
		(
			name,
			0,
			meta
		);
		if (FAILED(hr))
		{
			std::cerr << "Failed to get meta from " << name << '\n';
			return false;
		}

		hr = LoadFromWICFile(name, 0, nullptr, image);
		if (FAILED(hr))
		{
			std::cerr << "Failed to load texture " << name << '\n';
			return false;
		}
	}

	hr = CreateShaderResourceView
	(
		m_graphic->getDirect3D()->getDevice(),
		image.GetImages(),
		image.GetImageCount(),
		meta,
		resource.ResView.ReleaseAndGetAddressOf()
	);
	if (FAILED(hr))
	{
		std::cerr << "Failed to create shader view " << name << '\n';
		return false;
	}
	m_resources[resource.Id] = resource;

	return true;
}

float core::scripting::getDeltaTime()
{
	return Game::instance()->getDeltaTime();
}

float core::scripting::getTimeScale()
{
	return Game::instance()->getTimeScale();
}

void core::scripting::setTimeScale(float ts)
{
	Game::instance()->setTimeScale(ts);
}

float core::scripting::getUnscaledTime()
{
	return Game::instance()->getUnscaledTime();
}

