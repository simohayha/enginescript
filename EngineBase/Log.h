#pragma once

#include <functional>
#include <string>
#include <iostream>
#include <sstream>

#define LogCall(logLevel)	*(logger<logLevel>(__FILE__, __LINE__, __FUNCTION__))

class UnmanagedLog;

namespace core
{
	enum LogFlush : int
	{
		LOGFLUSH_ENDL,
		LOGFLUSH_FLUSH
	};

	enum LogLevel : int
	{
		LOGLEVEL_NOTHING,
		LOGLEVEL_CRITICAL,
		LOGLEVEL_ERROR,
		LOGLEVEL_WARNING,
		LOGLEVEL_INFO,
		LOGLEVEL_DEBUG
	};

	class Log
	{
	public:
		~Log();

		static Log*		get(LogLevel level = LOGLEVEL_DEBUG);

		Log&			operator<<(LogFlush right);
		void			setCurrentLogLevel(LogLevel logLevel);
		void			setLevel(LogLevel logLevel);
		std::stringbuf&	getBuffer();
		void			clearBuffer();
		void			setUnmanaged(UnmanagedLog* unmanaged);
		template <typename T>
		Log&			operator<<(const T& right);
		void			setLoggingCallback(std::function<void(LogLevel, std::string)> callback);

	private:
		static Log*		m_instance;

		LogLevel		m_level;
		LogLevel		m_current;
		std::stringbuf	m_buffer;
		std::ostream	m_os;
		UnmanagedLog*	m_unmanaged;

		std::function<void(LogLevel, std::string)>	m_loggingCallback;

		Log(LogLevel level);
	};
}

#include "Log.inl"