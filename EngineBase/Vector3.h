#pragma once

#include <d3d11.h>
#include <SimpleMath.h>

namespace core
{
	struct Vector3 : DirectX::SimpleMath::Vector3
	{
	public:
		Vector3();
		Vector3(float x);
		Vector3(float x, float y, float z);
		Vector3(const DirectX::SimpleMath::Vector3& v);
		~Vector3();
	};
}
