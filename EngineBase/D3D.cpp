#include <DirectXColors.h>

#include <sstream>
#include <iostream>
#include <algorithm>
#include <memory>

#include "Utility.h"
#include "D3D.h"

core::D3D::D3D()
{
}

core::D3D::~D3D()
{
}

bool core::D3D::initialize(HWND hwnd, int screenWidth, int screenHeight, bool vsync, bool fullscreen, float screenDepth, float screenNear)
{
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
	m_vsyncEnabled = vsync;

	if (!checkVideoCard())
	{
		return false;
	}

	if (!initSwapChain(hwnd, fullscreen))
	{
		return false;
	}

	if (!initDepthBuffer())
	{
		return false;
	}

	if (!initRasterizer())
	{
		return false;
	}

	initProjection(screenDepth, screenNear);

	if (!init2DStencil())
	{
		return false;
	}

	if (!initBlendStates())
	{
		return false;
	}

	initViewPort();

	return true;
}

void core::D3D::shutdown()
{
}

void core::D3D::beginScene(DirectX::SimpleMath::Color& clearColor)
{
	m_deviceContext->ClearRenderTargetView(m_renderTargetView.Get(), clearColor);

	m_deviceContext->ClearDepthStencilView(m_depthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.0f, 0);

	CD3D11_VIEWPORT viewport(0.0f, 0.0f, static_cast<float>(m_screenWidth), static_cast<float>(m_screenHeight));
	m_deviceContext->RSSetViewports(1u, &viewport);
}

void core::D3D::endScene()
{
	HRESULT	hr = S_OK;
	hr = m_swapChain->Present(0, 0);
	if (FAILED(hr))
	{
		std::cout << "Present failed\n";
	}
}

ID3D11Device* core::D3D::getDevice()
{
	return m_device.Get();
}

ID3D11DeviceContext* core::D3D::getDeviceContext()
{
	return m_deviceContext.Get();
}

void core::D3D::getProjectionMatrix(XMMATRIX& matrix)
{
	matrix = XMLoadFloat4x4(&m_projectionMatrix);
}

void core::D3D::getProjectionMatrix(XMFLOAT4X4& matrix)
{
	matrix = m_projectionMatrix;
}

D3D_DRIVER_TYPE core::D3D::getDriverType() const
{
	return D3D_DRIVER_TYPE();
}

ID3D11RasterizerState* core::D3D::getWireframeState() const
{
	return m_wireFrame.Get();
}

ID3D11RasterizerState* core::D3D::getSolidState() const
{
	return m_solid.Get();
}

void core::D3D::getVideoCardInfo(char* cardName, int& memory)
{
	strcpy_s(cardName, 128, m_videoCardDescription);
	memory = m_videoCardMemory;
}

void core::D3D::resize(int height, int width)
{
	m_screenHeight = height;
	m_screenWidth = width;

	ID3D11RenderTargetView*	nullViews[] = { nullptr };
	m_deviceContext->OMSetRenderTargets(_countof(nullViews), nullViews, nullptr);
	m_renderTargetView.Reset();
	m_depthStencilView.Reset();
	m_deviceContext->Flush();

	HRESULT	hr = S_OK;

	if (m_swapChain)
	{
		unsigned int	backBufferWidth = static_cast<unsigned int>(width);
		unsigned int	backBufferHeight = static_cast<unsigned int>(height);
		DXGI_FORMAT		backBufferFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
		unsigned int	backBufferCount = 2u;

		hr = m_swapChain->ResizeBuffers(backBufferCount, backBufferWidth, backBufferHeight, backBufferFormat, 0u);
		if (FAILED(hr))
		{
			throw std::exception("Failed to resize buffer");
		}
	}

	ComPtr<ID3D11Texture2D>	backBuffer;
	hr = m_swapChain->GetBuffer(0u, IID_PPV_ARGS(backBuffer.GetAddressOf()));
	if (FAILED(hr))
	{
		throw std::exception("Failed to get buffer");
	}

	m_device->CreateRenderTargetView(backBuffer.Get(), nullptr, m_renderTargetView.GetAddressOf());

	initDepthBuffer();

	m_deviceContext->OMSetRenderTargets(1u, m_renderTargetView.GetAddressOf(), m_depthStencilView.Get());

	D3D11_VIEWPORT vp;
	vp.Width = static_cast<float>(m_screenWidth);
	vp.Height = static_cast<float>(m_screenHeight);
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0.0f;
	vp.TopLeftY = 0.0f;
	m_deviceContext->RSSetViewports(1u, &vp);

	initProjection(1000.0f, 0.1f);
}

bool core::D3D::checkVideoCard()
{
	ComPtr<IDXGIFactory>	factory;
	ComPtr<IDXGIAdapter>	adapter;
	ComPtr<IDXGIOutput>		adapterOutput;
	DXGI_ADAPTER_DESC		adapterDesc;
	unsigned int			numModes = 0u;
	size_t					stringLength = 0u;
	HRESULT					hr = S_OK;

	hr = CreateDXGIFactory
	(
		__uuidof(IDXGIFactory),
		(void**)factory.ReleaseAndGetAddressOf()
	);
	if (FAILED(hr))
	{
		return false;
	}

	hr = factory->EnumAdapters
	(
		0u,
		adapter.ReleaseAndGetAddressOf()
	);
	if (FAILED(hr))
	{
		return false;
	}

	hr = adapter->EnumOutputs
	(
		0u,
		adapterOutput.ReleaseAndGetAddressOf()
	);
	if (FAILED(hr))
	{
		return false;
	}

	hr = adapterOutput->GetDisplayModeList
	(
		DXGI_FORMAT_R8G8B8A8_UNORM,
		DXGI_ENUM_MODES_INTERLACED,
		&numModes,
		nullptr
	);
	if (FAILED(hr))
	{
		return false;
	}

	auto	displayModeList = std::unique_ptr<DXGI_MODE_DESC>(new(std::nothrow) DXGI_MODE_DESC[numModes]);
	if (!displayModeList)
	{
		return false;
	}
	hr = adapterOutput->GetDisplayModeList
	(
		DXGI_FORMAT_R8G8B8A8_UNORM,
		DXGI_ENUM_MODES_INTERLACED,
		&numModes,
		displayModeList.get()
	);
	if (FAILED(hr))
	{
		return false;
	}

	for (auto i = 0u; i < numModes; ++i)
	{
		if (displayModeList.get()[i].Width == m_screenWidth &&
			displayModeList.get()[i].Height == m_screenHeight)
		{
			m_numerator = displayModeList.get()[i].RefreshRate.Numerator;
			m_denominator = displayModeList.get()[i].RefreshRate.Denominator;
		}
	}

	hr = adapter->GetDesc(&adapterDesc);
	if (FAILED(hr))
	{
		return false;
	}

	m_videoCardMemory = static_cast<int>(adapterDesc.DedicatedVideoMemory / 1024 / 1024);
	auto	error = wcstombs_s(&stringLength, m_videoCardDescription, 128u, adapterDesc.Description, 128u);
	if (error != 0)
	{
		return false;
	}

	return true;
}

bool core::D3D::initSwapChain(HWND hwnd, bool fullscreen)
{
	DXGI_SWAP_CHAIN_DESC	swapChainDesc;
	D3D_FEATURE_LEVEL		featureLevel = D3D_FEATURE_LEVEL_11_0;
	ComPtr<ID3D11Texture2D>	backBufferPtr;
	HRESULT					hr = S_OK;
	unsigned int			creationFlags = 0u;

#if _DEBUG
	creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Width = m_screenWidth;
	swapChainDesc.BufferDesc.Height = m_screenHeight;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	if (m_vsyncEnabled)
	{
		swapChainDesc.BufferDesc.RefreshRate.Numerator = m_numerator;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = m_denominator;
	}
	else
	{
		swapChainDesc.BufferDesc.RefreshRate.Numerator = 0u;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = 1u;
	}
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = hwnd;
	swapChainDesc.SampleDesc.Count = 1u;
	swapChainDesc.SampleDesc.Quality = 0u;
	swapChainDesc.Windowed = !fullscreen;
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_CENTERED;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapChainDesc.Flags = 0u;

	hr = D3D11CreateDeviceAndSwapChain
	(
		nullptr,
		D3D_DRIVER_TYPE_HARDWARE,
		nullptr,
		creationFlags,
		&featureLevel,
		1u,
		D3D11_SDK_VERSION,
		&swapChainDesc,
		m_swapChain.ReleaseAndGetAddressOf(),
		m_device.ReleaseAndGetAddressOf(),
		nullptr,
		m_deviceContext.ReleaseAndGetAddressOf()
	);
	if (FAILED(hr))
	{
		return false;
	}

	hr = m_swapChain->GetBuffer
	(
		0u,
		__uuidof(ID3D11Texture2D),
		(LPVOID*)backBufferPtr.ReleaseAndGetAddressOf()
	);
	if (FAILED(hr))
	{
		return false;
	}

	hr = m_device->CreateRenderTargetView(backBufferPtr.Get(), nullptr, m_renderTargetView.ReleaseAndGetAddressOf());
	if (FAILED(hr))
	{
		return false;
	}

	return true;
}

bool core::D3D::initDepthBuffer()
{
	D3D11_TEXTURE2D_DESC			depthBufferDesc;
	D3D11_DEPTH_STENCIL_DESC		depthStencilDesc;
	D3D11_DEPTH_STENCIL_VIEW_DESC	depthStencilViewDesc;
	HRESULT							hr = S_OK;

	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));
	depthBufferDesc.Width = m_screenWidth;
	depthBufferDesc.Height = m_screenHeight;
	depthBufferDesc.MipLevels = 1u;
	depthBufferDesc.ArraySize = 1u;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1u;
	depthBufferDesc.SampleDesc.Quality = 0u;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0u;
	depthBufferDesc.MiscFlags = 0u;
	hr = m_device->CreateTexture2D
	(
		&depthBufferDesc,
		nullptr,
		m_depthStencilBuffer.ReleaseAndGetAddressOf()
	);
	if (FAILED(hr))
	{
		return false;
	}

	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	hr = m_device->CreateDepthStencilState
	(
		&depthStencilDesc,
		m_depthStencilState.ReleaseAndGetAddressOf()
	);
	if (FAILED(hr))
	{
		return false;
	}
	m_deviceContext->OMSetDepthStencilState(m_depthStencilState.Get(), 1u);

	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;
	hr = m_device->CreateDepthStencilView
	(
		m_depthStencilBuffer.Get(),
		&depthStencilViewDesc,
		m_depthStencilView.ReleaseAndGetAddressOf()
	);
	if (FAILED(hr))
	{
		return false;
	}

	m_deviceContext->OMSetRenderTargets(1u, m_renderTargetView.GetAddressOf(), m_depthStencilView.Get());

	return true;
}

bool core::D3D::init2DStencil()
{
	D3D11_DEPTH_STENCIL_DESC	depthDisabledStencilDesc;
	HRESULT						hr = S_OK;

	ZeroMemory(&depthDisabledStencilDesc, sizeof(depthDisabledStencilDesc));
	depthDisabledStencilDesc.DepthEnable = false;
	depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthDisabledStencilDesc.StencilEnable = true;
	depthDisabledStencilDesc.StencilReadMask = 0xFF;
	depthDisabledStencilDesc.StencilWriteMask = 0xFF;
	depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	hr = m_device->CreateDepthStencilState
	(
		&depthDisabledStencilDesc,
		m_depthDisabledStencilState.ReleaseAndGetAddressOf()
	);
	if (FAILED(hr))
	{
		return false;
	}

	return true;
}

bool core::D3D::initBlendStates()
{
	D3D11_BLEND_DESC	blendStateDesc;
	HRESULT				hr = S_OK;

	ZeroMemory(&blendStateDesc, sizeof(blendStateDesc));
	blendStateDesc.RenderTarget[0].BlendEnable = true;
	blendStateDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blendStateDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendStateDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendStateDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendStateDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendStateDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendStateDesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;
	hr = m_device->CreateBlendState
	(
		&blendStateDesc,
		m_alphaEnableBlendingState.ReleaseAndGetAddressOf()
	);
	if (FAILED(hr))
	{
		return false;
	}

	blendStateDesc.RenderTarget[0].BlendEnable = false;
	hr = m_device->CreateBlendState
	(
		&blendStateDesc,
		m_alphaDisableBlendingState.ReleaseAndGetAddressOf()
	);
	if (FAILED(hr))
	{
		return false;
	}

	return true;
}

bool core::D3D::initRasterizer()
{
	D3D11_RASTERIZER_DESC	rasterDesc;
	HRESULT					hr = S_OK;

	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.DepthBias = 0;
	rasterDesc.SlopeScaledDepthBias = 0.0f;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.ScissorEnable = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.AntialiasedLineEnable = false;
	hr = m_device->CreateRasterizerState
	(
		&rasterDesc,
		m_solid.ReleaseAndGetAddressOf()
	);
	if (FAILED(hr))
	{
		return false;
	}

	rasterDesc.FillMode = D3D11_FILL_WIREFRAME;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.DepthBias = 0;
	rasterDesc.SlopeScaledDepthBias = 0.0f;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.ScissorEnable = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.AntialiasedLineEnable = false;
	hr = m_device->CreateRasterizerState
	(
		&rasterDesc,
		m_wireFrame.ReleaseAndGetAddressOf()
	);
	if (FAILED(hr))
	{
		return false;
	}

	return true;
}

void core::D3D::initViewPort()
{
	D3D11_VIEWPORT	viewport;

	viewport.Width = static_cast<float>(m_screenWidth);
	viewport.Height = static_cast<float>(m_screenHeight);
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;

	m_deviceContext->RSSetViewports(1u, &viewport);
}

void core::D3D::initProjection(float screenDepth, float screenNear)
{
	float	fov = XM_PI / 2.0f;
	float	aspect = static_cast<float>(m_screenWidth) / static_cast<float>(m_screenHeight);

	std::stringstream	ss;
	ss << "Fov: " << fov << ", apsect: " << aspect << '\n';
	OutputDebugStringA(ss.str().c_str());
	
	XMMATRIX projectionMatrix = XMMatrixPerspectiveFovLH(fov, aspect, screenNear, screenDepth);
	XMStoreFloat4x4(&m_projectionMatrix, projectionMatrix);
}
