#include "EngineData.hpp"
#include "MeshFilterComponent.h"

core::MeshFilterComponent::MeshFilterComponent(EntityId id) :
	Component(id)
{
	setAssetType(E_MESHFILTER);
}

core::MeshFilterComponent::~MeshFilterComponent()
{
}

core::Model3D* core::MeshFilterComponent::getModel()
{
	return m_model;
}

void core::MeshFilterComponent::setModel(Model3D* model)
{
	m_model = model;
}

void core::MeshFilterComponent::encodeAsset(std::ofstream& stream) const
{
	YAML::Emitter	out;

	out << YAML::BeginDoc;

	out << YAML::BeginMap;
	out << YAML::Key << "Mesh Filter";
	out << YAML::BeginMap;

	out << YAML::Key << "m_fileId" << YAML::Value << getFileId();
	out << YAML::Key << "m_type" << YAML::Value << getAssetType();
	out << YAML::Key << "m_gameObject" << YAML::Value << EngineData::getObject(getOwner())->getFileId();

	out << YAML::EndMap;
	out << YAML::EndMap;

	out << YAML::Newline;

	stream << out.c_str();
}
