#include "Scripting.h"
#include "EngineData.hpp"
#include "IdentityComponent.h"

extern core::Scripting*	instance;

int core::IdentityComponent::Id()
{
	return 0;
}

core::IdentityComponent::IdentityComponent(EntityId id) :
	Component(id)
{
	setAssetType(E_IDENTITY);

	//mono_add_internal_call("Scripting.Components.Identity::get_name", scripting::getName);
	//mono_add_internal_call("Scripting.Components.Identity::set_name", scripting::setName);
}

core::IdentityComponent::~IdentityComponent()
{
}

void core::IdentityComponent::setName(const std::string& name)
{
	m_name = name;
}

std::string core::IdentityComponent::getName() const
{
	return m_name;
}

void core::IdentityComponent::encodeAsset(std::ofstream& stream) const
{
	YAML::Emitter	out;

	out << YAML::BeginDoc;

	out << YAML::BeginMap;
	out << YAML::Key << "Identity";
	out << YAML::BeginMap;
	out << YAML::Key << "m_fileId" << YAML::Value << getFileId();
	out << YAML::Key << "m_type" << YAML::Value << getAssetType();
	out << YAML::Key << "m_gameObject" << YAML::Value << EngineData::getObject(getOwner())->getFileId();

	out << YAML::Key << "m_name" << m_name;
	out << YAML::EndMap;
	out << YAML::EndMap;

	out << YAML::Newline;

	stream << out.c_str();
}

void core::IdentityComponent::decodeAsset(YAML::Node& node)
{
}

MonoString* core::scripting::getName(MonoObject* thisObject)
{
	MonoClassField*	field = mono_class_get_field_from_name(instance->getClass("Identity"), "instanceID");
	if (!field)
	{
		return nullptr;
	}

	int id = 0;
	mono_field_get_value(thisObject, field, &id);

	auto	identity = EngineData::getComponent<IdentityComponent>(id);
	return mono_string_new(instance->getDomain(), identity->getName().c_str());
}

void core::scripting::setName(MonoObject* thisObject, MonoString* name)
{
	// TODO
}
