#include "MonoClasses.h"

namespace core
{
	namespace mono
	{
		namespace klass
		{
			MonoClass*	Object = nullptr;
			MonoClass*	GameObject = nullptr;
			MonoClass*	Component = nullptr;
			MonoClass*	WorldPositionComponent = nullptr;
			MonoClass*	RenderComponent = nullptr;
		}
	}
}