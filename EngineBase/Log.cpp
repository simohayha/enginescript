#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "UnmanagedLog.h"
#include "Log.h"

core::Log*	core::Log::m_instance = nullptr;

core::Log& core::Log::operator<<(core::LogFlush right)
{
	if (right == LOGFLUSH_ENDL)
	{
		m_os << '\n';
	}
	else
	{
		if (m_current <= m_level)
		{
			if (m_loggingCallback)
			{
				m_loggingCallback(m_current, m_buffer.str());
			}
			//if (m_unmanaged)
			//{
			//	m_unmanaged->getFeedback()->OnFlush(m_current);
			//}
			else
			{
				OutputDebugStringA(m_buffer.str().c_str());
				OutputDebugStringA("\n");
			}
		}
		m_buffer.str("");
	}
	return *this;
}

core::Log* core::Log::get(core::LogLevel level)
{
	if (m_instance == nullptr)
	{
		m_instance = new(std::nothrow) Log(level);
		if (!m_instance)
		{
			throw std::exception("Failed to allocate logger");
		}
	}
	return m_instance;
}

void core::Log::setCurrentLogLevel(LogLevel logLevel)
{
	m_current = logLevel;
}

void core::Log::setLevel(LogLevel logLevel)
{
	m_level = logLevel;
}

std::stringbuf& core::Log::getBuffer()
{
	return m_buffer;
}

void core::Log::clearBuffer()
{
	m_buffer.str("");
}

void core::Log::setUnmanaged(UnmanagedLog* unmanaged)
{
	m_unmanaged = unmanaged;
}

void core::Log::setLoggingCallback(std::function<void(LogLevel, std::string)> callback)
{
	m_loggingCallback = callback;
}

core::Log::Log(LogLevel level) :
	m_level(level),
	m_current(LOGLEVEL_NOTHING),
	m_os(&m_buffer),
	m_unmanaged(nullptr)
{
}
