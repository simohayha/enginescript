#include <iostream>
#include <algorithm>

#include "Log.h"
#include "Resource.h"
#include "Shader.h"

core::Shader::Shader() :
	m_vertexShader(nullptr),
	m_pixelShader(nullptr),
	m_sampleState(nullptr)
{
	setAssetType(E_SHADER);
}

core::Shader::~Shader()
{
}

bool core::Shader::initialize(ID3D11Device* device, HWND hwnd, const std::string& file)
{
	bool result;

	//result = initializeShader(device, hwnd, L"vs.cso", L"ps.cso");
	result = initializeShader(device, hwnd, file);

	if (!result)
	{
		return false;
	}

	return true;
}

void core::Shader::shutdown()
{
	shutdownShader();
}

const core::ConstantBufferLayout& core::Shader::getBufferLayout(int index) const
{
	return m_constantBuffers[index]->Layout;
}

ID3D11VertexShader* core::Shader::getVertexShader()
{
	return m_vertexShader;
}

ID3D11PixelShader* core::Shader::getPixelShader()
{
	return m_pixelShader;
}

ID3D11InputLayout* core::Shader::getInputLayout()
{
	return m_layout;
}

bool core::Shader::setMaterialBuffer(ID3D11DeviceContext* context, const Material& material)
{
	auto	bufferIt = std::find_if(m_constantBuffers.begin(), m_constantBuffers.end(), [](ConstantBuffer* buffer) 
	{
		return buffer->Layout.Name == "MaterialBuffer";
	});

	if (bufferIt != m_constantBuffers.end())
	{
		return setVSMaterialConstantBuffers(context, *bufferIt, material);
	}
	else
	{
		return false;
	}
}

bool core::Shader::setCameraMatrixBuffer(ID3D11DeviceContext* context, XMFLOAT4X4 view, XMFLOAT4X4 projection)
{
	auto	bufferIt = std::find_if(m_constantBuffers.begin(), m_constantBuffers.end(), [](ConstantBuffer* buffer)
	{
		return buffer->Layout.Name == "CameraMatrixBuffer";
	});

	if (bufferIt != m_constantBuffers.end())
	{
		HRESULT						result;
		D3D11_MAPPED_SUBRESOURCE	mappedResource;
		XMMATRIX*					dataPtr;

		XMMATRIX	v = XMLoadFloat4x4(&view);
		XMMATRIX	p = XMLoadFloat4x4(&projection);

		v = XMMatrixTranspose(v);
		p = XMMatrixTranspose(p);

		result = context->Map((*bufferIt)->Buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
		if (FAILED(result))
		{
			return false;
		}

		dataPtr = (XMMATRIX*)mappedResource.pData;
		dataPtr[0] = v;
		dataPtr[1] = p;

		context->Unmap((*bufferIt)->Buffer, 0);

		context->VSSetConstantBuffers((*bufferIt)->Slot, 1, &(*bufferIt)->Buffer);

		return true;
	}
	else
	{
		return false;
	}
}

bool core::Shader::setWorldMatrixBuffer(ID3D11DeviceContext* context, XMFLOAT4X4 world)
{
	auto	bufferIt = std::find_if(m_constantBuffers.begin(), m_constantBuffers.end(), [](ConstantBuffer* buffer)
	{
		return buffer->Layout.Name == "WorldMatrixBuffer";
	});

	if (bufferIt != m_constantBuffers.end())
	{
		HRESULT						result;
		D3D11_MAPPED_SUBRESOURCE	mappedResource;
		XMMATRIX*					dataPtr;

		XMMATRIX	w = XMLoadFloat4x4(&world);

		w = XMMatrixTranspose(w);

		result = context->Map((*bufferIt)->Buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
		if (FAILED(result))
		{
			return false;
		}

		dataPtr = (XMMATRIX*)mappedResource.pData;
		dataPtr[0] = w;

		context->Unmap((*bufferIt)->Buffer, 0);

		context->VSSetConstantBuffers((*bufferIt)->Slot, 1, &(*bufferIt)->Buffer);

		return true;
	}
	else
	{
		return false;
	}
}

void core::Shader::setModelBuffer(const Model3D& model)
{
}

void core::Shader::setSamplers(ID3D11DeviceContext* context)
{
	for (auto& it : m_shaderInput)
	{
		if (it.InputDesc.Type == D3D_SIT_SAMPLER)
		{
			context->PSSetSamplers(it.InputDesc.BindPoint, 1u, it.SampleState.GetAddressOf());
		}
	}
}

void core::Shader::setResources(ID3D11DeviceContext* context, const Material& material)
{
	for (auto i = 0u; i < material.Resources.size(); ++i)
	{
		context->PSSetShaderResources(i, 1u, material.Resources[i]->ResView.GetAddressOf());
	}
}

void core::Shader::clearResources(ID3D11DeviceContext* context, const Material& material)
{
	ID3D11ShaderResourceView*	nullSRV[] = { nullptr };
	context->PSSetShaderResources(0u, static_cast<UINT>(material.Resources.size()), nullSRV);
}

bool core::Shader::initializeShader(ID3D11Device* device, HWND hwnd, WCHAR* vs, WCHAR* ps)
{
	HRESULT						hr = S_OK;
	D3D11_BUFFER_DESC			bufferDesc;

	//if (!buildShaderProgram(device, vs, ps))
	//{
	//	return false;
	//}

	for (size_t i = 0u; i < m_constantBuffers.size(); ++i)
	{
		unsigned int	byteWidth = 0u;
		for (size_t j = 0; j < m_constantBuffers[i]->Layout.Variables.size(); ++j)
		{
			byteWidth += m_constantBuffers[i]->Layout.Variables[j].Size;
		}

		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.ByteWidth = byteWidth;
		bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		bufferDesc.MiscFlags = 0;
		bufferDesc.StructureByteStride = 0;

		hr = device->CreateBuffer(&bufferDesc, nullptr, &m_constantBuffers[i]->Buffer);
		if (FAILED(hr))
		{
			return false;
		}
	}

	for (size_t i = 0u; i < m_shaderInput.size(); ++i)
	{
		auto& input = m_shaderInput[i].InputDesc;
		if (input.Type == D3D_SIT_SAMPLER)
		{
			D3D11_SAMPLER_DESC	samplerDesc;
			samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
			samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
			samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
			samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
			samplerDesc.MipLODBias = 0.0f;
			samplerDesc.MaxAnisotropy = 1u;
			samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
			samplerDesc.BorderColor[0] = 0.0f;
			samplerDesc.BorderColor[1] = 0.0f;
			samplerDesc.BorderColor[2] = 0.0f;
			samplerDesc.BorderColor[3] = 0.0f;
			samplerDesc.MinLOD = 0.0f;
			samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

			hr = device->CreateSamplerState(&samplerDesc, m_shaderInput[i].SampleState.ReleaseAndGetAddressOf());
			if (FAILED(hr))
			{
				return false;
			}
		}
	}

	return true;
}

bool core::Shader::initializeShader(ID3D11Device * device, HWND hwnd, const std::string& file)
{
	HRESULT						hr = S_OK;
	D3D11_BUFFER_DESC			bufferDesc;

	if (!buildShaderProgram(device, file))
	{
		return false;
	}

	for (size_t i = 0u; i < m_constantBuffers.size(); ++i)
	{
		unsigned int	byteWidth = 0u;
		for (size_t j = 0; j < m_constantBuffers[i]->Layout.Variables.size(); ++j)
		{
			byteWidth += m_constantBuffers[i]->Layout.Variables[j].Size;
		}

		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.ByteWidth = byteWidth;
		bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		bufferDesc.MiscFlags = 0;
		bufferDesc.StructureByteStride = 0;

		hr = device->CreateBuffer(&bufferDesc, nullptr, &m_constantBuffers[i]->Buffer);
		if (FAILED(hr))
		{
			return false;
		}
	}

	for (size_t i = 0u; i < m_shaderInput.size(); ++i)
	{
		auto& input = m_shaderInput[i].InputDesc;
		if (input.Type == D3D_SIT_SAMPLER)
		{
			D3D11_SAMPLER_DESC	samplerDesc;
			samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
			samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
			samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
			samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
			samplerDesc.MipLODBias = 0.0f;
			samplerDesc.MaxAnisotropy = 1u;
			samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
			samplerDesc.BorderColor[0] = 0.0f;
			samplerDesc.BorderColor[1] = 0.0f;
			samplerDesc.BorderColor[2] = 0.0f;
			samplerDesc.BorderColor[3] = 0.0f;
			samplerDesc.MinLOD = 0.0f;
			samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

			hr = device->CreateSamplerState(&samplerDesc, m_shaderInput[i].SampleState.ReleaseAndGetAddressOf());
			if (FAILED(hr))
			{
				return false;
			}
		}
	}

	return true;
}

void core::Shader::shutdownShader()
{
	if (m_layout)
	{
		m_layout->Release();
		m_layout = nullptr;
	}

	if (m_pixelShader)
	{
		m_pixelShader->Release();
		m_pixelShader = nullptr;
	}

	if (m_vertexShader)
	{
		m_vertexShader->Release();
		m_vertexShader = nullptr;
	}
}

void core::Shader::renderShader(ID3D11DeviceContext* context, int indexCount)
{
	context->IASetInputLayout(m_layout);

	context->VSSetShader(m_vertexShader, nullptr, 0);
	context->PSSetShader(m_pixelShader, nullptr, 0);

	context->DrawIndexed(indexCount, 0, 0);
}

bool core::Shader::buildShaderProgram(ID3D11Device* device, WCHAR* vertexShaderPath, WCHAR* pixelShaderPath)
{
	//HRESULT	hr = S_OK;
	//ID3DBlobPtr	errorMessage = nullptr;
	//ID3DBlobPtr	vertexShaderBuffer = nullptr;
	//ID3DBlobPtr	pixelShaderBuffer = nullptr;

	//hr = D3DCompileFromFile
	//(
	//	vertexShaderPath,
	//	nullptr,
	//	nullptr,
	//	"main",
	//	"vs_5_0",
	//	D3D10_SHADER_ENABLE_STRICTNESS,
	//	0u,
	//	&vertexShaderBuffer,
	//	&errorMessage
	//);
	//if (FAILED(hr))
	//{
	//	if (errorMessage)
	//	{

	//	}
	//	else
	//	{

	//	}
	//}

	//hr = D3DCompileFromFile
	//(
	//	pixelShaderPath,
	//	nullptr,
	//	nullptr,
	//	"main",
	//	"ps_5_0",
	//	D3D10_SHADER_ENABLE_STRICTNESS,
	//	0u,
	//	&pixelShaderBuffer,
	//	&errorMessage
	//);
	//if (FAILED(hr))
	//{
	//	if (errorMessage)
	//	{

	//	}
	//	else
	//	{

	//	}
	//}

	//hr = device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), nullptr, &m_vertexShader);
	//if (FAILED(hr))
	//{
	//	return false;
	//}

	//hr = device->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(), pixelShaderBuffer->GetBufferSize(), nullptr, &m_pixelShader);
	//if (FAILED(hr))
	//{
	//	return false;
	//}

	//hr = enumInputLayout(device, vertexShaderBuffer.GetInterfacePtr());
	//if (FAILED(hr))
	//{
	//	return false;
	//}

	//enumConstantBuffers(device, vertexShaderBuffer.GetInterfacePtr());
	//enumConstantBuffers(device, pixelShaderBuffer.GetInterfacePtr());

	//enumResourcesBinding(device, vertexShaderBuffer.GetInterfacePtr());
	//enumResourcesBinding(device, pixelShaderBuffer.GetInterfacePtr());

	return false;
}

bool core::Shader::buildShaderProgram(ID3D11Device* device, const std::string& file)
{
	std::wstring	wFile;
	wFile.assign(file.begin(), file.end());

	HRESULT	hr = S_OK;
	ID3DBlobPtr	errorMessage = nullptr;
	ID3DBlobPtr	vertexShaderBuffer = nullptr;
	ID3DBlobPtr	pixelShaderBuffer = nullptr;

	hr = D3DCompileFromFile
	(
		wFile.c_str(),
		nullptr,
		nullptr,
		"vs",
		"vs_5_0",
		D3D10_SHADER_ENABLE_STRICTNESS,
		0u,
		&vertexShaderBuffer,
		&errorMessage
	);
	if (FAILED(hr))
	{
		if (errorMessage)
		{
			outputShaderErrorMessage(errorMessage, L"");
		}
		else
		{
			LogCall(LOGLEVEL_CRITICAL) << "Missing shader file" << LOGFLUSH_FLUSH;
		}
	}

	hr = D3DCompileFromFile
	(
		wFile.c_str(),
		nullptr,
		nullptr,
		"ps",
		"ps_5_0",
		D3D10_SHADER_ENABLE_STRICTNESS,
		0u,
		&pixelShaderBuffer,
		&errorMessage
	);
	if (FAILED(hr))
	{
		if (errorMessage)
		{
			outputShaderErrorMessage(errorMessage, L"");
		}
		else
		{
			LogCall(LOGLEVEL_CRITICAL) << "Missing shader file" << LOGFLUSH_FLUSH;
		}
	}

	hr = device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), nullptr, &m_vertexShader);
	if (FAILED(hr))
	{
		return false;
	}

	hr = device->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(), pixelShaderBuffer->GetBufferSize(), nullptr, &m_pixelShader);
	if (FAILED(hr))
	{
		return false;
	}

	hr = enumInputLayout(device, vertexShaderBuffer.GetInterfacePtr());
	if (FAILED(hr))
	{
		return false;
	}

	enumConstantBuffers(device, vertexShaderBuffer.GetInterfacePtr());
	enumConstantBuffers(device, pixelShaderBuffer.GetInterfacePtr());

	enumResourcesBinding(device, vertexShaderBuffer.GetInterfacePtr());
	enumResourcesBinding(device, pixelShaderBuffer.GetInterfacePtr());

	return true;
}

HRESULT core::Shader::enumInputLayout(ID3D11Device* device, ID3DBlob* vsBlob)
{
	HRESULT	hr = S_OK;

	ID3D11ShaderReflectionPtr	vertReflect = nullptr;
	hr = D3DReflect(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), IID_ID3D11ShaderReflection, (void**)&vertReflect);
	if (FAILED(hr))
	{
		return hr;
	}

	D3D11_SHADER_DESC	descVertex;
	hr = vertReflect->GetDesc(&descVertex);
	if (FAILED(hr))
	{
		return hr;
	}

	std::vector<D3D11_INPUT_ELEMENT_DESC>	inputLayoutArray;
	uint32_t								byteOffset = 0u;
	D3D11_SIGNATURE_PARAMETER_DESC			inputDesc;

	for (uint32_t i = 0; i < descVertex.InputParameters; ++i)
	{
		vertReflect->GetInputParameterDesc(i, &inputDesc);

		D3D11_INPUT_ELEMENT_DESC	ie;
		ie.SemanticName = inputDesc.SemanticName;
		ie.SemanticIndex = inputDesc.SemanticIndex;
		ie.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		ie.InputSlot = 0;
		ie.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		ie.InstanceDataStepRate = 0;
		ie.AlignedByteOffset = byteOffset;

		if (inputDesc.Mask == 1)
		{
			if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32)
			{
				ie.Format = DXGI_FORMAT_R32_UINT;
			}
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32)
			{
				ie.Format = DXGI_FORMAT_R32_SINT;
			}
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32)
			{
				ie.Format = DXGI_FORMAT_R32_FLOAT;
			}
			byteOffset += 4;
		}
		else if (inputDesc.Mask <= 3)
		{
			if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32)
			{
				ie.Format = DXGI_FORMAT_R32G32_UINT;
			}
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32)
			{
				ie.Format = DXGI_FORMAT_R32G32_SINT;
			}
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32)
			{
				ie.Format = DXGI_FORMAT_R32G32_FLOAT;
			}
			byteOffset += 8;
		}
		else if (inputDesc.Mask <= 7)
		{
			if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32)
			{
				ie.Format = DXGI_FORMAT_R32G32B32_UINT;
			}
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32)
			{
				ie.Format = DXGI_FORMAT_R32G32B32_SINT;
			}
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32)
			{
				ie.Format = DXGI_FORMAT_R32G32B32_FLOAT;
			}
			byteOffset += 12;
		}
		else if (inputDesc.Mask <= 15)
		{
			if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32)
			{
				ie.Format = DXGI_FORMAT_R32G32B32A32_UINT;
			}
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32)
			{
				ie.Format = DXGI_FORMAT_R32G32B32A32_SINT;
			}
			else if (inputDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32)
			{
				ie.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
			}
			byteOffset += 16;
		}

		inputLayoutArray.push_back(ie);
	}

	UINT	numElements = static_cast<UINT>(inputLayoutArray.size());
	hr = device->CreateInputLayout(inputLayoutArray.data(), numElements, vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), &m_layout);
	if (FAILED(hr))
	{
		return hr;
	}

	return S_OK;
}

HRESULT core::Shader::enumConstantBuffers(ID3D11Device* device, ID3DBlob* blob)
{
	HRESULT	hr = S_OK;

	ID3D11ShaderReflectionPtr	reflect = nullptr;
	hr = D3DReflect(blob->GetBufferPointer(), blob->GetBufferSize(), IID_ID3D11ShaderReflection, (void**)&reflect);
	if (FAILED(hr))
	{
		return hr;
	}

	D3D11_SHADER_DESC	desc;
	hr = reflect->GetDesc(&desc);
	if (FAILED(hr))
	{
		return hr;
	}

	for (uint32_t i = 0u; i < desc.ConstantBuffers; ++i)
	{
		ConstantBuffer* buffer = new ConstantBuffer();
		ID3D11ShaderReflectionConstantBuffer*	constBuffer = reflect->GetConstantBufferByIndex(i);
		D3D11_SHADER_BUFFER_DESC	bufferDesc;

		hr = constBuffer->GetDesc(&bufferDesc);
		if (FAILED(hr))
		{
			return hr;
		}

		buffer->Layout.Name = std::string(bufferDesc.Name);
		buffer->Layout.Description = bufferDesc;
		buffer->Slot = i;

		for (uint32_t j = 0u; j < bufferDesc.Variables; ++j)
		{
			ID3D11ShaderReflectionVariable*	variable = constBuffer->GetVariableByIndex(j);
			D3D11_SHADER_VARIABLE_DESC		varDesc;
			hr = variable->GetDesc(&varDesc);
			if (FAILED(hr))
			{
				return hr;
			}

			buffer->Layout.Variables.push_back(varDesc);

			D3D11_SHADER_TYPE_DESC		varType;
			ID3D11ShaderReflectionType*	type = variable->GetType();
			hr = type->GetDesc(&varType);
			if (FAILED(hr))
			{
				return hr;
			}

			buffer->Layout.Types.push_back(varType);
		}

		buffer->Index = static_cast<unsigned int>(m_constantBuffers.size());
		try
		{
			m_constantBuffers.push_back(buffer);
		}
		catch (const std::bad_alloc& e)
		{
			std::cerr << e.what() << '\n';
			return S_FALSE;
		}
	}

	return S_OK;
}

HRESULT core::Shader::enumResourcesBinding(ID3D11Device * device, ID3D10Blob * blob)
{
	HRESULT	hr = S_OK;

	ID3D11ShaderReflectionPtr	reflect = nullptr;
	hr = D3DReflect(blob->GetBufferPointer(), blob->GetBufferSize(), IID_ID3D11ShaderReflection, (void**)&reflect);
	if (FAILED(hr))
	{
		return hr;
	}

	D3D11_SHADER_DESC	desc;
	hr = reflect->GetDesc(&desc);
	if (FAILED(hr))
	{
		return hr;
	}

	for (uint32_t i = 0u; i < desc.BoundResources; ++i)
	{
		D3D11_SHADER_INPUT_BIND_DESC	resource;
		hr = reflect->GetResourceBindingDesc(i, &resource);
		if (FAILED(hr))
		{
			return hr;
		}

		if (resource.Type == D3D_SIT_SAMPLER ||
			resource.Type == D3D_SIT_TEXTURE)
		{
			SamplerState	samplerState;
			samplerState.InputDesc = resource;
			m_shaderInput.push_back(samplerState);
		}
	}

	return hr;
}

bool core::Shader::setVSMaterialConstantBuffers(ID3D11DeviceContext* context, ConstantBuffer* cbuffer, const Material& material)
{
	HRESULT						result;
	D3D11_MAPPED_SUBRESOURCE	mappedResource;
	void*						dataPtr;

	result = context->Map(cbuffer->Buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if (FAILED(result))
	{
		return false;
	}

	dataPtr = (void*)mappedResource.pData;

	XMVECTOR	color = XMLoadFloat4(&material.Color);

	size_t sizeOfVector = sizeof(XMVECTOR);
	memcpy(mappedResource.pData, &color, sizeOfVector);

	context->Unmap(cbuffer->Buffer, 0);

	context->VSSetConstantBuffers(cbuffer->Slot, 1, &cbuffer->Buffer);

	return true;
}

bool core::Shader::setPSMaterialConstantBuffers(ID3D11DeviceContext* context, ConstantBuffer* cbuffer, const Material& material)
{
	return true;
}

void core::Shader::outputShaderErrorMessage(ID3D10Blob* error, WCHAR* file)
{
	char*				compileError;
	unsigned long long	bufferSize;
	unsigned long long	i;
	std::ofstream		fout;

	compileError = static_cast<char*>(error->GetBufferPointer());

	bufferSize = error->GetBufferSize();

	fout.open("shader-error.txt");

	for (i = 0ul; i < bufferSize; ++i)
	{
		fout << compileError[i];
	}

	fout.close();

	error->Release();
	error = nullptr;

	LogCall(LOGLEVEL_CRITICAL) << "Error compiling shader, check shader-error.txt for message" << LOGFLUSH_FLUSH;
}
