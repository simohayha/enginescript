#pragma once

#include <d3d11_2.h>

#include <list>

#include "Component.h"
#include "Vector3.h"
#include "Quaternion.h"
#include "Matrix.h"

namespace core
{
	class TransformComponent : public Component
	{
	public:
		static int Id();

		TransformComponent(EntityId id);
		~TransformComponent();

		Vector3&	getPosition();
		Quaternion&	getRotation();
		Vector3		getEulerAngle();
		Vector3&	getScale();

		void	addChild(int childId);
		void	removeChild(int childId);
		void	setParent(int parentId);

		std::list<int>&			getChilds();
		const std::list<int>&	getChilds() const;
		int						getParent() const;

		virtual void	encodeAsset(std::ofstream& stream) const override;
		virtual void	decodeAsset(YAML::Node& node) override;

		void	getLocalTransformMatrix(Matrix& transform);
		void	getWorldTransformMatrix(Matrix& transform);

	private:
		int				m_parent;
		std::list<int>	m_childs;

		Vector3		m_position;
		Quaternion	m_rotation;
		Vector3		m_eulerAngle;
		Vector3		m_scale;
	};

	namespace scripting
	{
		void	INTERNAL_get_position(MonoObject* thisObject, Vector3* position);
		void	INTERNAL_set_position(MonoObject* thisObject, Vector3* position);
		void	INTERNAL_get_rotation(MonoObject* thisObject, Quaternion* rotation);
		void	INTERNAL_set_rotation(MonoObject* thisObject, Quaternion* rotation);
		void	INTERNAL_get_localScale(MonoObject* thisObject, Vector3* localScale);
		void	INTERNAL_set_localScale(MonoObject* thisObject, Vector3* localScale);

		void	INTERNAL_CALL_TransformDirection(MonoObject* self, Vector3* direction, Vector3* result);
		void	INTERNAL_CALL_RotateAroundInternal(MonoObject* self, Vector3* axis, float angle);
	}
}