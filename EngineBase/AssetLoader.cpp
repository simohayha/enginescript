#include <boost\uuid\uuid.hpp>
#include <boost\uuid\uuid_generators.hpp>
#include <boost\uuid\uuid_io.hpp>

#include <fstream>
#include <list>

#include "Log.h"
#include "ObjectFactory.hpp"
#include "Asset.h"
#include "TextAsset.h"
#include "Shader.h"
#include "Scene.h"
#include "AssetLoader.h"

core::AssetLoader::AssetLoader()
{
}

core::AssetLoader::~AssetLoader()
{
}

bool core::AssetLoader::loadAssets(const std::string& directory)
{
	if (!loadAssetsFromFiles(directory))
	{
		return false;
	}

	if (!linkAssets())
	{
		return false;
	}

	generateVisualStudioSolution();

	return true;
}

bool core::AssetLoader::loadAssetsFromFiles(const std::string & directory)
{
	using namespace boost::filesystem;

	try
	{
		auto	root = ObjectFactory::instantiate<Asset>();
		root->setFilename("Assets (root)");
		EngineData::setAssetDirectory(root);

		path	p(directory);

		loadDirectory(root, p);
	}
	catch (const filesystem_error& ex)
	{
		LogCall(LOGLEVEL_CRITICAL) << "Failed to load asset directory\n" << ex.what() << LOGFLUSH_FLUSH;
		return false;
	}
	return true;
}

bool core::AssetLoader::linkAssets()
{
	return true;
}

void core::AssetLoader::loadDirectory(Asset* parent, boost::filesystem::path directory)
{
	using namespace boost::filesystem;

	for (auto&& x : directory_iterator(directory))
	{
		boost::filesystem::path	path = x.path();

		if (path.extension() != ".asset")
		{
			auto	asset = process(path);
			parent->addChild(asset);
			if (is_directory(path))
			{
				loadDirectory(asset, path);
			}
		}
	}
}


core::Asset* core::AssetLoader::processSingle(boost::filesystem::path item, core::Asset* asset)
{
	using namespace boost::filesystem;

	LogCall(LOGLEVEL_DEBUG) << "Processing " << item << LOGFLUSH_FLUSH;

	asset->setFilename(item.filename().string());
	asset->setFilepath(item.parent_path().string());
	asset->setExtension(item.extension().string());

	if (is_directory(item))
	{
		asset->setAssetType(core::AssetType::E_DIRECTORY);
	}
	else
	{
		if (item.extension() == ".jpg")
		{
			asset->setAssetType(E_TEXTURE);
		}
		else if (item.extension() == ".cs")
		{
			asset->setAssetType(E_MONOSCRIPT);
		}
		else if (item.extension() == ".txt")
		{
			asset->setAssetType(E_TEXTASSET);
		}
		else if (item.extension() == ".shader")
		{
			asset->setAssetType(E_SHADER);
		}
		else if (item.extension() == ".mat")
		{
			asset->setAssetType(E_MATERIAL);
		}
		else if (item.extension() == ".engine")
		{
			asset->setAssetType(E_SCENE);
		}
	}

	std::ifstream	metaStream;
	metaStream.open(asset->getCompletePath());
	if (metaStream.is_open())
	{
		asset->decodeMeta(metaStream);
	}

	std::ifstream	assetStream;
	assetStream.open(asset->getCompletePath());
	if (assetStream.is_open())
	{
		if (EngineData::isTypeOf(asset->getId(), E_TEXTASSET) ||
			EngineData::isTypeOf(asset->getId(), E_MONOSCRIPT) ||
			EngineData::isTypeOf(asset->getId(), E_SHADER))
		{
			asset->decodeAsset(assetStream);
		}
		else
		{
			try
			{
				auto&	node = YAML::LoadAll(assetStream);
				asset->decodeAsset(node);
			}
			catch (const std::exception& e)
			{
				LogCall(LOGLEVEL_ERROR) << "Failed to process " << item << ": " << e.what() << LOGFLUSH_FLUSH;
			}
		}
	}

	return asset;
}

core::Asset* core::AssetLoader::process(boost::filesystem::path item)
{
	using namespace boost::filesystem;

	LogCall(LOGLEVEL_DEBUG) << "Processing " << item << LOGFLUSH_FLUSH;

	auto	asset = instantiateAsset(item);
	return processSingle(item, asset);
}

core::Asset* core::AssetLoader::instantiateAsset(boost::filesystem::path item)
{
	if (!item.has_extension())
	{
		return ObjectFactory::instantiate<Asset>();
	}
	else
	{
		auto	ext = item.extension();
		Asset*	asset = nullptr;

		if (ext == ".mat")
		{
			asset = ObjectFactory::instantiate<Material>();
		}
		else if (ext == ".cs" || ext == ".txt")
		{
			asset = ObjectFactory::instantiate<TextAsset>();
		}
		else if (ext == ".shader")
		{
			asset = ObjectFactory::instantiate<Shader>();
		}
		else if (ext == ".engine")
		{
			asset = ObjectFactory::instantiate<Scene>();
		}
		else
		{
			asset = ObjectFactory::instantiate<Asset>();
		}

		return asset;
	}
}

void core::AssetLoader::generateVisualStudioSolution()
{
	boost::uuids::uuid	slnUuid = boost::uuids::random_generator()();
	boost::uuids::uuid	csprojUuid = boost::uuids::random_generator()();

	std::ofstream	sln("./Project.sln");
	if (sln.is_open())
	{
		sln << "Microsoft Visual Studio Solution File, Format Version 11.00\n";
		sln << "# Visual Studio 2010\n";
		sln << "Project(\"{" << slnUuid << "}\") = \"New Project\", \"Assembly-CSharp.csproj\", \"{" << csprojUuid << "}\"\n";
		sln << "EndProject\n";
		sln << "Global\n";
		sln << "\tGlobalSection(SolutionConfigurationPlatforms) = preSolution\n";
		sln << "\t\tDebug|Any CPU = Debug|Any CPU\n";
		sln << "\t\tRelease|Any CPU = Release|Any CPU\n";
		sln << "\tEngGlobalSection\n";
		sln << "\tGlobalSection(ProjectConfigurationPlatforms) = postSolution\n";
		sln << "\t\t{" << csprojUuid << "}.Debug|Any CPU.ActiveCfg = Debug|Any CPU\n";
		sln << "\t\t{" << csprojUuid << "}.Debug|Any CPU.Build.0 = Debug|Any CPU\n";
		sln << "\t\t{" << csprojUuid << "}.Release|Any CPU.ActiveCfg = Release|Any CPU\n";
		sln << "\t\t{" << csprojUuid << "}.Release|Any CPU.Build.0 = Release|Any CPU\n";
		sln << "\tEndGlobalSection\n";
		sln << "\tGlobalSection(SolutionProperties) = preSolution\n";
		sln << "\t\tHideSolutionNode = FALSE\n";
		sln << "\tEndGlobalSection\n";
		sln << "EndGlobal\n";
	}

	std::ofstream	csproj("./Assembly-CSharp.csproj");
	if (csproj.is_open())
	{
		csproj << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		csproj << "<Project ToolsVersion = \"4.0\" DefaultTargets = \"Build\" xmlns = \"http://schemas.microsoft.com/developer/msbuild/2003\">\n";

		csproj << "\t<PropertyGroup>\n";
		csproj << "\t\t<Configuration Condition=\" '$(Configuration)' == '' \">Debug</Configuration>\n";
		csproj << "\t\t<Platform Condition=\" '$(Platform)' == '' \">AnyCPU</Platform>\n";
		csproj << "\t\t<ProductVersion>10.0.20506</ProductVersion>\n";
		csproj << "\t\t<SchemaVersion>2.0</SchemaVersion>\n";
		csproj << "\t\t<RootNamespace></RootNamespace>\n";
		csproj << "\t\t<ProjectGuid>{" << csprojUuid << "}</ProjectGuid>\n";
		csproj << "\t\t<OutputType>Library</OutputType>\n";
		csproj << "\t\t<AppDesignerFolder>Properties</AppDesignerFolder>\n";
		csproj << "\t\t<AssemblyName>Assembly-CSharp</AssemblyName>\n";
		csproj << "\t\t<TargetFrameworkVersion>v4.6.2</TargetFrameworkVersion>\n";
		csproj << "\t\t<FileAlignment>512</FileAlignment>\n";
		csproj << "\t\t<BaseDirectory>Assets</BaseDirectory>\n";
		csproj << "\t</PropertyGroup>\n";

		csproj << "\t<PropertyGroup Condition=\" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' \">\n";
		csproj << "\t\t<DebugSymbols>true</DebugSymbols>\n";
		csproj << "\t\t<DebugType>full</DebugType>\n";
		csproj << "\t\t<Optimize>false</Optimize>\n";
		csproj << "\t\t<OutputPath>Temp\\bin\\Debug\\</OutputPath>\n";
		csproj << "\t\t<DefineConstants>DEBUG</DefineConstants>\n";
		csproj << "\t\t<ErrorReport>prompt</ErrorReport>\n";
		csproj << "\t\t<WarningLevel>4</WarningLevel>\n";
		csproj << "\t\t<NoWarn>0169</NoWarn>\n";
		csproj << "\t</PropertyGroup>\n";

		csproj << "\t<PropertyGroup Condition=\" '$(Configuration)|$(Platform)' == 'Release|AnyCPU' \">\n";
		csproj << "\t\t<DebugType>pdbonly</DebugType>\n";
		csproj << "\t\t<Optimize>true</Optimize>\n";
		csproj << "\t\t<OutputPath>Temp\\bin\\Release\\</OutputPath>\n";
		csproj << "\t\t<ErrorReport>prompt</ErrorReport>\n";
		csproj << "\t\t<WarningLevel>4</WarningLevel>\n";
		csproj << "\t\t<NoWarn>0169</NoWarn>\n";
		csproj << "\t</PropertyGroup>\n";

		csproj << "\t<ItemGroup>\n";
		csproj << "\t\t<Reference Include=\"System\" />\n";
		csproj << "\t\t<Reference Include=\"System.XML\" />\n";
		csproj << "\t\t<Reference Include=\"System.Core\" />\n";
		csproj << "\t\t<Reference Include=\"System.Runtime.Serialization\" />\n";
		csproj << "\t\t<Reference Include=\"System.Xml.Linq\" />\n";
		csproj << "\t</ItemGroup>\n";

		csproj << "\t<ItemGroup>\n";
		std::list<Asset*>	list;
		EngineData::getAllObjectOfType(list, E_MONOSCRIPT);
		for (auto& item : list)
		{
			csproj << "\t\t<Compile Include=\"" << item->getFilepath() << "/" << item->getFilename() << "\" />\n";
		}
		csproj << "\t</ItemGroup>\n";

		csproj << "\t<Import Project=\"$(MSBuildToolsPath)\\Microsoft.CSharp.targets\" />\n";

		csproj << "</Project>\n";
	}
}
