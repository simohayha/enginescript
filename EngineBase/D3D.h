#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <wrl/client.h>

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")

#include <d3d11_2.h>
#include <DirectXMath.h>
#include <SimpleMath.h>

using namespace DirectX;
using namespace Microsoft::WRL;

namespace core
{
	class D3D
	{
	public:
		D3D();
		~D3D();

		bool	initialize(HWND hwnd, int screenWidth, int screenHeight, bool vsync, bool fullscreen, float screenDepth, float screenNear);
		void	shutdown();

		void	beginScene(DirectX::SimpleMath::Color& clearColor);
		void	endScene();

		ID3D11Device*			getDevice();
		ID3D11DeviceContext*	getDeviceContext();

		void			getProjectionMatrix(XMMATRIX& matrix);
		void			getProjectionMatrix(XMFLOAT4X4& matrix);
		D3D_DRIVER_TYPE	getDriverType() const;

		ID3D11RasterizerState*	getWireframeState() const;
		ID3D11RasterizerState*	getSolidState() const;

		void	getVideoCardInfo(char* cardName, int& memory);

		void	resize(int height, int width);

	private:
		XMFLOAT4X4		m_projectionMatrix;

		unsigned int	m_numerator;
		unsigned int	m_denominator;
		int				m_screenWidth;
		int				m_screenHeight;
		bool			m_vsyncEnabled;
		int				m_videoCardMemory;
		char			m_videoCardDescription[128];

		ComPtr<IDXGISwapChain>			m_swapChain;
		ComPtr<ID3D11Device>			m_device;
		ComPtr<ID3D11DeviceContext>		m_deviceContext;

		ComPtr<ID3D11RenderTargetView>	m_renderTargetView;
		ComPtr<ID3D11Texture2D>			m_depthStencilBuffer;
		ComPtr<ID3D11DepthStencilState>	m_depthStencilState;
		ComPtr<ID3D11DepthStencilView>	m_depthStencilView;

		ComPtr<ID3D11RasterizerState>	m_wireFrame;
		ComPtr<ID3D11RasterizerState>	m_solid;

		ComPtr<ID3D11DepthStencilState>	m_depthDisabledStencilState;

		ComPtr<ID3D11BlendState>		m_alphaEnableBlendingState;
		ComPtr<ID3D11BlendState>		m_alphaDisableBlendingState;

		bool	checkVideoCard();
		bool	initSwapChain(HWND hwnd, bool fullscreen);
		bool	initDepthBuffer();
		bool	init2DStencil();
		bool	initBlendStates();
		bool	initRasterizer();
		void	initViewPort();
		void	initProjection(float screenDepth, float screenNear);
	};
}
