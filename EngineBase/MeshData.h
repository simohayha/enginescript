#pragma once

#include <vector>

#include "Vertex.h"

namespace core
{
	struct MeshData
	{
	public:
		std::vector<Vertex>			Vertices;
		std::vector<unsigned int>	Indices;
	};
}