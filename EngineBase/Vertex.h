#pragma once

#include <DirectXMath.h>

using namespace DirectX;

namespace core
{
	struct Vertex
	{
	public:
		Vertex();
		Vertex(const XMFLOAT4& p, const XMFLOAT3& n, const XMFLOAT3& t, const XMFLOAT2& uv);
		Vertex(const XMFLOAT3& p);
		Vertex(
			float px, float py, float pz,
			float nx, float ny, float nz,
			float tx, float ty, float tz,
			float u, float v
		);

		XMFLOAT4	Position;
		XMFLOAT3	Normal;
		XMFLOAT3	Tangent;
		XMFLOAT2	UV;
	};
}
