#include "Log.h"
#include "EngineData.hpp"

std::map<EntityId, core::Object*>						core::EngineData::m_objects;
std::list<EntityId>										core::EngineData::m_availableObjects;
std::list<EntityId>										core::EngineData::m_usedObjects;
std::map<size_t, core::System<core::Component>>			core::EngineData::m_components;
core::Asset*											core::EngineData::m_assetDirectory;
std::map<core::AssetType, std::vector<core::AssetType>>	core::EngineData::m_assetAncestor;
std::map<unsigned long long, EntityId>					core::EngineData::m_fileIdToEntity;

void core::EngineData::save()
{
	std::list<Asset*>	assets;
	EngineData::getAllObjectOfTypeChildOf<Asset>(assets, E_ASSET);
	for (auto& asset : assets)
	{
		//if (asset->isDirty())
		//{
			std::ofstream	outStreamAsset;
			outStreamAsset.open(asset->getCompletePath());
			if (outStreamAsset.is_open())
			{
				asset->encodeAsset(outStreamAsset);
				outStreamAsset.close();
			}

			std::ofstream	outStreamMeta;
			outStreamMeta.open(asset->getCompletePath() + ".asset");
			if (outStreamMeta.is_open())
			{
				asset->encodeMeta(outStreamMeta);
				outStreamMeta.close();
			}
		//}
	}

	assets = assets;
}

void core::EngineData::addFileIdToEntity(unsigned long long fileId, EntityId entityId)
{
	m_fileIdToEntity[fileId] = entityId;
}

core::EngineData::EngineData()
{
}

core::EngineData::~EngineData()
{
}

bool core::EngineData::initialize()
{
	try
	{
		m_availableObjects.resize(getMaxObjectCount());
	}
	catch (const std::bad_alloc& e)
	{
		LogCall(LOGLEVEL_CRITICAL) << e.what() << LOGFLUSH_FLUSH;
		return false;
	}

	int	i = 1;
	for (auto& it : m_availableObjects)
	{
		it = i++;
	}

	m_assetAncestor[E_ASSET].push_back(E_OBJECT);
	m_assetAncestor[E_GAMEOBjECT].push_back(E_OBJECT);
	m_assetAncestor[E_COMPONENT].push_back(E_OBJECT);
	m_assetAncestor[E_MATERIAL].push_back(E_ASSET);
	m_assetAncestor[E_TEXTURE].push_back(E_ASSET);
	m_assetAncestor[E_DIRECTORY].push_back(E_ASSET);
	m_assetAncestor[E_SHADER].push_back(E_ASSET);
	m_assetAncestor[E_MONOSCRIPT].push_back(E_ASSET);
	m_assetAncestor[E_SCENE].push_back(E_ASSET);
	m_assetAncestor[E_TRANSFORM].push_back(E_COMPONENT);
	m_assetAncestor[E_RENDERER].push_back(E_COMPONENT);
	m_assetAncestor[E_CAMERA].push_back(E_COMPONENT);
	m_assetAncestor[E_IDENTITY].push_back(E_COMPONENT);
	m_assetAncestor[E_MESHFILTER].push_back(E_COMPONENT);

	return true;
}

core::Object* core::EngineData::getObject(EntityId id)
{
	return m_objects[id];
}

void core::EngineData::addObject(Object* object)
{
	m_objects[object->getId()] = object;
}

constexpr int core::EngineData::getMaxObjectCount()
{
	return 65565;
}

EntityId core::EngineData::reserveId()
{
	EntityId	available = m_availableObjects.front();
	m_availableObjects.pop_front();
	return available;
}

void core::EngineData::releaseId(EntityId id)
{
	delete m_objects[id];
	m_objects[id] = nullptr;
	m_usedObjects.remove(id);
}

void core::EngineData::getAllComponents(std::vector<Component*>& components, int gameObjectId)
{
	for (auto& system : m_components)
	{
		auto	it = system.second.components().find(gameObjectId);
		if (it != system.second.components().end())
		{
			components.push_back(system.second.components()[gameObjectId]);
		}
	}
}

core::Asset& core::EngineData::getAssetDirectory()
{
	return *m_assetDirectory;
}

core::Asset* core::EngineData::getAssetDirectoryPtr()
{
	return m_assetDirectory;
}

void core::EngineData::setAssetDirectory(Asset* directory)
{
	m_assetDirectory = directory;
}

bool core::EngineData::isTypeOf(int objectId, core::AssetType type)
{
	return isTypeOf(getObject(objectId), type);
}

bool core::EngineData::isTypeChildOf(int objectId, core::AssetType type)
{
	return isTypeChildOf(getObject(objectId), type);
}

bool core::EngineData::isTypeOf(core::Object* object, core::AssetType type)
{
	if (!object)
	{
		return false;
	}
	return object->getAssetType() == type;
}

bool core::EngineData::isTypeChildOf(core::Object* object, core::AssetType type)
{
	if (!object)
	{
		return false;
	}

	auto	arrayOfAncestor = m_assetAncestor[object->getAssetType()];
	for (auto ancestor : arrayOfAncestor)
	{
		if (ancestor == type)
		{
			return true;
		}
		else
		{
			return isTypeChildOf(object, ancestor);
		}
	}

	return false;
}