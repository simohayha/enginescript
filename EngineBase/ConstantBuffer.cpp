#include "ConstantBuffer.h"

core::ConstantBuffer::ConstantBuffer() :
	Index(0u),
	Buffer(nullptr),
	Slot(0u)
{
}

core::ConstantBuffer::~ConstantBuffer()
{
	if (Buffer)
	{
		Buffer->Release();
		Buffer = nullptr;
	}
}

core::DynamicBuffer::DynamicBuffer(ConstantBuffer* buffer) :
	Buffer(buffer)
{
}

core::DynamicBuffer::~DynamicBuffer()
{
}
