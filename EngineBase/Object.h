#pragma once

#include <string>

#include "Entity.h"
#include "AssetType.h"

namespace core
{
	class Object
	{
	public:
		Object();
		virtual ~Object();

		void		setId(EntityId id);
		EntityId	getId() const;

		void		setAssetType(AssetType type);
		AssetType	getAssetType() const;

		virtual std::string	getName() const;
		virtual void		setName(const std::string& name);

		template <typename T>
		T*			as()
		{
			return static_cast<T*>(this);
		}

		bool	isDirty() const;
		void	setIsDirty(bool dirty);

		unsigned long long	getFileId() const;
		void				setFileId(unsigned long long fileId);

	protected:
		EntityId			m_id;
		AssetType			m_assetType;
		unsigned long long	m_fileId;
		bool				m_dirty;
	};
}