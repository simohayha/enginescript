#pragma once

#include <vector>

#include "Component.h"

#include <mono/metadata/metadata.h>

namespace core
{
	class Scripting;
	class AbstractScript;
}

namespace core
{
	class ScriptingComponent : public Component
	{
	public:
		ScriptingComponent(EntityId id, Scripting* scripting);
		~ScriptingComponent();

		bool	initialize();

		bool	attachNativeScript(const std::string& script);
		bool	attachMonoScript(const std::string& script);

		void	callStart();
		void	callUpdate();

		MonoObject*	getGameObject() const;

		virtual void	encodeAsset(std::ofstream& stream) const override;
		virtual void	decodeAsset(std::vector<YAML::Node>& nodes) override;

	private:
		Scripting*						m_scripting;
		MonoObject*						m_gameObject;
		std::vector<AbstractScript*>	m_scripts;
	};
}