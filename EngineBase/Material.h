#pragma once

#include <vector>

#include <DirectXMath.h>

#include "Asset.h"

namespace core
{
	class Shader;
	struct Resource;
}

namespace core
{
	class Material : public Asset
	{
	public:
		enum class Type : char
		{
			E_VECTOR,
			E_TEXTURE,
			E_INT,
			E_FLOAT,
		};

		Material();
		virtual ~Material();

		virtual void	encodeAsset(std::ofstream& stream) const override;
		virtual void	encodeMeta(std::ofstream& stream) const override;

		virtual void	decodeMeta(std::ifstream& stream) override;

		DirectX::XMFLOAT4		Color;
		std::vector<Resource*>	Resources;

		Shader*	m_shader;
	};
}