#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <exception>
#include <cstdint>

namespace core
{
	class StepTimer
	{
	public:
		StepTimer();

		uint64_t	getElapsedTicks() const;
		double		getElapsedSeconds() const;

		uint64_t	getTotalTicks() const;
		double		getTotalSeconds() const;

		uint32_t	getFrameCount() const;

		uint32_t	getFramesPerSecond() const;

		void		setFixedTimeStep(bool isFixedTimestep);

		void		setTargetElapsedTicks(uint64_t targetElapsed);
		void		setTargetElapsedSeconds(double targetElapsed);

		static const uint64_t	TicksPerSecond = 10000000u;

		static double	TicksToSeconds(uint64_t ticks);
		static uint64_t	SecondsToTicks(double seconds);

		void	resetElapsedTime();

		template <typename TUpdate>
		void	tick(const TUpdate& update)
		{
			LARGE_INTEGER	currentTime;

			if (!QueryPerformanceCounter(&currentTime))
			{
				throw std::exception("QueryPerformanceCounter");
			}

			uint64_t	timeDelta = currentTime.QuadPart - m_qpcLastTime.QuadPart;

			m_qpcLastTime = currentTime;
			m_qpcSecondCounter += timeDelta;

			if (timeDelta > m_qpcMaxDelta)
			{
				timeDelta = m_qpcMaxDelta;
			}

			timeDelta *= TicksPerSecond;
			timeDelta /= m_qpcFrequency.QuadPart;

			uint32_t lastFrameCount = m_frameCount;

			if (m_isFixedTimeStep)
			{
				if (abs(static_cast<int64_t>(timeDelta - m_targetElapsedTicks)) < TicksPerSecond / 4000)
				{
					timeDelta = m_targetElapsedTicks;
				}

				m_leftOverTicks += timeDelta;

				while (m_leftOverTicks >= m_targetElapsedTicks)
				{
					m_elapsedTicks = m_targetElapsedTicks;
					m_totalTicks += m_targetElapsedTicks;
					m_leftOverTicks -= m_targetElapsedTicks;
					m_frameCount++;

					update();
				}
			}
			else
			{
				m_elapsedTicks = timeDelta;
				m_totalTicks += timeDelta;
				m_leftOverTicks = 0;
				m_frameCount++;

				update();
			}

			if (m_frameCount != lastFrameCount)
			{
				m_framesThisSecond++;
			}

			if (m_qpcSecondCounter >= static_cast<uint64_t>(m_qpcFrequency.QuadPart))
			{
				m_framesPerSecond = m_framesThisSecond;
				m_framesThisSecond = 0;
				m_qpcSecondCounter %= m_qpcFrequency.QuadPart;
			}
		}

	private:
		LARGE_INTEGER	m_qpcFrequency;
		LARGE_INTEGER	m_qpcLastTime;
		uint64_t		m_qpcMaxDelta;

		uint64_t		m_elapsedTicks;
		uint64_t		m_totalTicks;
		uint64_t		m_leftOverTicks;

		uint32_t		m_frameCount;
		uint32_t		m_framesPerSecond;
		uint32_t		m_framesThisSecond;
		uint64_t		m_qpcSecondCounter;

		bool			m_isFixedTimeStep;
		uint64_t		m_targetElapsedTicks;
	};
}
