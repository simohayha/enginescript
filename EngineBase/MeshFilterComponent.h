#pragma once

#include "Component.h"

namespace core
{
	class Model3D;

	class MeshFilterComponent : public Component
	{
	public:
		MeshFilterComponent(EntityId id);
		~MeshFilterComponent();

		Model3D*	getModel();
		void		setModel(Model3D* model);

		virtual void	encodeAsset(std::ofstream& stream) const override;

	private:
		Model3D*	m_model;
	};
}