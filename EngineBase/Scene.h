#pragma once

#include <map>

#include "GameObject.h"
#include "Asset.h"

namespace core
{
	class Scene : public Asset
	{
	public:
		Scene();
		virtual ~Scene();

		virtual void	encodeAsset(std::ofstream& stream) const override;
		virtual void	decodeAsset(std::vector<YAML::Node>& nodes) override;

		void	addGameObject(GameObject* object);
		void	removeGameObject(GameObject* object);

	private:
		std::map<int, GameObject*>	m_gameObjects;
	};
}
