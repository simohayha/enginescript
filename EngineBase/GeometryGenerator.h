#pragma once

#include "MeshData.h"

namespace core
{
	class GeometryGenerator
	{
	public:
		enum class Shape : char
		{
			E_BOX,
			E_GRID,
		};

		void	createBox(float witdh, float height, float depth, MeshData& meshData);

		void	createGrid(float width, float depth, unsigned int m, unsigned int n, MeshData& meshData);
	};
}
