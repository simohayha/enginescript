#include <sstream>

#include "Log.h"
#include "Asset.h"

core::Asset::Asset() :
	m_filename(""),
	m_filepath(""),
	m_extension(""),
	m_parent(nullptr)
{
}

core::Asset::~Asset()
{
}

const std::string& core::Asset::getFilename() const
{
	return m_filename;
}

const std::string& core::Asset::getFilepath() const
{
	return m_filepath;
}

const std::string& core::Asset::getExtension() const
{
	return m_extension;
}

const std::string core::Asset::getCompletePath() const
{
	std::stringstream	ss;
	ss << getFilepath() << "/" << getFilename();
	return ss.str();
}

const std::string core::Asset::getCompleteMetaPath() const
{
	std::stringstream	ss;
	ss << getFilepath() << "/" << getFilename() << ".asset";
	return ss.str();
}

void core::Asset::setFilename(std::string filename)
{
	m_filename = filename;
}

void core::Asset::setFilepath(std::string filepath)
{
	m_filepath = filepath;
}

void core::Asset::setExtension(std::string extension)
{
	m_extension = extension;
}

core::Asset* core::Asset::getParent() const
{
	return m_parent;
}

void core::Asset::setParent(Asset* parent)
{
	m_parent = parent;
}

std::list<core::Asset*>& core::Asset::getChilds()
{
	return m_childs;
}

void core::Asset::addChild(Asset* asset)
{
	LogCall(LOGLEVEL_DEBUG) << "Add " << asset->getFilename() << " (" << &asset->getFilename() << ") to " << getFilename() << " (" << &getFilename() << ")" << LOGFLUSH_FLUSH;
	m_childs.push_back(asset);
	asset->setParent(this);
}

void core::Asset::removeChild(Asset* asset)
{
	m_childs.remove(asset);
}

std::string core::Asset::getName() const
{
	return m_filename;
}

void core::Asset::setName(const std::string& name)
{
	m_filename = name;
}
