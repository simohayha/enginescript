#include "StepTimer.h"

core::StepTimer::StepTimer() :
	m_elapsedTicks(0),
	m_totalTicks(0),
	m_leftOverTicks(0),
	m_frameCount(0),
	m_framesPerSecond(0),
	m_framesThisSecond(0),
	m_qpcSecondCounter(0),
	m_isFixedTimeStep(false),
	m_targetElapsedTicks(TicksPerSecond / 60)
{
	if (!QueryPerformanceFrequency(&m_qpcFrequency))
	{
		throw std::exception("QueryPerformanceFrequency");
	}

	if (!QueryPerformanceCounter(&m_qpcLastTime))
	{
		throw std::exception("QueryPerformanceCounter");
	}

	m_qpcMaxDelta = m_qpcFrequency.QuadPart / 10;
}

uint64_t core::StepTimer::getElapsedTicks() const
{
	return m_elapsedTicks;
}

double core::StepTimer::getElapsedSeconds() const
{
	return TicksToSeconds(m_elapsedTicks);
}

uint64_t core::StepTimer::getTotalTicks() const
{
	return m_totalTicks;
}

double core::StepTimer::getTotalSeconds() const
{
	return TicksToSeconds(m_totalTicks);
}

uint32_t core::StepTimer::getFrameCount() const
{
	return m_frameCount;
}

uint32_t core::StepTimer::getFramesPerSecond() const
{
	return m_framesPerSecond;
}

void core::StepTimer::setFixedTimeStep(bool isFixedTimestep)
{
	m_isFixedTimeStep = isFixedTimestep;
}

void core::StepTimer::setTargetElapsedTicks(uint64_t targetElapsed)
{
	m_targetElapsedTicks = targetElapsed;
}

void core::StepTimer::setTargetElapsedSeconds(double targetElapsed)
{
	m_targetElapsedTicks = SecondsToTicks(targetElapsed);
}

double core::StepTimer::TicksToSeconds(uint64_t ticks)
{
	return static_cast<double>(ticks) / TicksPerSecond;
}

uint64_t core::StepTimer::SecondsToTicks(double seconds)
{
	return static_cast<uint64_t>(seconds * TicksPerSecond);
}

void core::StepTimer::resetElapsedTime()
{
	if (!QueryPerformanceCounter(&m_qpcLastTime))
	{
		throw std::exception("QueryPerformanceCounter");
	}

	m_leftOverTicks = 0;
	m_framesPerSecond = 0;
	m_framesThisSecond = 0;
	m_qpcSecondCounter = 0;
}
