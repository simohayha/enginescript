#pragma once

#include "Component.h"
#include "Graphic.h"

#include "System.h"
#include "Material.h"

namespace core
{
	class TransformComponent;
	class Model3D;

	enum RenderMode : unsigned short
	{
		E_RENDERMODE_SOLID,
		E_RENDERMODE_WIREFRAME,
	};

	class RenderComponent : public Component
	{
	public:
		static int Id();

		RenderComponent(EntityId id/*, System<TransformComponent>& transformSystem*/);
		~RenderComponent();

		bool					render(Graphic& graphics);
		Material*				getMaterial();
		void					setMaterial(Material* material);
		void					setRenderMode(RenderMode renderMode);
		RenderMode				getRenderMode() const;

		virtual void	encodeAsset(std::ofstream& stream) const override;

	private:
		Material*				m_material;
		RenderMode				m_renderMode;
	};
}
