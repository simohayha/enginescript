#pragma once

#include <vector>

#include <boost\filesystem.hpp>

namespace core
{
	class Asset;
}

namespace core
{
	class AssetLoader
	{
	public:
		AssetLoader();
		~AssetLoader();

		bool	loadAssets(const std::string& directory);
		Asset*	processSingle(boost::filesystem::path item, core::Asset* asset);
		Asset*	process(boost::filesystem::path item);

	private:

		bool	loadAssetsFromFiles(const std::string& directory);
		bool	linkAssets();

		void	loadDirectory(Asset* parent, boost::filesystem::path directory);
		Asset*	instantiateAsset(boost::filesystem::path item);
		void	generateVisualStudioSolution();
	};
}