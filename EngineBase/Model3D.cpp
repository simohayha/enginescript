#include <memory>

#include "Model3D.h"

core::Model3D::Model3D()
{
}

core::Model3D::~Model3D()
{
}

void core::Model3D::createFromShape(const GeometryGenerator::Shape shape)
{
	GeometryGenerator	geoGen;

	if (shape == GeometryGenerator::Shape::E_BOX)
	{
		geoGen.createBox(1.0f, 1.0f, 1.0f, m_meshData);
	}
	else if (shape == GeometryGenerator::Shape::E_GRID)
	{
		geoGen.createGrid(100.0f, 100.0f, 100, 100, m_meshData);
	}
}

bool core::Model3D::initialize(ID3D11Device* device)
{
	bool	result;

	result = initializeBuffers(device);
	if (!result)
	{
		return false;
	}

	return true;
}

void core::Model3D::shutdown()
{
	shutdownBuffers();
}

void core::Model3D::render(ID3D11DeviceContext* context)
{
	renderBuffers(context);
}

unsigned int core::Model3D::getIndexCount() const
{
	return static_cast<unsigned int>(m_meshData.Indices.size());
}

ID3D11Buffer** core::Model3D::getVertexBuffer()
{
	return &m_buffers.Vertex;
}

ID3D11Buffer** core::Model3D::getIndexBuffer()
{
	return &m_buffers.Index;
}

bool core::Model3D::initializeBuffers(ID3D11Device* device)
{
	if (m_meshData.Vertices.size() == 0 || m_meshData.Indices.size() == 0)
	{
		return false;
	}

	D3D11_BUFFER_DESC		vertexBufferDesc;
	D3D11_BUFFER_DESC		indexBufferDesc;
	D3D11_SUBRESOURCE_DATA	vertexData;
	D3D11_SUBRESOURCE_DATA	indexData;
	HRESULT					result;

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = static_cast<UINT>(sizeof(Vertex) * m_meshData.Vertices.size());
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	vertexData.pSysMem = &m_meshData.Vertices[0];
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_buffers.Vertex);
	if (FAILED(result))
	{
		return false;
	}

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = static_cast<UINT>(sizeof(unsigned long) * m_meshData.Indices.size());
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	indexData.pSysMem = &m_meshData.Indices[0];
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_buffers.Index);
	if (FAILED(result))
	{
		return false;
	}

	return true;
}

void core::Model3D::shutdownBuffers()
{
	if (m_buffers.Index)
	{
		m_buffers.Index->Release();
		m_buffers.Index = nullptr;
	}

	if (m_buffers.Vertex)
	{
		m_buffers.Vertex->Release();
		m_buffers.Vertex = nullptr;
	}
}

void core::Model3D::renderBuffers(ID3D11DeviceContext* context)
{
	assert(m_buffers.Vertex != nullptr);
	assert(m_buffers.Index != nullptr);

	unsigned int	stride;
	unsigned int	offset;

	stride = sizeof(Vertex);
	offset = 0;

	context->IASetVertexBuffers(0, 1, &m_buffers.Vertex, &stride, &offset);

	context->IASetIndexBuffer(m_buffers.Index, DXGI_FORMAT_R32_UINT, 0);

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}
