#pragma once

#include "mono/metadata/class.h"

namespace core
{
	namespace mono
	{
		namespace klass
		{
			extern MonoClass*	Object;
			extern MonoClass*	GameObject;
			extern MonoClass*	Component;
			extern MonoClass*	WorldPositionComponent;
			extern MonoClass*	RenderComponent;
		}
	}
}