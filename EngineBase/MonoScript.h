#pragma once

#include <vector>

#include <mono/metadata/metadata.h>
#include <mono/metadata/object.h>

#include "AbstractScript.h"

namespace core
{
	class MonoScript : public AbstractScript
	{
	public:
		enum class Functions : size_t
		{
			// Used as indexes ! Do not change the numbers
			FUNCTION_START = 0,
			FUNCTION_UPDATE,
			FUNCTION_MAX
		};

		MonoScript(MonoClass* scriptClass, MonoObject* scriptObject, MonoObject* attachedGameObject);
		~MonoScript();

		virtual bool	initialize() override;

	protected:
		virtual void	onStart() override;
		virtual void	onUpdate() override;

	private:
		MonoClass*					m_scriptClass;
		MonoObject*					m_scriptObject;
		MonoObject*					m_attachedGameObject;
		std::vector<MonoMethod*>	m_methods;

		MonoMethod*	getMethod(Functions function) const;
		void		invoke(MonoMethod* method, void** args) const;
	};

	namespace scripting
	{
		void	INTERNAL_set_enabled(MonoObject* thisObject, bool enabled);
		bool	INTERNAL_get_enabled(MonoObject* thisObject);
	}
}