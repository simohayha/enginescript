#include "Quaternion.h"

core::Quaternion::Quaternion() :
	DirectX::SimpleMath::Quaternion()
{
}

core::Quaternion::Quaternion(float x, float y, float z, float w) :
	DirectX::SimpleMath::Quaternion(x, y, z, w)
{
}

core::Quaternion::Quaternion(const DirectX::SimpleMath::Quaternion& q) :
	DirectX::SimpleMath::Quaternion(q)
{
}

core::Quaternion::~Quaternion()
{
}

void core::Quaternion::toEulerAngle(Vector3& eulerAngles)
{
	float	ysqr = y * y;

	// roll (x-axis rotation)
	float	t0 = +2.0f * (w * x + y * z);
	float	t1 = +1.0f - 2.0f * (x * x + ysqr);
	float	roll = std::atan2(t0, t1);

	// pitch (y-axis rotation)
	float	t2 = +2.0f * (w * y - z * x);
	t2 = t2 > 1.0f ? 1.0f : t2;
	t2 = t2 < -1.0f ? -1.0f : t2;
	float	pitch = std::asin(t2);

	// yaw (z-axis rotation)
	float	t3 = +2.0f * (w * z + x * y);
	float	t4 = +1.0f - 2.0f * (ysqr + z * z);
	float	yaw = std::atan2(t3, t4);

	eulerAngles.x = pitch;
	eulerAngles.y = yaw;
	eulerAngles.z = roll;
}

void core::scripting::INTERNAL_CALL_Internal_FromEulerRad(Vector3* euler, Quaternion* value)
{
	*value = DirectX::SimpleMath::Quaternion::CreateFromYawPitchRoll(euler->x, euler->y, euler->z);
}

void core::scripting::INTERNAL_CALL_Inverse(Quaternion* rotation, Quaternion* value)
{
	rotation->Inverse(*value);
}
