#include "Shader.h"
#include "Material.h"

core::Material::Material() :
	Color(1.0f, 1.0f, 1.0f, 1.0f)
{
	setAssetType(E_MATERIAL);
}

core::Material::~Material()
{
}

void core::Material::encodeAsset(std::ofstream& stream) const
{
	YAML::Emitter	out;

	out << YAML::BeginMap;
	out << YAML::Key << "Material";
	out << YAML::BeginMap;
	out << YAML::Key << "m_shader";
	if (!m_shader)
	{
		out << YAML::Key << "fileId" << YAML::Value << 0;
	}
	else
	{
		out << YAML::Key << "fileId" << YAML::Value << m_shader->getFileId();
	}
	out << YAML::EndMap;
	out << YAML::EndMap;

	stream << out.c_str();
}

void core::Material::encodeMeta(std::ofstream& stream) const
{
	YAML::Emitter	out;
	out << YAML::BeginMap;
	out << YAML::Key << "m_fileId" << YAML::Key << m_fileId;
	out << YAML::EndMap;

	stream << out.c_str();
}

void core::Material::decodeMeta(std::ifstream& stream)
{
}
