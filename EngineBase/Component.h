#pragma once

#include "Object.h"
#include "Entity.h"
#include "Serializable.h"

#include "mono/metadata/class.h"
#include "mono/metadata/object.h"

namespace core
{
	class Component : public Object, public Serializable
	{
	public:
		static MonoClass*	Class;

		Component(EntityId id);
		virtual ~Component();

		EntityId	getOwner() const;

	protected:
		EntityId	m_owner;
	};
}