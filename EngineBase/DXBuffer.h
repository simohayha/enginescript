#pragma once

#include <d3d11.h>

namespace core
{
	struct DXBuffer
	{
	public:
		DXBuffer();
		~DXBuffer();

		ID3D11Buffer*	Vertex;
		ID3D11Buffer*	Index;
	};
}