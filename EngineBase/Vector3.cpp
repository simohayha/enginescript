#include "Vector3.h"

core::Vector3::Vector3() : 
	DirectX::SimpleMath::Vector3()
{
}

core::Vector3::Vector3(float x) :
	DirectX::SimpleMath::Vector3(x, x, x)
{
}


core::Vector3::Vector3(float x, float y, float z) :
	DirectX::SimpleMath::Vector3(x, y, z)
{
}

core::Vector3::Vector3(const DirectX::SimpleMath::Vector3& v) :
	DirectX::SimpleMath::Vector3(v)
{
}

core::Vector3::~Vector3()
{
}