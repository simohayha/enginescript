#pragma once

#include <string>

#include "Export.h"

namespace core
{
	class Log;
}

class UNMANAGED_API UnmanagedLog
{
public:
	class UNMANAGED_API Feedback
	{
	public:
		virtual void	OnFlush(int value) = 0;
	};

	UnmanagedLog(Feedback* feedback);

	void		getValue(char* buffer);
	size_t		getLen();
	core::Log*	getLog();
	void		setLog(core::Log* log);
	Feedback*	getFeedback();
	void		setFeedback(Feedback* feedback);

private:
	core::Log*	m_log;
	Feedback*	m_feedback;
};