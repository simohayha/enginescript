#include <algorithm>
#include <functional>

#include "Input.h"
#include "TransformComponent.h"
#include "MonoScript.h"

#include "Log.h"
#include "Vector3.h"
#include "Quaternion.h"
#include "Game.h"
#include "EngineData.hpp"
#include "Scripting.h"

core::Scripting*	instance = nullptr;

core::Scripting::Scripting() :
	m_domain(nullptr)
{
	instance = this;
}

core::Scripting::~Scripting()
{
}

struct CastHelper
{
	void*	t;
	void*	onePointerFurtherTanT;
};

bool core::Scripting::initialize()
{
	bool	ok = false;

	//mono_set_dirs("C:\\Program Files\\Mono\\lib", "C:\\Program Files\\Mono\\etc");
	mono_set_dirs("Mono\\lib", "Mono\\etc");
	mono_config_parse(nullptr);

	m_domain = mono_jit_init_version("system", "v4.0.30319");
	if (!m_domain)
	{
		return false;
	}

	m_assembliesMeta.resize(2);

	ok = loadAssembly(m_assembliesMeta[0], "Scripting.dll");
	if (!ok)
	{
		return false;
	}
	ok = loadAssembly(m_assembliesMeta[1], "Assembly-CSharp.dll");
	if (!ok)
	{
		return false;
	}

	if (!loadClass(m_assembliesMeta[0], "Scripting", "Object"))
	{
		return false;
	}
	if (!loadClass(m_assembliesMeta[0], "Scripting", "GameObject"))
	{
		return false;
	}
	if (!loadClass(m_assembliesMeta[0], "Scripting.Components", "Identity"))
	{
		return false;
	}
	if (!loadClass(m_assembliesMeta[0], "Scripting.Components", "Transform"))
	{
		return false;
	}
	if (!loadClass(m_assembliesMeta[0], "Scripting", "Vector3"))
	{
		return false;
	}

	registerInternalCalls();

	int retval = mono_jit_exec(m_domain, m_assembliesMeta[0].Assembly, 0, nullptr);

	return true;
}

MonoDomain* core::Scripting::getDomain() const
{
	return m_domain;
}

MonoImage* core::Scripting::getImage(const std::string& assembly) const
{
	auto it = std::find_if(m_assembliesMeta.begin(), m_assembliesMeta.end(), [&assembly](const AssemblyMeta& arg)
	{
		return arg.Name == assembly;
	});

	if (it != m_assembliesMeta.cend())
	{
		return it->Image;
	}

	return nullptr;
}

MonoObject* core::Scripting::createMonoBehaviour(const std::string & name)
{
	return nullptr;
}

MonoClass* core::Scripting::getClass(const std::string& name) const
{
	MonoClass*	klass = nullptr;
	try
	{
		klass = m_bindings.at(name);
	}
	catch (const std::out_of_range&/*e*/)
	{
		LogCall(LOGLEVEL_ERROR) << "Failed to find component " << name << LOGFLUSH_FLUSH;
	}
	return klass;
}

std::string core::Scripting::getScriptLocation() const
{
	return "../../Assembly-CSharp";
}

bool core::Scripting::loadAssembly(AssemblyMeta& meta, const std::string& assembly)
{
	meta.Name = assembly;

	meta.Assembly = mono_domain_assembly_open(m_domain, assembly.c_str());
	if (!meta.Assembly)
	{
		return false;
	}
	meta.Image = mono_assembly_get_image(meta.Assembly);
	if (!meta.Image)
	{
		return false;
	}

	return true;
}

bool core::Scripting::loadClass(AssemblyMeta& meta, const std::string& space, const std::string& name)
{
	MonoClass*	klass = mono_class_from_name(meta.Image, space.c_str(), name.c_str());
	if (!klass)
	{
		return false;
	}

	m_bindings[name] = klass;

	return true;
}

void core::Scripting::registerInternalCalls()
{
	// Misc
	mono_add_internal_call("Scripting.Component::GetComponentFastPath", scripting::getComponentFast);
	mono_add_internal_call("Scripting.Component::get_gameobject", scripting::getGameObject);
	mono_add_internal_call("Scripting.Debug::INTERNAL_Log", scripting::internalLog);

	// Transform
	mono_add_internal_call("Scripting.Components.Transform::INTERNAL_get_position", scripting::INTERNAL_get_position);
	mono_add_internal_call("Scripting.Components.Transform::INTERNAL_set_position", scripting::INTERNAL_set_position);
	mono_add_internal_call("Scripting.Components.Transform::INTERNAL_get_rotation", scripting::INTERNAL_get_rotation);
	mono_add_internal_call("Scripting.Components.Transform::INTERNAL_set_rotation", scripting::INTERNAL_set_rotation);
	mono_add_internal_call("Scripting.Components.Transform::INTERNAL_get_localScale", scripting::INTERNAL_get_localScale);
	mono_add_internal_call("Scripting.Components.Transform::INTERNAL_set_localScale", scripting::INTERNAL_set_localScale);
	mono_add_internal_call("Scripting.Components.Transform::INTERNAL_CALL_TransformDirection", scripting::INTERNAL_CALL_TransformDirection);
	mono_add_internal_call("Scripting.Components.Transform::INTERNAL_CALL_RotateAroundInternal", scripting::INTERNAL_CALL_RotateAroundInternal);

	// Input
	mono_add_internal_call("Scripting.Input::GetKeyDownInt", scripting::INTERNAL_GetKeyDownInt);
	mono_add_internal_call("Scripting.Input::GetKeyInt", scripting::INTERNAL_GetKeyInt);
	mono_add_internal_call("Scripting.Input::GetKeyUpInt", scripting::INTERNAL_GetKeyUpInt);
	mono_add_internal_call("Scripting.Input::GetMouseButtonDown", scripting::INTERNAL_GetMouseButtonDown);
	mono_add_internal_call("Scripting.Input::GetMouseButton", scripting::INTERNAL_GetMouseButton);
	mono_add_internal_call("Scripting.Input::GetMouseButtonUp", scripting::INTERNAL_GetMouseButtonUp);
	mono_add_internal_call("Scripting.Input::INTERNAL_get_mousePosition", scripting::INTERNAL_GetMousePosition);

	// Behaviour
	mono_add_internal_call("Scripting.Components.Behaviour::set_enabled", scripting::INTERNAL_set_enabled);
	mono_add_internal_call("Scripting.Components.Behaviour::get_enabled", scripting::INTERNAL_get_enabled);

	// Time
	mono_add_internal_call("Scripting.Time::get_deltaTime", scripting::getDeltaTime);
	mono_add_internal_call("Scripting.Time::get_timeScale", scripting::getDeltaTime);
	mono_add_internal_call("Scripting.Time::set_timeScale", scripting::setTimeScale);

	// Quaternion
	mono_add_internal_call("Scripting.Quaternion::INTERNAL_CALL_Internal_FromEulerRad", scripting::INTERNAL_CALL_Internal_FromEulerRad);
	mono_add_internal_call("Scripting.Quaternion::INTERNAL_CALL_Inverse", scripting::INTERNAL_CALL_Inverse);
}

void core::scripting::getComponentFast(MonoObject* thisObject, MonoObject* type, void** ptr)
{
	MonoProperty*	prop = nullptr;
	void*			iter = nullptr;
	MonoClass*		klassRT = mono_object_get_class(type);

	prop = mono_class_get_property_from_name(klassRT, "Name");
	if (prop)
	{
		MonoObject* name = mono_property_get_value(prop, type, nullptr, nullptr);
		char*		p = mono_string_to_utf8((MonoString*)name);
		//LogCall(core::LOGLEVEL_DEBUG) << "Component is : " << p << core::LOGFLUSH_FLUSH;

		std::string	str(p);
		MonoClass*		klass = instance->getClass(str);
		if (klass)
		{
			MonoDomain*		domain = instance->getDomain();
			MonoObject*		object = mono_object_new(domain, klass);
			if (!object)
			{
				return;
			}
			mono_gchandle_new(object, false);

			int thisObjectId = 0;
			MonoClassField*	objectField = mono_class_get_field_from_name(instance->getClass("Object"), "instanceID");
			if (objectField != nullptr)
			{
				mono_field_get_value(thisObject, objectField, &thisObjectId);
				mono_field_set_value(object, objectField, &thisObjectId);
			}

			// We must retrieve the gameobject and set the id

			//int id = 0;
			//mono_field_get_value(object, field, &id);
			//mono_field_set_value(object, field, &owner);

			*(ptr - 1) = object;
			//LogCall(core::LOGLEVEL_DEBUG) << "Get component call" << core::LOGFLUSH_FLUSH;
		}
		else
		{
			LogCall(core::LOGLEVEL_ERROR) << "Cannot find component " << str << core::LOGFLUSH_FLUSH;
		}
	}
}

MonoObject* core::scripting::getGameObject(MonoObject* component)
{
	MonoClassField*	field = mono_class_get_field_from_name(instance->getClass("Component"), "instanceID");
	if (!field)
	{
		return nullptr;
	}

	int id = 0;
	mono_field_get_value(component, field, &id);

	auto	scriptingComponent = EngineData::getComponent<ScriptingComponent>(id);
	if (scriptingComponent)
	{
		return scriptingComponent->getGameObject();
	}
	else
	{
		return nullptr;
	}
}

void core::scripting::internalLog(int severity, MonoString* message, MonoObject* object)
{
	char*	ptr = mono_string_to_utf8(message);
	LogCall(LOGLEVEL_DEBUG) << ptr << LOGFLUSH_FLUSH;
}
