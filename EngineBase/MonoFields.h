#pragma once

#include "mono/metadata/class.h"

namespace core
{
	namespace mono
	{
		namespace field
		{
			extern MonoClassField*	ObjectNativeHandler;
		}
	}
}
