#include <fstream>
#include <sstream>

#include <boost\filesystem.hpp>

#include "Log.h"
#include "TextAsset.h"

core::TextAsset::TextAsset() :
	m_text(""),
	m_loaded(false)
{
	setAssetType(E_TEXTASSET);
}

core::TextAsset::~TextAsset()
{
}

std::string& core::TextAsset::getText()
{
	if (!m_loaded)
	{
		loadText();
	}
	return m_text;
}

std::string core::TextAsset::getText() const
{
	std::string	text;
	if (!m_loaded)
	{
		text = loadText();
	}
	else
	{
		text = m_text;
	}
	return text;
}

void core::TextAsset::setText(const std::string& text)
{
	m_text = text;
	setIsDirty(true);
}

void core::TextAsset::encodeAsset(std::ofstream& stream) const
{
	stream << getText();
}

void core::TextAsset::decodeAsset(std::ifstream& stream)
{
	loadText(stream);
	m_loaded = true;
}

void core::TextAsset::loadText()
{
	std::stringstream	ss;
	ss << m_filepath << "/" << m_filename;
	std::ifstream	file(ss.str());
	loadText(file);
}

std::string core::TextAsset::loadText() const
{
	std::stringstream	ss;
	ss << m_filepath << "/" << m_filename;
	std::ifstream	file(ss.str());
	return loadText(file);
}

void core::TextAsset::loadText(std::ifstream& stream)
{
	std::stringstream	ss;
	if (!stream.is_open())
	{
		LogCall(LOGLEVEL_ERROR) << "Failed to open file " << m_filename;
	}
	try
	{
		ss.clear();
		ss.str("");
		ss << stream.rdbuf();
		m_text = ss.str();
	}
	catch (const std::exception& e)
	{
		LogCall(LOGLEVEL_ERROR) << "Failed to load file " << m_filename << "\n" << e.what() << LOGFLUSH_FLUSH;
	}
}

std::string core::TextAsset::loadText(std::ifstream& stream) const
{
	std::stringstream	ss;
	if (!stream.is_open())
	{
		LogCall(LOGLEVEL_ERROR) << "Failed to open file " << m_filename;
	}
	try
	{
		ss.clear();
		ss.str("");
		ss << stream.rdbuf();
		return ss.str();
	}
	catch (const std::exception& e)
	{
		LogCall(LOGLEVEL_ERROR) << "Failed to load file " << m_filename << "\n" << e.what() << LOGFLUSH_FLUSH;
	}

	return "";
}
