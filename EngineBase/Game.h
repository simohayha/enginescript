#pragma once

#include <memory>
#include <map>
#include <list>
#include <functional>

#include "Graphic.h"
#include "Input.h"
#include "Scripting.h"

#include "Resource.h"
#include "System.h"
#include "WorldPositionComponent.h"
#include "TransformComponent.h"
#include "RenderComponent.h"
#include "IdentityComponent.h"
#include "ScriptingComponent.h"
#include "MeshFilterComponent.h"
#include "StepTimer.h"
#include "ObjectFactory.hpp"
#include "GameObject.h"

/*
public void RenderFrame(Model[] models)
{
	// Per frame
	Bind(View);
	Bind(Projection);
	BindLighting();

	// Per effect
	BindEffect();

	foreach(var material in GetMaterials(models))
	{
		// Per material
		Bind(material.Color);
		Bind(material.DiffuseMap);

		foreach(var model in GetModelsByMaterial(material, models))
		{
			// Per mesh
			Bind(model.VertexBuffer);
			Bind(model.IndexBuffer);

			foreach(var instance in model.Instances)
			{
				// Per instance
				Bind(instance.World);

				// Draw the instance
				Draw();
			}
		}
	}
}
*/

/*
http://gamedev.stackexchange.com/questions/49779/different-shaders-for-different-objects-directx-11
for each shader:
	// Set the device context to use this shader
	pContext->VSSetShader(shader.pVertexShader);
	pContext->PSSetShader(shader.pPixelShader);
	
	for each material that uses this shader:
		// Set the device context to use any constant buffers, textures, samplers,
		// etc. needed for this material
		pContext->VSSetConstantBuffers(...);
		pContext->PSSetConstantBuffers(...);
		pContext->PSSetShaderResources(...);
		pContext->PSSetSamplers(...);
		
		for each mesh that uses this material:
			// Set any constant buffers containing parameters specific to the mesh
			// (e.g. world matrix)
			pContext->VSSetConstantBuffers(...);
			
			// Set the context to use the vertex & index buffers for this mesh
			pContext->IASetInputLayout(mesh.pInputLayout);
			pContext->IASetVertexBuffers(...);
			pContext->IASetIndexBuffer(...);
			pContext->IASetPrimitiveTopology(...)
			
			// Draw it
			pContext->DrawIndexed(...)
*/

namespace core
{
	class Scene;
}

namespace core
{
	class Game
	{
	public:
		Game();
		~Game();

		static Game*	instance();

		bool	initialize(HWND parent, size_t width, size_t height);
		void	update(bool ignorePause = false);
		void	render();
		void	shutdown();

		Graphic&		graphic();
		const Graphic&	graphic() const;
		Input&			input();
		const Input&	input() const;

		bool	isInitialized() const;

		void	pause();
		bool	isPaused() const;

		int		addGameObject(int parent = 0);
		int		addRoot();
		void	getRootGameObjects(std::vector<int>& gameObjects);
		Asset*	getAssetFolder();
		AssetType	getAssetType(int objectId);
		Object*		getObject(int objectId);

		template <typename T>
		T*	getComponent(int gameObjectId)
		{
			return EngineData::getComponent<T>(gameObjectId);
		}

		template <typename T>
		T*	addComponent(int gameObjectid)
		{
			T*	component = ObjectFactory::instantiateComponent<T>(gameObjectid);
			return component;
		}

		template <typename T, typename... Args>
		T*	addComponent(int gameObjectid, Args... args)
		{
			T*	component = ObjectFactory::instantiateComponent<T>(gameObjectid, std::forward<Args...>(args...));
			return component;
		}

		float	getDeltaTime() const;
		float	getTimeScale() const;
		void	setTimeScale(float ts);
		float	getUnscaledTime() const;
		float	getTotalTime() const;

	private:
		static Game*				m_instance;
		bool						m_paused;
		float						m_timeScale;
		StepTimer					m_timer;
		std::unique_ptr<Graphic>	m_graphic;
		std::unique_ptr<Input>		m_input;
		std::unique_ptr<Scripting>	m_scripting;
		std::map<int, Resource>		m_resources;
		Scene*						m_currentScene;

		bool	onUpdate();
		bool	onRender();

		bool	loadTexture(const wchar_t* name);
	};

	namespace scripting
	{
		float	getDeltaTime();
		float	getTimeScale();
		void	setTimeScale(float ts);
		float	getUnscaledTime();
	}
}