#pragma once

#include <string>

#include <mono/mini/jit.h>
#include <mono/metadata/mono-config.h>
#include <mono/metadata/assembly.h>

namespace core
{
	struct AssemblyMeta
	{
		std::string		Name;
		MonoAssembly*	Assembly;
		MonoImage*		Image;
	};
}