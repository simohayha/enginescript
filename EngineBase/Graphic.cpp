#include "D3D.h"
#include "Camera.h"
#include "Shader.h"
#include "Model3D.h"
#include "CameraComponent.h"
#include "RenderComponent.h"
#include "TransformComponent.h"
#include "MeshFilterComponent.h"
#include "EngineData.hpp"
#include "Graphic.h"

#include "SimpleMath.h"

#include <iostream>

//#include <vsgcapture.h>

namespace core
{
	Graphic::Graphic() :
		m_direct3D(nullptr),
		m_currentCamera(nullptr),
		m_shader(nullptr)
	{
	}

	Graphic::~Graphic()
	{
	}

	bool Graphic::initialize(HWND hwnd, int width, int height)
	{
		bool result;

		m_direct3D.reset(new(std::nothrow) D3D);
		if (!m_direct3D)
		{
			return false;
		}

		result = m_direct3D->initialize(hwnd, width, height, VSYNC_ENABLED, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR);
		if (!result)
		{
			return false;
		}

		return true;
	}

	void Graphic::shutdown()
	{
		if (m_shader)
		{
			m_shader->shutdown();
			m_shader.reset(nullptr);
		}

		if (m_direct3D)
		{
			m_direct3D->shutdown();
			m_direct3D.reset(nullptr);
		}
	}

	bool Graphic::frame(RenderComponent* component)
	{
		bool result;

		result = render(component);
		if (!result)
		{
			return false;
		}

		return true;
	}

	D3D* Graphic::getDirect3D()
	{
		return m_direct3D.get();
	}

	void Graphic::resize(int height, int width)
	{
		m_direct3D->resize(height, width);
	}

	void Graphic::beginScene(DirectX::SimpleMath::Color& clearColor)
	{
		m_direct3D->beginScene(clearColor);
	}

	void Graphic::endScene()
	{
		m_direct3D->endScene();
	}

	void Graphic::bindShader(Shader& shader)
	{
		auto	context = m_direct3D->getDeviceContext();

		context->VSSetShader(shader.getVertexShader(), nullptr, 0u);;
		context->PSSetShader(shader.getPixelShader(), nullptr, 0u);
	}

	void Graphic::bindMaterial(Shader& shader, const Material& material)
	{
		auto	context = m_direct3D->getDeviceContext();

		shader.setMaterialBuffer(m_direct3D->getDeviceContext(), material);
	}

	void Graphic::bindModel(Shader& shader, Model3D& model)
	{
		auto	context = m_direct3D->getDeviceContext();
	}

	void Graphic::setCurrentCamera(CameraComponent* camera)
	{
		m_currentCamera = camera;
	}

	bool Graphic::render(RenderComponent* component)
	{
		if (!component->getMaterial())
		{
			return false;
		}

		auto	context = m_direct3D->getDeviceContext();

		auto	meshFilter = EngineData::getComponent<MeshFilterComponent>(component->getOwner());
		auto	transform = EngineData::getComponent<TransformComponent>(component->getOwner());

		// Set the device context to use this shader
		context->VSSetShader(component->getMaterial()->m_shader->getVertexShader(), nullptr, 0u);
		context->PSSetShader(component->getMaterial()->m_shader->getPixelShader(), nullptr, 0u);

		component->getMaterial()->m_shader->setSamplers(context);

		component->getMaterial()->m_shader->setResources(context, *component->getMaterial());

		// Set the device context to use any constant buffers, textures, samplers, etc. needed for this material
		if (!component->getMaterial()->m_shader->setMaterialBuffer(context, *component->getMaterial()))
		{
			return false;
		}
		DirectX::SimpleMath::Matrix	viewMatrix;
		DirectX::SimpleMath::Matrix	projectionMatrix;
		m_currentCamera->getViewMatrix(viewMatrix);
		m_currentCamera->getProjectionMatrix(projectionMatrix);
		if (!component->getMaterial()->m_shader->setCameraMatrixBuffer(context, viewMatrix, projectionMatrix))
		{
			return false;
		}

		Matrix	world;
		transform->getWorldTransformMatrix(world);

		if (!component->getMaterial()->m_shader->setWorldMatrixBuffer(context, world))
		{
			return false;
		}

		// Set the context to use the vertex & index buffers for this mesh
		unsigned int stride = sizeof(Vertex);
		unsigned int offset = 0u;
		context->IASetInputLayout(component->getMaterial()->m_shader->getInputLayout());
		context->IASetVertexBuffers(0, 1, meshFilter->getModel()->getVertexBuffer(), &stride, &offset);
		context->IASetIndexBuffer(*meshFilter->getModel()->getIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		if (component->getRenderMode() == E_RENDERMODE_WIREFRAME)
		{
			context->RSSetState(m_direct3D->getWireframeState());
		}
		else if (component->getRenderMode() == E_RENDERMODE_SOLID)
		{
			context->RSSetState(m_direct3D->getSolidState());
		}

		unsigned int indexCount = meshFilter->getModel()->getIndexCount();
		context->DrawIndexed(indexCount, 0u, 0u);

		component->getMaterial()->m_shader->clearResources(context, *component->getMaterial());

		return true;
	}
}