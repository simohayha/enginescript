#include "WorldPositionComponent.h"

core::WorldPositionComponent::WorldPositionComponent(EntityId id) :
	Component(id),
	m_position(0.0f, 0.0f, 0.0f)
{
}

core::WorldPositionComponent::~WorldPositionComponent()
{
}

DirectX::XMFLOAT3& core::WorldPositionComponent::getPosition()
{
	return m_position;
}
