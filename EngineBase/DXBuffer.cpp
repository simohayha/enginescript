#include "DXBuffer.h"

core::DXBuffer::DXBuffer() :
	Vertex(nullptr),
	Index(nullptr)
{
}

core::DXBuffer::~DXBuffer()
{
	if (Index)
	{
		Index->Release();
		Index = nullptr;
	}
	if (Vertex)
	{
		Vertex->Release();
		Vertex = nullptr;
	}
}
