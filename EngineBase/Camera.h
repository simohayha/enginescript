#pragma once

#include <DirectXMath.h>

using namespace DirectX;

namespace core
{
	class Camera
	{
	public:
		Camera();
		~Camera();

		void		setPosition(float x, float y, float z);
		void		setRotation(float x, float y, float z);

		XMFLOAT3	getPosition() const;
		XMFLOAT3	getRotation() const;

		void		render();
		void		getViewMatrix(XMMATRIX& matrix);
		void		getViewMatrix(XMFLOAT4X4& matrix);

	private:
		float		m_positionX;
		float		m_positionY;
		float		m_positionZ;
		float		m_rotationX;
		float		m_rotationY;
		float		m_rotationZ;
		XMFLOAT4X4	m_view;
	};
}