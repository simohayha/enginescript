#pragma once

#include "Object.h"
#include "Serializable.h"
#include "Entity.h"

namespace core
{
	class GameObject : public Object, public Serializable
	{
	public:
		GameObject();
		virtual ~GameObject();

		virtual void	encodeAsset(std::ofstream& stream) const override;
		virtual void	decodeAsset(YAML::Node& node) override;
		//virtual void	decode(std::ofstream& stream) override;

		virtual std::string	getName() const override;
		virtual void		setName(const std::string& name) override;
	};
}