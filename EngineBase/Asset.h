#pragma once

#include <string>
#include <list>

#include "Serializable.h"
#include "Object.h"

namespace core
{
	class Asset : public Object, public Serializable
	{
	public:
		Asset();
		virtual ~Asset();

		const std::string&	getFilename() const;
		const std::string&	getFilepath() const;
		const std::string&	getExtension() const;
		const std::string	getCompletePath() const;
		const std::string	getCompleteMetaPath() const;

		void	setFilename(std::string filename);
		void	setFilepath(std::string filepath);
		void	setExtension(std::string extension);

		Asset*				getParent() const;
		void				setParent(Asset* parent);
		std::list<Asset*>&	getChilds();
		void				addChild(Asset* asset);
		void				removeChild(Asset* asset);

		virtual std::string	getName() const override;
		virtual void		setName(const std::string& name) override;

	protected:
		std::string	m_filename;
		std::string	m_filepath;
		std::string	m_extension;

		Asset*				m_parent;
		std::list<Asset*>	m_childs;
	};
}
