#include "AbstractScript.h"

core::AbstractScript::AbstractScript() :
	m_enabled(true)
{
}

core::AbstractScript::~AbstractScript()
{
}

bool core::AbstractScript::initialize()
{
	return true;
}

void core::AbstractScript::start()
{
	onStart();
}

void core::AbstractScript::update()
{
	onUpdate();
}

void core::AbstractScript::setEnabled(bool enabled)
{
	m_enabled = enabled;
}

bool core::AbstractScript::getEnabled() const
{
	return m_enabled;
}
