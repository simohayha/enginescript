#pragma once

#include <d3d11.h>
#include <SimpleMath.h>

#include "Component.h"

namespace core
{
	class CameraComponent : public Component
	{
	public:
		CameraComponent(EntityId id);
		~CameraComponent();

		void	getViewMatrix(DirectX::SimpleMath::Matrix& matrix);
		void	getProjectionMatrix(DirectX::SimpleMath::Matrix& matrix);

		void						setAspect(float aspect);
		float						getAspect() const;
		void						setNear(float cameraNear);
		float						getNear() const;
		void						setFar(float cameraFar);
		float						getFar() const;
		void						setFov(float fov);
		float						getFov() const;
		void						setBackgroundColor(DirectX::SimpleMath::Color color);
		DirectX::SimpleMath::Color	getBackgroundColor() const;

		virtual void	encodeAsset(std::ofstream& stream) const override;
		virtual void	decodeAsset(YAML::Node& node) override;

	private:
		float	m_aspect;
		float	m_near;
		float	m_far;
		float	m_fov;

		DirectX::SimpleMath::Color	m_backgroundColor;
		DirectX::SimpleMath::Matrix	m_view;
		DirectX::SimpleMath::Matrix	m_projection;
	};
}