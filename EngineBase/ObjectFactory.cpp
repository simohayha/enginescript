#include <sstream>

#include <boost\filesystem.hpp>

#include "AssetLoader.h"
#include "ObjectFactory.hpp"

boost::uuids::random_generator	core::ObjectFactory::m_generator;

core::Asset* core::ObjectFactory::createAsset(AssetType type, Asset* parent)
{
	if (!parent)
	{
		parent = EngineData::getAssetDirectoryPtr();
	}

	boost::filesystem::path	path;
	if (type == E_DIRECTORY)
	{
		path = getSuitablePath("New folder");
	}
	else if (type == E_MATERIAL)
	{
		path = getSuitablePath("New material.mat");
	}
	else if (type == E_MONOSCRIPT)
	{
		path = getSuitablePath("New script.cs");
	}
	else if (type == E_SCENE)
	{
		path = getSuitablePath("New scene.engine");
	}

	AssetLoader	loader;
	auto		asset = loader.process(path);
	parent->addChild(asset);
	return asset;
}

std::string core::ObjectFactory::getSuitablePath(const std::string& baseName)
{
	bool					ok = false;
	int						iter = 0;
	boost::filesystem::path	path;
	while (!ok)
	{
		std::stringstream	ss;
		if (iter == 0)
		{
			ss << "./Assets/" << baseName;
			path = boost::filesystem::path(ss.str());
		}
		else
		{
			ss << "./Assets/" << baseName << " " << iter;
			path = boost::filesystem::path(ss.str());
		}

		if (!path.has_extension())
		{
			ok = boost::filesystem::create_directory(path);
		}
		else
		{
			boost::filesystem::ofstream	stream(path);
			ok = true;
		}
		iter++;
	}

	return path.string();
}
