#include "Game.h"
#include "Input.h"

namespace core
{
	Input::Input()
	{
	}

	Input::~Input()
	{
	}

	void Input::initialize()
	{
		for (int i = 0; i < 256; ++i)
		{
			m_previousKeys[i] = false;
			m_keys[i] = false;
		}
		for (int i = 0; i < 3; ++i)
		{
			m_previousMouseButtons[i] = false;
			m_mouseButtons[i] = false;
		}
	}

	void Input::keyDown(unsigned int key)
	{
		m_keys[key] = true;
	}

	void Input::keyUp(unsigned int key)
	{
		m_keys[key] = false;
	}

	void Input::mouseButtonDown(unsigned int button)
	{
		m_mouseButtons[button] = true;
	}

	void Input::mouseButtonUp(unsigned int button)
	{
		m_mouseButtons[button] = false;
	}

	void Input::setMousePosition(Vector3 position)
	{
		m_mousePosition = position;
	}

	void Input::setMousePosition(float x, float y, float z)
	{
		setMousePosition({ x, y, z });
	}

	bool Input::isAnyKeyDown() const
	{
		for (auto i = 0u; i < 256u; ++i)
		{
			if (m_keys[i] && !m_previousKeys[i])
			{
				return true;
			}
		}
		return false;
	}

	bool Input::isAnyKey() const
	{
		for (auto i = 0u; i < 256u; ++i)
		{
			if (m_keys[i])
			{
				return true;
			}
		}
		return false;
	}

	bool Input::isKeyDown(unsigned int key) const
	{
		return m_keys[key] && !m_previousKeys[key];
	}

	bool Input::isKey(unsigned int key) const
	{
		return m_keys[key];
	}

	bool Input::isKeyUp(unsigned int key) const
	{
		return !m_keys[key] && m_previousKeys[key];
	}

	bool Input::isMouseButtonDown(unsigned int button)
	{
		return m_mouseButtons[button] && !m_previousMouseButtons[button];
	}

	bool Input::isMouseButton(unsigned int button)
	{
		return m_mouseButtons[button];
	}

	bool Input::isMouseButtonUp(unsigned int button)
	{
		return !m_mouseButtons[button] && m_previousMouseButtons[button];
	}

	Vector3 Input::getMousePosition() const
	{
		return m_mousePosition;
	}

	void Input::reset()
	{
		std::memcpy(m_previousKeys, m_keys, 256);
		std::memcpy(m_previousMouseButtons, m_mouseButtons, 3);
	}
}

bool core::scripting::INTERNAL_get_AnyKeyDown()
{
	return core::Game::instance()->input().isAnyKeyDown();
}

bool core::scripting::INTERNAL_get_AnyKey()
{
	return core::Game::instance()->input().isAnyKey();
}

bool core::scripting::INTERNAL_GetKeyDownInt(int key)
{
	return core::Game::instance()->input().isKeyDown(key);
}

bool core::scripting::INTERNAL_GetKeyInt(int key)
{
	return core::Game::instance()->input().isKey(key);
}

bool core::scripting::INTERNAL_GetKeyUpInt(int key)
{
	return core::Game::instance()->input().isKeyUp(key);
}

bool core::scripting::INTERNAL_GetMouseButtonDown(int button)
{
	return core::Game::instance()->input().isMouseButtonDown(button);
}

bool core::scripting::INTERNAL_GetMouseButton(int button)
{
	return core::Game::instance()->input().isMouseButton(button);
}

bool core::scripting::INTERNAL_GetMouseButtonUp(int button)
{
	return core::Game::instance()->input().isMouseButtonUp(button);
}

void core::scripting::INTERNAL_GetMousePosition(Vector3* mousePosition)
{
	*mousePosition = core::Game::instance()->input().getMousePosition();
}
