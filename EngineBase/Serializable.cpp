#include "Serializable.h"

core::Serializable::Serializable()
{
}

core::Serializable::~Serializable()
{
}

void core::Serializable::encodeAsset(std::ofstream& stream) const
{
}

void core::Serializable::decodeAsset(std::vector<YAML::Node>& nodes)
{
}

void core::Serializable::decodeAsset(YAML::Node& node)
{
}

void core::Serializable::decodeAsset(std::ifstream& stream)
{
}

void core::Serializable::encodeMeta(std::ofstream& stream) const
{
}

void core::Serializable::decodeMeta(std::ifstream& stream)
{
}
