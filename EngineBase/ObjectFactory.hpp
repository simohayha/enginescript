#pragma once

#include <type_traits>

#include <boost\uuid\uuid.hpp>
#include <boost\uuid\uuid_generators.hpp>
#include <boost\functional\hash.hpp>

#include "Object.h"
#include "Component.h"
#include "EngineData.hpp"

namespace core
{
	class ObjectFactory
	{
	public:
		~ObjectFactory() = default;

		template <typename T>
		static T*	instantiate()
		{
			static_assert(std::is_base_of<Object, T>::value, "T must derive from Object");

			auto	uuid = m_generator();
			boost::hash<boost::uuids::uuid> uuid_hasher;
			std::size_t uuid_hash_value = uuid_hasher(uuid);

			T*	object = new T();
			int	available = EngineData::reserveId();
			object->setId(available);
			object->setFileId(uuid_hash_value);
			EngineData::addObject(object);
			return object;
		}

		template <typename T>
		static T*	instantiate(int id)
		{
			static_assert(std::is_base_of<Object, T>::value, "T must derive from Object");

			auto	uuid = m_generator();
			boost::hash<boost::uuids::uuid> uuid_hasher;
			std::size_t uuid_hash_value = uuid_hasher(uuid);

			T*	object = new T();
			object->setId(id);
			object->setFileId(uuid_hash_value);
			EngineData::addObject(object);
			return object;
		}

		template <class T, typename... Args>
		static T*	instantiate(Args... args)
		{
			static_assert(std::is_base_of<Object, T>::value, "T must derive from Object");

			auto	uuid = m_generator();
			boost::hash<boost::uuids::uuid> uuid_hasher;
			std::size_t uuid_hash_value = uuid_hasher(uuid);

			T*	object = new T(std::forward<Args...>(args...));
			int	available = EngineData::reserveId();
			object->setId(available);
			object->setFileId(uuid_hash_value);
			EngineData::addObject(object);
			return object;
		}

		template <typename T, typename... Args>
		static T*	instantiateComponent(int owner)
		{
			static_assert(std::is_base_of<Component, T>::value, "T must derive from Component");

			auto	uuid = m_generator();
			boost::hash<boost::uuids::uuid> uuid_hasher;
			std::size_t uuid_hash_value = uuid_hasher(uuid);

			int	available = EngineData::reserveId();
			T*	component = new T(owner);
			component->setId(available);
			component->setFileId(uuid_hash_value);
			EngineData::registerComponent(component);
			EngineData::addObject(component);
			return component;
		}

		template <typename T, typename... Args>
		static T*	instantiateComponent(int owner, Args... args)
		{
			static_assert(std::is_base_of<Component, T>::value, "T must derive from Component");

			auto	uuid = m_generator();
			boost::hash<boost::uuids::uuid> uuid_hasher;
			std::size_t uuid_hash_value = uuid_hasher(uuid);

			int	available = EngineData::reserveId();
			T*	component = new T(owner, std::forward<Args...>(args...));
			component->setId(available);
			component->setFileId(uuid_hash_value);
			EngineData::registerComponent(component);
			EngineData::addObject(component);
			return component;
		}

		static Asset*		createAsset(AssetType type, Asset* parent = nullptr);
		static std::string	getSuitablePath(const std::string& baseName);

	private:
		ObjectFactory() = default;

		static void	setId(EntityId id, Object* object)
		{
			object->setId(id);
		}

		static boost::uuids::random_generator	m_generator;
	};
}
