#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <memory>

#include <d3d11.h>
#include <SimpleMath.h>

#include "Utility.h"
#include "Shader.h"
#include "Material.h"
#include "Model3D.h"

namespace core
{
	class D3D;
	class Camera;
	class Model3D;
	class Shader;
	class RenderComponent;
	class CameraComponent;

	const bool	FULL_SCREEN = false;
	const bool	VSYNC_ENABLED = false;
	const float	SCREEN_DEPTH = 1000.0f;
	const float	SCREEN_NEAR = 0.1f;

	class Graphic
	{
	public:
		Graphic();
		~Graphic();

		bool	initialize(HWND hwnd, int width, int height);
		void	shutdown();
		bool	frame(RenderComponent* component);

		D3D*	getDirect3D();

		void	resize(int height, int width);

		void	beginScene(DirectX::SimpleMath::Color& clearColor);
		void	endScene();

		void	bindShader(Shader& shader);
		void	bindMaterial(Shader& shader, const Material& material);
		void	bindModel(Shader& shader, Model3D& model);

		void	setCurrentCamera(CameraComponent* camera);

	private:
		std::unique_ptr<D3D>		m_direct3D;
		CameraComponent*			m_currentCamera;
		std::unique_ptr<Shader>		m_shader;

		bool	render(RenderComponent* component);
	};
}
