#pragma once

#include <d3d11.h>
#include <SimpleMath.h>

#include "Vector3.h"

namespace core
{
	struct Quaternion : public DirectX::SimpleMath::Quaternion
	{
	public:
		Quaternion();
		Quaternion(float x, float y, float z, float w);
		Quaternion(const DirectX::SimpleMath::Quaternion& q);
		~Quaternion();

		void	toEulerAngle(Vector3& eulerAngles);
	};

	namespace scripting
	{
		void INTERNAL_CALL_Internal_FromEulerRad(Vector3* euler, Quaternion* value);
		void INTERNAL_CALL_Inverse(Quaternion* rotation, Quaternion* value);
	}
}
