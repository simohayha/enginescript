#pragma once

#include <d3d11.h>
#include <SimpleMath.h>

namespace core
{
	struct Matrix : DirectX::SimpleMath::Matrix
	{
	public:
		Matrix();
		Matrix(const DirectX::SimpleMath::Matrix& m);
	};
}