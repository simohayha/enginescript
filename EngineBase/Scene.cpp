#include "Log.h"
#include "EngineData.hpp"
#include "ObjectFactory.hpp"
#include "CameraComponent.h"
#include "Scene.h"

core::Scene::Scene()
{
	setAssetType(E_SCENE);
}

core::Scene::~Scene()
{
}

void core::Scene::encodeAsset(std::ofstream& stream) const
{
	for (auto& go : m_gameObjects)
	{
		go.second->encodeAsset(stream);
	}
}

void core::Scene::decodeAsset(std::vector<YAML::Node>& nodes)
{
	for (auto& node : nodes)
	{
		if (node.IsMap())
		{
			LogCall(LOGLEVEL_DEBUG) << "Found a map" << LOGFLUSH_FLUSH;

			if (node["GameObject"])
			{
				auto	gameObject = ObjectFactory::instantiate<GameObject>();
				gameObject->decodeAsset(node["GameObject"]);
				EngineData::addFileIdToEntity(gameObject->getFileId(), gameObject->getId());
				addGameObject(gameObject);
			}
			else if (node["Render"])
			{
				auto	render = ObjectFactory::instantiate<RenderComponent, int>(-1);
				render->decodeAsset(node["Render"]);
				EngineData::addFileIdToEntity(render->getFileId(), render->getId());

				LogCall(LOGLEVEL_DEBUG) << "Map have a Render" << LOGFLUSH_FLUSH;
			}
			else if (node["Transform"])
			{
				auto	transform = ObjectFactory::instantiate<TransformComponent, int>(-1);
				transform->decodeAsset(node["Transform"]);
				EngineData::addFileIdToEntity(transform->getFileId(), transform->getId());

				LogCall(LOGLEVEL_DEBUG) << "Map have a Transform" << LOGFLUSH_FLUSH;
			}
			else if (node["Mesh Filter"])
			{
				auto	meshFilter = ObjectFactory::instantiate<MeshFilterComponent, int>(-1);
				meshFilter->decodeAsset(node["Mesh Filter"]);
				EngineData::addFileIdToEntity(meshFilter->getFileId(), meshFilter->getId());

				LogCall(LOGLEVEL_DEBUG) << "Map have a Mesh Filter" << LOGFLUSH_FLUSH;
			}
			else if (node["Camera"])
			{
				auto	camera = ObjectFactory::instantiate<CameraComponent, int>(-1);
				camera->decodeAsset(node["Camera"]);
				EngineData::addFileIdToEntity(camera->getFileId(), camera->getId());

				LogCall(LOGLEVEL_DEBUG) << "Map have a Camera" << LOGFLUSH_FLUSH;
			}
			else if (node["Identity"])
			{
				auto	identity = ObjectFactory::instantiate<IdentityComponent, int>(-1);
				identity->decodeAsset(node["Identity"]);
				EngineData::addFileIdToEntity(identity->getFileId(), identity->getId());

				LogCall(LOGLEVEL_DEBUG) << "Map have a Identity" << LOGFLUSH_FLUSH;
			}
		}
		if (node.IsScalar())
		{
			LogCall(LOGLEVEL_DEBUG) << "Found a scalar" << LOGFLUSH_FLUSH;
		}
	}
}

void core::Scene::addGameObject(GameObject* object)
{
	if (!object)
	{
		return;
	}

	m_gameObjects[object->getId()] = object;
}

void core::Scene::removeGameObject(GameObject* object)
{
	if (!object)
	{
		return;
	}

	if (m_gameObjects.erase(object->getId()) != 1u)
	{
		LogCall(LOGLEVEL_WARNING) << "Trying to remove gameobject " << object->getId() << ", but object is not registered" << LOGFLUSH_FLUSH;
	}
}
