#include "Component.h"

MonoClass*	core::Component::Class = nullptr;

core::Component::Component(EntityId id) :
	m_owner(id)
{
}

core::Component::~Component()
{
}

EntityId core::Component::getOwner() const
{
	return m_owner;
}
