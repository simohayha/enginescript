#include "Camera.h"

core::Camera::Camera() :
	m_positionX(0.0f),
	m_positionY(0.0f),
	m_positionZ(0.0f),
	m_rotationX(0.0f),
	m_rotationY(0.0f),
	m_rotationZ(0.0f)
{
}

core::Camera::~Camera()
{
}

void core::Camera::setPosition(float x, float y, float z)
{
	m_positionX = x;
	m_positionY = y;
	m_positionZ = z;
}

void core::Camera::setRotation(float x, float y, float z)
{
	m_rotationX = x;
	m_rotationY = y;
	m_rotationZ = z;
}

XMFLOAT3 core::Camera::getPosition() const
{
	return XMFLOAT3(m_positionX, m_positionY, m_positionZ);
}

XMFLOAT3 core::Camera::getRotation() const
{
	return XMFLOAT3(m_rotationX, m_rotationY, m_rotationZ);
}

void core::Camera::render()
{
	XMVECTOR	eye = XMVectorSet(0.0f, 1.0f, -5.0f, 0.0f);
	XMVECTOR	at = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	XMVECTOR	up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	XMStoreFloat4x4(&m_view, XMMatrixLookAtLH(eye, at, up));
}

void core::Camera::getViewMatrix(XMMATRIX& matrix)
{
	matrix = XMLoadFloat4x4(&m_view);
}

void core::Camera::getViewMatrix(XMFLOAT4X4& matrix)
{
	matrix = m_view;
}
