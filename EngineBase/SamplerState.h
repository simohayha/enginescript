#pragma once

#include <d3d11_2.h>
#include <d3dcompiler.h>
#include <wrl/client.h>

using namespace Microsoft::WRL;

namespace core
{
	struct SamplerState
	{
	public:
		D3D11_SHADER_INPUT_BIND_DESC	InputDesc;
		ComPtr<ID3D11SamplerState>		SampleState;
	};
}