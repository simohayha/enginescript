#pragma once

#include <d3d11.h>
#include <d3dcompiler.h>
#include <d3d11sdklayers.h>
#include <comdef.h>
#include <DirectXMath.h>

#include <vector>
#include <fstream>
#include <string>

#include "ConstantBuffer.h"
#include "SamplerState.h"
#include "Material.h"
#include "TextAsset.h"
#include "Model3D.h"

using namespace DirectX;
using namespace std;

_COM_SMARTPTR_TYPEDEF(ID3DBlob, __uuidof(ID3DBlob));
_COM_SMARTPTR_TYPEDEF(ID3D11ShaderReflection, IID_ID3D11ShaderReflection);

namespace core
{
	class Shader : public TextAsset
	{
	public:
		Shader();
		virtual ~Shader();

		bool	initialize(ID3D11Device* device, HWND hwnd, const std::string& file);
		void	shutdown();

		const ConstantBufferLayout& getBufferLayout(int index) const;

		ID3D11VertexShader*	getVertexShader();
		ID3D11PixelShader*	getPixelShader();
		ID3D11InputLayout*	getInputLayout();

		bool	setMaterialBuffer(ID3D11DeviceContext* context, const Material& material);
		bool	setCameraMatrixBuffer(ID3D11DeviceContext* context, XMFLOAT4X4 view, XMFLOAT4X4 projection);
		bool	setWorldMatrixBuffer(ID3D11DeviceContext* context, XMFLOAT4X4 world);
		void	setModelBuffer(const Model3D& model);
		void	setSamplers(ID3D11DeviceContext* context);
		void	setResources(ID3D11DeviceContext* context, const Material& material);

		void	clearResources(ID3D11DeviceContext* context, const Material& material);

	private:
		struct MatrixBufferType
		{
			XMMATRIX	world;
			XMMATRIX	view;
			XMMATRIX	projection;
		};

		ID3D11VertexShader*	m_vertexShader;
		ID3D11PixelShader*	m_pixelShader;
		ID3D11InputLayout*	m_layout;
		ID3D11SamplerState*	m_sampleState;

		std::vector<ConstantBuffer*>	m_constantBuffers;
		std::vector<SamplerState>		m_shaderInput;

		bool	initializeShader(ID3D11Device* device, HWND hwnd, WCHAR* vs, WCHAR* ps);
		bool	initializeShader(ID3D11Device* device, HWND hwnd, const std::string& file);
		void	shutdownShader();

		void	renderShader(ID3D11DeviceContext* context, int indexCount);

		bool	buildShaderProgram(ID3D11Device* device, WCHAR* vertexShaderPath, WCHAR* pixelShaderPath);
		bool	buildShaderProgram(ID3D11Device* device, const std::string& file);

		HRESULT	enumInputLayout(ID3D11Device* device, ID3DBlob* vsBlob);
		HRESULT	enumConstantBuffers(ID3D11Device* device, ID3DBlob* blob);
		HRESULT	enumResourcesBinding(ID3D11Device* device, ID3D10Blob* blob);

		bool	setVSMaterialConstantBuffers(ID3D11DeviceContext* context, ConstantBuffer* cbuffer, const Material& material);
		bool	setPSMaterialConstantBuffers(ID3D11DeviceContext* context, ConstantBuffer* cbuffer, const Material& material);

		void	outputShaderErrorMessage(ID3D10Blob* error, WCHAR* file);
	};
}