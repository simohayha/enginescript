#pragma once

#include <vector>
#include <string>

#include <d3d11.h>
#include <d3d11sdklayers.h>
#include <d3dcompiler.h>

namespace core
{
	struct ConstantBufferLayout
	{
		std::string								Name;
		D3D11_SHADER_BUFFER_DESC				Description;
		std::vector<D3D11_SHADER_VARIABLE_DESC>	Variables;
		std::vector<D3D11_SHADER_TYPE_DESC>		Types;
	};

	struct ConstantBuffer
	{
	public:
		ConstantBuffer();
		~ConstantBuffer();

		unsigned int			Index;
		ID3D11Buffer*			Buffer;
		ConstantBufferLayout	Layout;
		unsigned int			Slot;
	};

	struct DynamicBuffer
	{
	public:
		DynamicBuffer(ConstantBuffer* buffer);
		~DynamicBuffer();

		ConstantBuffer*	Buffer;
		unsigned int	Size;
		unsigned int	WritePos;
		unsigned int	AllocPos;
	};
}
