#pragma once

#include <string>
#include <vector>

namespace core
{
	class Mesh;
}

namespace core
{
	class FileLoader
	{
		void	LoadMeshFromFile(const std::wstring& mesh, std::vector<Mesh*> loadedMesh, bool cleanLoadedMeshesVector);
	};
}