#pragma once

#include <d3d11.h>
#include <DirectXMath.h>

#include "MeshData.h"
#include "DXBuffer.h"
#include "GeometryGenerator.h"
#include "Object.h"
#include "Material.h"

using namespace DirectX;

namespace core
{
	class Model3D : public Object
	{
	public:
		Model3D();
		~Model3D();

		void	createFromShape(const GeometryGenerator::Shape shape);

		bool	initialize(ID3D11Device* device);
		void	shutdown();
		void	render(ID3D11DeviceContext* context);

		unsigned		getIndexCount() const;

		ID3D11Buffer**	getVertexBuffer();
		ID3D11Buffer**	getIndexBuffer();

	private:
		DXBuffer	m_buffers;
		MeshData	m_meshData;

		bool	initializeBuffers(ID3D11Device* device);
		void	shutdownBuffers();
		void	renderBuffers(ID3D11DeviceContext* context);
	};
}