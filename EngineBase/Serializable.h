#pragma once

#include <vector>
#include <fstream>

#include <yaml-cpp/yaml.h>

namespace core
{
	class Serializable
	{
	public:
		Serializable();
		virtual ~Serializable();

		virtual void	encodeAsset(std::ofstream& stream) const;
		virtual void	decodeAsset(std::vector<YAML::Node>& nodes);
		virtual void	decodeAsset(YAML::Node& node);
		virtual void	decodeAsset(std::ifstream& stream);
		virtual void	encodeMeta(std::ofstream& stream) const;
		virtual void	decodeMeta(std::ifstream& stream);
	};
}
