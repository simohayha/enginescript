#pragma once

#include <map>

#include "Entity.h"

namespace core
{
	//class Sys
	//{
	//public:
	//	Sys();
	//	virtual ~Sys();

	//	template <typename T>
	//	virtual std::map<EntityId, T*>& components() = 0;

	//	template <typename T>
	//	virtual const std::map<EntityId, T*>& components() const = 0;
	//};

	template <typename C>
	class System
	{
	public:
		System();
		~System();

		std::map<EntityId, C*>&			components();
		const std::map<EntityId, C*>&	components() const;
		C*								get(size_t idx);

	private:
		std::map<EntityId, C*>	m_components;
	};

	template<typename C>
	inline std::map<EntityId, C*>& System<C>::components()
	{
		return m_components;
	}

	template<typename C>
	inline C* System<C>::get(size_t idx)
	{
		C*	component = nullptr;
		try
		{
			component = m_components.at(static_cast<unsigned long>(idx));
		}
		catch (const std::out_of_range& e)
		{
			throw e;
		}
		return component;
	}

	template<typename C>
	inline const std::map<EntityId, C*>& System<C>::components() const
	{
		return m_components;
	}
}
