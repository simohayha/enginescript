#pragma once

#include <d3d11_2.h>
#include <wrl/client.h>

using namespace Microsoft::WRL;

namespace core
{
	struct Resource
	{
	public:
		unsigned int						Id;
		ComPtr<ID3D11Resource>				Res;
		ComPtr<ID3D11ShaderResourceView>	ResView;

		Resource();

	private:
		static unsigned int UUID;
	};
}