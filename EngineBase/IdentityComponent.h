#pragma once

#include <vector>

#include "Component.h"

namespace core
{
	class IdentityComponent : public Component
	{
	public:
		static int Id();

		IdentityComponent(EntityId id);
		~IdentityComponent();

		virtual void		setName(const std::string& name) override;
		virtual std::string	getName() const override;
		//const std::string&	getName() const;

		virtual void		encodeAsset(std::ofstream& stream) const override;
		virtual void		decodeAsset(YAML::Node& node) override;

	private:
		std::string	m_name;
	};

	namespace scripting
	{
		MonoString*	getName(MonoObject* thisObject);
		void		setName(MonoObject* thisObject, MonoString* name);
	}
}