#include "Game.h"

template <typename T>
core::Log& core::Log::operator<<(const T& right)
{
	if (m_current <= m_level)
	{
		m_os << right;
	}
	return *this;
}

template <core::LogLevel level>
core::Log* logger(const std::string& file, int line, const std::string& func)
{
	core::Log*	logger = core::Log::get();
	std::string	temp;

	temp = file;
	std::reverse(temp.begin(), temp.end());
	size_t	pos = temp.find_first_of("\\/");
	if (pos != std::string::npos)
	{
		temp.erase(temp.begin() + pos, temp.end());
	}
	std::reverse(temp.begin(), temp.end());
	logger->setCurrentLogLevel(level);
	*logger << core::Game::instance()->getTotalTime() << ' ';
	*logger << '[' << temp << ':' << line << ':' << func << ']';
	if (level == core::LOGLEVEL_CRITICAL)
	{
		*logger << "_CRITICAL_\t";
	}
	else if (level == core::LOGLEVEL_ERROR)
	{
		*logger << "_ERROR_\t";
	}
	else if (level == core::LOGLEVEL_WARNING)
	{
		*logger << "_WARNING_\t";
	}
	else if (level == core::LOGLEVEL_INFO)
	{
		*logger << "_INFO_\t";
	}
	else if (level == core::LOGLEVEL_DEBUG)
	{
		*logger << "_DEBUG_\t";
	}


	return logger;
}