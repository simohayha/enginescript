#pragma once

#include <string>

#include "Asset.h"

namespace core
{
	class TextAsset : public Asset
	{
	public:
		TextAsset();
		virtual ~TextAsset();

		std::string&	getText();
		std::string		getText() const;
		void			setText(const std::string& text);

		virtual void	encodeAsset(std::ofstream& stream) const override;
		virtual void	decodeAsset(std::ifstream& stream) override;

	private:
		std::string	m_text;
		bool		m_loaded;

		void		loadText();
		std::string	loadText() const;
		void		loadText(std::ifstream& stream);
		std::string	loadText(std::ifstream& stream) const;
	};
}