#pragma once

#include <string>
#include <vector>
#include <map>

#include "AssemblyMeta.h"

namespace core
{
	class Scripting
	{
	public:
		Scripting();
		~Scripting();

		bool		initialize();
		MonoDomain*	getDomain() const;
		MonoImage*	getImage(const std::string& assembly) const;

		MonoObject*	createMonoBehaviour(const std::string& name);

		MonoClass*	getClass(const std::string& name) const;

	private:
		MonoDomain*					m_domain;
		std::vector<AssemblyMeta>	m_assembliesMeta;
		std::vector<MonoClass*>		m_userMonoBehaviours;

		std::map<std::string, MonoClass*>	m_bindings;

		std::string	getScriptLocation() const;

		bool	loadAssembly(AssemblyMeta& meta, const std::string& assembly);
		bool	loadClass(AssemblyMeta& meta, const std::string& space, const std::string& name);

		void	registerInternalCalls();
	};

	namespace scripting
	{
		void		getComponentFast(MonoObject* thisObject, MonoObject* type, void** ptr);
		MonoObject*	getGameObject(MonoObject* component);
		void		internalLog(int severity, MonoString* message, MonoObject* object);
	}
}