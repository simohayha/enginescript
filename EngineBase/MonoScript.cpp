#include "Log.h"
#include "MonoScript.h"

core::MonoScript::MonoScript(MonoClass* scriptClass, MonoObject* scriptObject, MonoObject* attachedGameObject) :
	m_scriptClass(scriptClass),
	m_scriptObject(scriptObject),
	m_attachedGameObject(attachedGameObject)
{
}

core::MonoScript::~MonoScript()
{
}

bool core::MonoScript::initialize()
{
	m_methods.resize(static_cast<size_t>(Functions::FUNCTION_MAX), nullptr);

	m_methods[static_cast<size_t>(Functions::FUNCTION_START)] = mono_class_get_method_from_name(m_scriptClass, "Start", 0);
	m_methods[static_cast<size_t>(Functions::FUNCTION_UPDATE)] = mono_class_get_method_from_name(m_scriptClass, "Update", 0);

	return true;
}

void core::MonoScript::onStart()
{
	MonoMethod*	start = getMethod(Functions::FUNCTION_START);
	if (start)
	{
		invoke(start, nullptr);
	}
}

void core::MonoScript::onUpdate()
{
	MonoMethod*	update = getMethod(Functions::FUNCTION_UPDATE);
	if (update)
	{
		invoke(update, nullptr);
	}
}

MonoMethod* core::MonoScript::getMethod(Functions function) const
{
	return m_methods[static_cast<size_t>(function)];
}

void core::MonoScript::invoke(MonoMethod* method, void** args) const
{
	MonoObject*	exception = nullptr;
	mono_runtime_invoke(method, m_scriptObject, args, &exception);
	if (exception)
	{
		MonoString* pMsg = mono_object_to_string(exception, nullptr);
		char*		message = mono_string_to_utf8(pMsg);
		LogCall(LOGLEVEL_ERROR) << '\n' << message << LOGFLUSH_FLUSH;
	}
}

void core::scripting::INTERNAL_set_enabled(MonoObject * thisObject, bool enabled)
{
}

bool core::scripting::INTERNAL_get_enabled(MonoObject * thisObject)
{
	return false;
}
