#pragma once

#include <map>
#include <list>
#include <vector>

#include "Object.h"
#include "Component.h"
#include "System.h"
#include "Entity.h"
#include "Asset.h"

namespace core
{
	class EngineData
	{
	public:
		~EngineData();

		static bool		initialize();

		static Object*	getObject(EntityId id);
		static void		addObject(Object* object);

		static constexpr int	getMaxObjectCount();

		template <typename T>
		static void		registerComponent(T* component)
		{
			const std::type_info&	componentType = typeid(T);
			size_t					componentHash = componentType.hash_code();
			m_components[componentHash].components()[component->getOwner()] = component;
		}

		template <typename T>
		static T*		getComponent(int gameObjectId)
		{
			const std::type_info&	componentType = typeid(T);
			size_t					componentHash = componentType.hash_code();
			auto&					components = m_components[componentHash].components();
			if (components.find(gameObjectId) != components.end())
			{
				Component*				component = components.at(gameObjectId);
				return dynamic_cast<T*>(component);
			}
			return nullptr;
		}

		static EntityId	reserveId();
		static void		releaseId(EntityId id);

		template <typename T>
		static System<Component>		getSystem()
		{
			const std::type_info&	componentType = typeid(T);
			size_t					componentHash = componentType.hash_code();
			return m_components[componentHash];
		}

		static void			getAllComponents(std::vector<Component*>& components, int gameObjectId);
		static Asset&		getAssetDirectory();
		static Asset*		getAssetDirectoryPtr();
		static void			setAssetDirectory(Asset* directory);

		static bool			isTypeOf(int objectId, core::AssetType type);
		static bool			isTypeChildOf(int objectId, core::AssetType type);

		template <typename T>
		static void			getAllObjectOfType(std::list<T*>& list, core::AssetType type, core::Asset* root = m_assetDirectory)
		{
			if (isTypeOf(root, type))
			{
				list.push_back(root);
			}

			auto&	childs = root->getChilds();
			for (auto& child : childs)
			{
				getAllObjectOfType(list, type, child);
			}
		}


		template <typename T>
		static void			getAllObjectOfTypeChildOf(std::list<T*>& list, core::AssetType type, core::Asset* root = m_assetDirectory)
		{
			if (isTypeOf(root, type))
			{
				list.push_back(root);
			}
			else if (isTypeChildOf(root, type))
			{
				list.push_back(root);
			}

			auto&	childs = root->getChilds();
			for (auto& child : childs)
			{
				getAllObjectOfTypeChildOf(list, type, child);
			}
		}

		static void			save();

		static void			addFileIdToEntity(unsigned long long fileId, EntityId entityId);

	private:
		EngineData();

		static bool			isTypeOf(Object* object, core::AssetType type);
		static bool			isTypeChildOf(Object* object, core::AssetType type);

		static std::map<EntityId, Object*>					m_objects;
		static std::list<EntityId>							m_availableObjects;
		static std::list<EntityId>							m_usedObjects;
		static std::map<size_t, System<Component>>			m_components;
		static std::map<AssetType, std::vector<AssetType>>	m_assetAncestor;
		static Asset*										m_assetDirectory;
		static std::map<unsigned long long, EntityId>		m_fileIdToEntity;
	};
}
