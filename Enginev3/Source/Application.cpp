#include "..\Include\Application.h"

namespace eg
{
	Application::Application(const START_UP_DESC& desc) :
		CoreApplication(desc),
		m_monoPlugin(nullptr),
		m_enginePlugin(nullptr)
	{
	}

	Application::~Application()
	{
	}

	Application& gApplication()
	{
		return static_cast<Application&>(Application::instance());
	}
}