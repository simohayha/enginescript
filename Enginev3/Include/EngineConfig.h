#pragma once

#define EG_RENDERER_MODULE "Render"
#define EG_RENDER_API_MODULE "D3D11RenderAPI"
#define EG_AUDIO_MODULE "OpenAudio"
#define EG_PHYSICS_MODULE "PhysX"
#define EG_INPUT_MODULE "OISInput"
