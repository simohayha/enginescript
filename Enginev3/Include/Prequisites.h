#pragma once

#include "CorePrequisites.h"

#if defined(EG_EXPORTS)
#define EG_EXPORT __declspec(dllexport)
#else
#define EG_EXPORT __declspec(dllimport)
#endif

#include "EngineConfig.h"