#pragma once

#include "Prequisites.h"
#include "CoreApplication.h"

namespace eg
{
	class EG_EXPORT Application : public CoreApplication
	{
	public:
		Application(const START_UP_DESC& desc);
		virtual ~Application();

		template <typename T = Application>
		static void startUp(const START_UP_DESC& desc)
		{
			CoreApplication::startUp<T>(desc);
		}

	protected:
		DynLib* m_monoPlugin;
		DynLib* m_enginePlugin;
	};

	EG_EXPORT Application& gApplication();
}