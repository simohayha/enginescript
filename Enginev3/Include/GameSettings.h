#pragma once

#include "Prequisites.h"

namespace eg
{
	class EG_EXPORT GameSettings
	{
	public:
		GameSettings() = default;

		String MainSceneUUID;
		bool Fullscreen = true;
		bool UseDesktopResolution = true;
		UINT32 ResolutionWidth = 1280u;
		UINT32 ResolutionHeight = 720u;
		WString TitleBarText;
	};
}