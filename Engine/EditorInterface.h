#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

namespace core
{
	class EditorInterface
	{
	public:
		EditorInterface();
		~EditorInterface();

		bool	initialize(HWND parent, size_t& width, size_t& height);
		void	shutdown();
		void	run();
		void	frame();
		void	pause();
	};
}
