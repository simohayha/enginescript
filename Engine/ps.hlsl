Texture2D		shaderTexture;
SamplerState	sampleType;

struct PixelInputType
{
	float4	position : SV_POSITION;
	float4	color : COLOR;
	float2	uv : TEXCOORD0;
};

float4 main(PixelInputType input) : SV_TARGET
{
	float4	textureColor;

	textureColor = shaderTexture.Sample(sampleType, input.uv);

	return textureColor * saturate(input.color);
}