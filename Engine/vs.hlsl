cbuffer MaterialBuffer
{
	float4	color;
};

cbuffer WorldMatrixBuffer
{
	matrix worldMatrix;
};

cbuffer CameraMatrixBuffer
{
	matrix	viewMatrix;
	matrix	projectionMatrix;
};

struct VertexInputType
{
	float4	position : POSITION;
	float3	normal : NORMAL;
	float3	tangent : TANGENT;
	float2	uv : TEXCOORD0;
};

struct PixelInputType
{
	float4	position : SV_POSITION;
	float4	color : COLOR;
	float2	uv : TEXCOORD0;
};

PixelInputType main(VertexInputType input)
{
	PixelInputType	output;

	input.position.w = 1.0f;

	output.position = mul(input.position, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	output.color = color;
	output.uv = input.uv;

	return output;
}