#include <windowsx.h>

#include <string>
#include <sstream>
#include <iostream>

#include "Game.h"
#include "Engine.h"

namespace core
{
	namespace
	{
		Engine*	appHandle = nullptr;
	}

	namespace mono
	{
		bool	isKeyDown(int key)
		{
			return appHandle->game().input().isKeyDown(key);
		}
	}

	static LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		PAINTSTRUCT	ps;
		HDC			hdc;

		std::cout << msg << '\n';

		switch (msg)
		{
		case WM_PAINT:
			hdc = BeginPaint(hwnd, &ps);
			EndPaint(hwnd, &ps);
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		case WM_SIZE:
			//if (appHandle->isInitialized())
			//{
			//	int width = LOWORD(lParam);
			//	int height = HIWORD(lParam);
			//	appHandle->game().graphic().resize(height, width);
			//}
			break;

		case WM_EXITSIZEMOVE:
			RECT	rc;
			GetClientRect(hwnd, &rc);
			appHandle->game().graphic().resize(rc.bottom - rc.top, rc.right - rc.left);

		case WM_KEYDOWN:
			appHandle->game().input().keyDown(static_cast<unsigned int>(wParam));
			break;

		case WM_KEYUP:
			appHandle->game().input().keyUp(static_cast<unsigned int>(wParam));
			break;

		case WM_MOUSEMOVE:
		{
			int	xPos = GET_X_LPARAM(lParam);
			int	yPos = GET_Y_LPARAM(lParam);
			appHandle->game().input().setMousePosition
			(
				static_cast<float>(xPos),
				static_cast<float>(yPos),
				0.0f
			);
		}
			break;

		case WM_LBUTTONDOWN:
			appHandle->game().input().mouseButtonDown(0);
			break;

		case WM_LBUTTONUP:
			appHandle->game().input().mouseButtonUp(0);
			break;

		case WM_MBUTTONDOWN:
			appHandle->game().input().mouseButtonDown(1);
			break;

		case WM_MBUTTONUP:
			appHandle->game().input().mouseButtonUp(1);
			break;

		case WM_RBUTTONDOWN:
			appHandle->game().input().mouseButtonDown(2);
			break;

		case WM_RBUTTONUP:
			appHandle->game().input().mouseButtonUp(2);
			break;

		default:
			return DefWindowProc(hwnd, msg, wParam, lParam);
		}

		return 0;
	}

	Engine::Engine() :
		m_game(nullptr),
		m_haveParent(nullptr)
	{
		appHandle = this;
	}

	Engine::~Engine()
	{
	}

	HWND Engine::initialize(HINSTANCE hInstance, int nCmdShow)
	{
		m_haveParent = false;

		bool	result = true;

		m_game.reset(new(std::nothrow) Game);
		if (!m_game)
		{
			return nullptr;
		}

		if (!initializeWindow(hInstance, nCmdShow))
		{
			return nullptr;
		}

		result = m_game->initialize(m_hwnd, 800, 600);
		if (!result)
		{
			return nullptr;
		}

		//m_mono.setup();
		//m_gameObject = m_mono.createGameObject();
		//m_mono.addInternalCall("Scripting.Input::GetKeyDownInt", mono::isKeyDown);

		return m_hwnd;
	}

	HWND Engine::intitialize(HWND parent, size_t width, size_t height)
	{
		m_haveParent = true;

		bool	result = true;

		m_width = width;
		m_height = height;

		if (!initializeWindow(parent, m_width, m_height))
		{
			return nullptr;
		}

		m_game.reset(new(std::nothrow) Game);
		if (!m_game)
		{
			return nullptr;
		}

		result = m_game->initialize(parent, width, height);
		if (!result)
		{
			return nullptr;
		}

		//m_mono.setup();
		//m_gameObject = m_mono.createGameObject();
		//m_mono.addInternalCall("Scripting.Input::GetKeyDownInt", mono::isKeyDown);

		return m_hwnd;
	}

	void Engine::shutdown()
	{
		if (m_game)
		{
			m_game->shutdown();
			m_game.reset(nullptr);
		}

		shutdownWindow();
	}

	void Engine::run()
	{
		if (!m_haveParent)
		{
			MSG msg = { 0 };
			while (WM_QUIT != msg.message)
			{
				if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
				else
				{
					onRun();
				}
			}
		}
		else
		{
			onRun();
		}
	}

	void Engine::update()
	{
		m_game->update();
	}

	void Engine::render()
	{
		m_game->render();
	}

	LRESULT Engine::messageHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		onRun();

		return 0;
	}

	void Engine::resizeWindow(int height, int width)
	{
		if (width < 10 || height < 10)
		{
			return;
		}

		m_game->graphic().resize(height, width);
	}

	void Engine::setFeedbackFct(std::function<void()> fct)
	{
		m_feedback = fct;
	}

	Game& Engine::game()
	{
		return *m_game.get();
	}

	const Game& Engine::game() const
	{
		return *m_game.get();
	}

	bool Engine::isInitialized() const
	{
		return m_game.get() != nullptr && m_game->isInitialized();
	}

	void Engine::pause()
	{
		m_game->pause();
	}

	int Engine::addGameObject(int parent)
	{
		return m_game->addGameObject(parent);
	}

	void Engine::getRootGameObjects(std::vector<int>& gameObjects)
	{
		m_game->getRootGameObjects(gameObjects);
	}

	bool Engine::initializeWindow(HWND parent, size_t& width, size_t& height)
	{
		WNDCLASSEX	wc;
		int posX = (GetSystemMetrics(SM_CXSCREEN) - static_cast<int>(width)) / 2;
		int posY = (GetSystemMetrics(SM_CYSCREEN) - static_cast<int>(height)) / 2;

		m_applicationName = L"DirectX";
		appHandle = this;
		m_hInstance = GetModuleHandle(nullptr);
		wc.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
		wc.lpfnWndProc = WndProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = m_hInstance;
		wc.hIcon = LoadIcon(nullptr, IDI_WINLOGO);
		wc.hIconSm = wc.hIcon;
		wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		wc.lpszMenuName = nullptr;
		wc.lpszClassName = m_applicationName;
		wc.cbSize = sizeof(wc);
		RegisterClassEx(&wc);
		m_hwnd = CreateWindowEx(0, m_applicationName, m_applicationName,
			WS_CHILD | WS_VISIBLE | LBS_NOTIFY | WS_BORDER,
			0, 0, static_cast<int>(width), static_cast<int>(height), parent, (HMENU)0x00000001,
			nullptr, nullptr);
		ShowWindow(m_hwnd, SW_SHOW);
		SetForegroundWindow(m_hwnd);
		SetFocus(m_hwnd);

		return true;
	}

	bool Engine::initializeWindow(HINSTANCE hInstance, int nCmdShow)
	{
		WNDCLASSEX wcex;
		wcex.cbSize = sizeof(WNDCLASSEX);
		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = WndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hInstance;
		wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)107);
		wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wcex.lpszMenuName = nullptr;
		wcex.lpszClassName = L"DirectX";
		wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)107);
		if (!RegisterClassEx(&wcex))
			return false;

		// Create window
		m_hInstance = hInstance;
		RECT rc = { 0, 0, 800, 600 };
		AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
		m_hwnd = CreateWindow(L"DirectX", L"DirectX",
			WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_SIZEBOX,
			CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, hInstance,
			nullptr);
		if (!m_hwnd)
			return false;

		ShowWindow(m_hwnd, nCmdShow);

		return true;
	}

	void Engine::shutdownWindow()
	{
		if (FULL_SCREEN)
		{
			ChangeDisplaySettings(nullptr, 0);
		}

		DestroyWindow(m_hwnd);
		m_hwnd = nullptr;

		UnregisterClass(m_applicationName, m_hInstance);
		m_hInstance = nullptr;

		appHandle = nullptr;
	}

	bool Engine::onRun()
	{
		bool result = true;

		m_game->update(false);
		m_game->render();

		return result;
	}

	void Engine::frame()
	{
		//if (m_game->isPaused())
		//{
			m_game->update(true);
			m_game->render();
		//}
	}

	Asset*	Engine::getAssetFolder()
	{
		return m_game->getAssetFolder();
	}

	AssetType Engine::getAssetType(int objectId)
	{
		return m_game->getAssetType(objectId);
	}

	Object* Engine::getObject(int objectId)
	{
		return m_game->getObject(objectId);
	}

}