#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <memory>
#include <list>
#include <functional>
#include <vector>

//#include "Mono.h"
#include "Game.h"

namespace core
{
	class Engine
	{
	public:
		Engine();
		~Engine();

		HWND				initialize(HINSTANCE hInstance, int nCmdShow);
		HWND				intitialize(HWND parent, size_t width, size_t height);
		void				shutdown();
		void				run();
		void				update();
		void				render();
		void				frame();

		LRESULT CALLBACK	messageHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

		void				resizeWindow(int height, int width);

		void				setFeedbackFct(std::function<void()> fct);

		Game&				game();
		const Game&			game() const;

		bool				isInitialized() const;

		void				pause();

		int					addGameObject(int parent);
		void				getRootGameObjects(std::vector<int>& gameObjects);
		Asset*				getAssetFolder();
		AssetType			getAssetType(int objectId);
		Object*				getObject(int objectId);

		template <class T>
		T*	getComponent(int gameObjectId)
		{
			return m_game->getComponent<T>(gameObjectId);
		}

	private:
		std::unique_ptr<Game>		m_game;

		bool						m_haveParent;
		size_t						m_width;
		size_t						m_height;
		HWND						m_hwnd;
		LPCWSTR						m_applicationName;
		HINSTANCE					m_hInstance;

		std::function<void()>		m_feedback;

		//scripting::Mono				m_mono;
		//MonoObject*					m_gameObject;

		bool	initializeWindow(HWND parent, size_t& width, size_t& height);
		bool	initializeWindow(HINSTANCE hInstance, int nCmdShow);
		void	shutdownWindow();
		bool	onRun();
	};
}
