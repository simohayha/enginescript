#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <memory>
#include <functional>
#include <vector>

#include "Engine.h"
#include "EngineExport.h"

namespace core
{
	class Engine;
	class Asset;
}

class CUnmanaged
{
public:
	class Feedback
	{
	public:
		virtual void	onClick() = 0;

		//virtual void	onNewGameObject() = 0;
	};

	CUnmanaged(Feedback* feedback);

	HWND			initialize(HWND parent, size_t width, size_t height);
	HWND			initialize(HINSTANCE hInstance, int nCmdShow);
	void			run(HWND parent);
	void			update();
	void			render();
	void			frame();
	void			resize(int height = 0, int width = 0);
	void			pause();
	int				addGameObject(int parent);
	void			getRootGameObjects(std::vector<int>& gameObjects);
	core::Asset*	getAssetFolder();
	core::AssetType	getAssetType(int objectid);
	core::Object*	getObject(int objectid);

	template <class T>
	T*	getComponent(int gameObjectId)
	{
		return m_engine->getComponent<T>(gameObjectId);
	}

private:
	Feedback*						m_feedback;
	std::unique_ptr<core::Engine>	m_engine;
};