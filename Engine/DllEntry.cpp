#include "Engine.h"
#include "DllEntry.h"

CUnmanaged::CUnmanaged(Feedback* feedback) :
	m_feedback(feedback)
{
}

HWND CUnmanaged::initialize(HWND parent, size_t width, size_t height)
{
	m_engine.reset(new core::Engine());

	m_engine->setFeedbackFct(std::bind(&CUnmanaged::Feedback::onClick, m_feedback));

	HWND ret = m_engine->intitialize(parent, width, height);

	return ret;
}

HWND CUnmanaged::initialize(HINSTANCE hInstance, int nCmdShow)
{
	m_engine.reset(new core::Engine());

	m_engine->setFeedbackFct(std::bind(&CUnmanaged::Feedback::onClick, m_feedback));

	HWND ret = m_engine->initialize(hInstance, nCmdShow);

	return ret;
}

void CUnmanaged::run(HWND parent)
{
	m_engine->run();
}

void CUnmanaged::update()
{
	m_engine->update();
}

void CUnmanaged::render()
{
	m_engine->render();
}

void CUnmanaged::frame()
{
	m_engine->frame();
}

void CUnmanaged::resize(int height, int width)
{
	m_engine->resizeWindow(height, width);
}

void CUnmanaged::pause()
{
	m_engine->pause();
}

int CUnmanaged::addGameObject(int parent)
{
	return m_engine->addGameObject(parent);
}

void CUnmanaged::getRootGameObjects(std::vector<int>& gameObjects)
{
	m_engine->getRootGameObjects(gameObjects);
}

core::AssetType CUnmanaged::getAssetType(int objectid)
{
	return m_engine->getAssetType(objectid);
}

core::Object* CUnmanaged::getObject(int objectid)
{
	return m_engine->getObject(objectid);
}

core::Asset* CUnmanaged::getAssetFolder()
{
	return m_engine->getAssetFolder();
}