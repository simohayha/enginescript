#pragma once

#include <sstream>

#include "PrequisitesUtils.h"

namespace eg
{
	class Exception : public std::exception
	{
	public:
		Exception(const char* type, const String& description, const String& source) :
			m_line(0l),
			m_typeName(type),
			m_description(description),
			m_source(source)
		{
		}

		Exception(const char* type, const String& description, const String& source, const char* file, long line) :
			m_line(line),
			m_typeName(type),
			m_description(description),
			m_source(source),
			m_file(file)
		{
		}

		Exception(const Exception& rhs) :
			m_line(rhs.m_line),
			m_typeName(rhs.m_typeName),
			m_description(rhs.m_description),
			m_source(rhs.m_source),
			m_file(rhs.m_file)
		{
		}

		~Exception() throw() = default;

		void operator=(const Exception& rhs)
		{
			m_description = rhs.m_description;
			m_source = rhs.m_source;
			m_file = rhs.m_file;
			m_line = rhs.m_line;
			m_typeName = rhs.m_typeName;
		}

		virtual const String& getFullDescription() const
		{
			if (m_fullDesc.empty())
			{
				StringStream desc;
			}
		}

	protected:
		long			m_line;
		String			m_typeName;
		String			m_description;
		String			m_source;
		String			m_file;
		mutable String	m_fullDesc;
	};
}