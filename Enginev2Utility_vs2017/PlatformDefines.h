#pragma once

#define EG_THREADLOCAL __declspec(thread)

#if defined(EG_UTILITY_EXPORTS)
#define EG_UTILITY_EXPORT __declspec(dllexport)
#else
#define EG_UTILITY_EXPORT __declspec(dllimport)
#endif
