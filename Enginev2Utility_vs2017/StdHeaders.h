#pragma once

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cstdarg>
#include <cmath>

#include <memory>

// STL containers
#include <vector>
#include <stack>
#include <map>
#include <string>
#include <set>
#include <list>
#include <deque>
#include <queue>
#include <bitset>
#include <array>

#include <unordered_map>
#include <unordered_set>

// STL algorithms & functions
#include <algorithm>
#include <functional>
#include <limits>

// C++ Stream stuff
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>

#include "EnumClassHash.h"

namespace eg
{
	template <typename Key>
	using HashType = typename std::conditional<std::is_enum<Key>::value, EnumClassHash, std::hash<Key>>::type;

	template <typename T, typename A = StdAlloc<T>>
	using Deque = std::deque<T, A>;

	template <typename T, typename A = StdAlloc<T>>
	using Vector = std::vector<T, A>;

	template <typename T, typename A = StdAlloc<T>>
	using List = std::list<T, A>;

	template <typename T, typename A = StdAlloc<T>>
	using Stack = std::stack<T, A>;

	template <typename T, typename A = StdAlloc<T>>
	using Queue = std::queue<T, A>;

	template <typename T, typename P = std::less<T>, typename A = StdAlloc<T>>
	using Set = std::set<T, P, A>;

	template <typename K, typename V, typename P = std::less<K>, typename A = StdAlloc<std::pair<const K, V>>>
	using Map = std::map<K, V, P, A>;

	template <typename K, typename V, typename P = std::less<K>, typename A = StdAlloc<std::pair<const K, V>>>
	using MultiMap = std::multimap<K, V, P, A>;

	template <typename T, typename H = HashType<K>, typename C = std::equal_to<T>, typename A = StdAlloc<T>>
	using UnorderedSet = std::unordered_set<T, H, C, A>;

	template <typename K, typename V, typename H = HashType<K>, typename C = std::equal_to<T>, typename A = StdAlloc<std::pair<const K, V>>>
	using UnorderedMap = std::unordered_map<K, V, H, C, A>;

	template <typename K, typename V, typename H = HashType<K>, typename C = std::equal_to<K>, typename A = StdAlloc<std::pair<const K, V>>>
	using UnorderedMultimap = std::unordered_multimap<K, V, H, C, A>;

	template <typename T>
	using SPtr = std::shared_ptr<T>;

	template <typename T, typename Alloc = GenAlloc>
	using UPtr = std::unique_ptr<T, decltype(&eg_delete<T, Alloc>)>;

	template <class Type, class AllocCategory, class... Args>
	SPtr<Type> eg_shared_ptr_new(Args&&... args)
	{
		return std::allocate_shared<Type>(StdAlloc<Type, AllocCategory>(), std::forward<Args>(args)...);
	}

	template <class Type, class... Args>
	SPtr<Type> eg_shared_ptr_new(Args&&... args)
	{
		return std::allocate_shared<Type>(StdAlloc<Type, GenAlloc>(), std::forward<Args>(args)...);
	}

	template <class Type, class MainAlloc = GenAlloc, class PtrDataAlloc = GenAlloc>
	SPtr<Type> eg_shared_ptr(Type* data)
	{
		return SPtr<Type>(data, &eg_delete<Type, MainAlloc>, StdAlloc<Type, PtrDataAlloc>());
	}

	template <class Type, class Alloc = GenAlloc>
	UPtr<Type, Alloc> eg_unique_ptr(Type* data)
	{
		return std::unique_ptr<Type, decltype(&eg_delete<Type, Alloc>)>(data, eg_delete<Type, Alloc>);
	}

	template <class Type, class Alloc, class... Args>
	UPtr<Type> eg_unique_ptr_new(Args&&... args)
	{
		Type* rawPtr = eg_new<Type, GenAlloc>(std::forward<Args>(args)...);

		return eg_unique_ptr<Type, Alloc>(rawPtr);
	}

	template <class Type, class... Args>
	UPtr<Type> eg_unique_ptr_new(Args&&... args)
	{
		Type* rawPtr = eg_new<Type, GenAlloc>(std::forward<Args>(args)...);

		return eg_unique_ptr<Type, GenAlloc>(rawPtr);
	}
}