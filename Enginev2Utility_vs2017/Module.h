#pragma once

#include "PrequisitesUtils.h"

namespace eg
{
	template <class T>
	class Module
	{
	public:
		static T& instance()
		{
		}

		static void shutDown()
		{
			if (isDestroyed())
			{

			}

			if (!isStartedUp())
			{

			}

			static_cast<Module*>(_instance())->onShutDown();
		}

		static bool isStarted()
		{
			return isStartedUp() && !isDestroyed();
		}

	protected:
		Module() = default;

		virtual ~Module()
		{
			_instance() = nullptr;
			isDestroyed() = true;
		}

		Module(const Module&) = default;
		Module& operator=(const Module&) = default;

		virtual void onStartUp()
		{
		}

		virtual void onShutDown()
		{
		}

		static T*& _instance()
		{
			static T* inst = nullptr;
			return inst;
		}

		static bool& isDestroyed()
		{
			static bool	inst = false;
			return inst;
		}

		static bool& isStartedUp()
		{
			static bool	inst = false;
			return inst;
		}
	};
}