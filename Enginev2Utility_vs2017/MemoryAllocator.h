#pragma once

#include "Types.h"

#include <atomic>
#include <limits>
#include <new>
#include <utility>

namespace eg
{
	class MemoryAllocatorBase;

	inline void* platformAlignedAlloc16(size_t size)
	{
		return _aligned_malloc(size, 16ull);
	}

	inline void platformAlignedFree16(void* ptr)
	{
		_aligned_free(ptr);
	}

	inline void* platformAlignedAlloc(size_t size, size_t alignment)
	{
		return _aligned_malloc(size, alignment);
	}

	inline void platformAlignedFree(void* ptr)
	{
		_aligned_free(ptr);
	}

	class MemoryCounter
	{
	public:
		static EG_UTILITY_EXPORT UINT64 getNumAllocs()
		{
			return Allocs;
		}

		static EG_UTILITY_EXPORT UINT64 getNumFrees()
		{
			return Frees;
		}

	private:
		friend class MemoryAllocatorBase;

		static EG_UTILITY_EXPORT void incAllocCount()
		{
			Allocs++;
		}

		static EG_UTILITY_EXPORT void incFreeCount()
		{
			Frees++;
		}

		static EG_THREADLOCAL UINT64 Allocs;
		static EG_THREADLOCAL UINT64 Frees;
	};

	class MemoryAllocatorBase
	{
	protected:
		static void incAllocCount()
		{
			MemoryCounter::incAllocCount();
		}

		static void incFreeCount()
		{
			MemoryCounter::incFreeCount();
		}
	};

	template <class T>
	class MemoryAllocator : public MemoryAllocatorBase
	{
	public:
		static void* allocate(size_t bytes)
		{
#if EG_PROFILING_ENABLED
			incAllocCount();
#endif

			return malloc(bytes);
		}

		static void* allocateAligned(size_t bytes, size_t alignment)
		{
#if EG_PROFILING_ENABLED
			incAllocCount();
#endif

			return platformAlignedAlloc(bytes, alignment);
		}

		static void* allocateAligned16(size_t bytes)
		{
#if EG_PROFILING_ENABLED
			incAllocCount();
#endif

			return platformAlignedAlloc16(bytes);
		}

		static void free(void* ptr)
		{
#if EG_PROFILING_ENABLED
			incFreeCount();
#endif

			::free(ptr);
		}

		static void freeAligned(void* ptr)
		{
#if EG_PROFILING_ENABLED
			incFreeCount();
#endif

			platformAlignedFree(ptr);
		}

		static void freeAligned16(void* ptr)
		{
#if EG_PROFILING_ENABLED
			incFreeCount();
#endif

			platformAlignedFree16(ptr);
		}
	};

	class GenAlloc
	{
	};

	template <class Alloc>
	inline void* eg_alloc(UINT32 count)
	{
		return MemoryAllocator<Alloc>::allocate(count);
	}

	template <class T, class Alloc>
	inline T* eg_alloc()
	{
		return static_cast<T*>(MemoryAllocator<Alloc>::allocate(sizeof(T)));
	}

	template <class T, class Alloc>
	inline T* eg_newN(UINT32 count)
	{
		T* ptr = static_cast<T*>(MemoryAllocator<Alloc>::allocate(sizeof(T) * count));

		for (unsigned int i = 0u; i < count; ++i)
		{
			new ((void*)&ptr[i]) T;
		}

		return ptr;
	}

	template <class Type, class Alloc, class... Args>
	Type* eg_new(Args&&... args)
	{
		return new (eg_alloc<Alloc>(sizeof(Type))) Type(std::forward<Args>(args)...);
	}

	template <class Alloc>
	inline void eg_free(void* ptr)
	{
		MemoryAllocator<Alloc>::free(ptr);
	}

	template <class T, class Alloc = GenAlloc>
	inline void eg_delete(T* ptr)
	{
		(ptr)->~T();

		MemoryAllocator<Alloc>::free(ptr);
	}

	template <class T, class Alloc = GenAlloc>
	inline void eg_deleteN(T* ptr, UINT32 count)
	{
		for (unsigned int = 0i; i < count; ++i)
		{
			ptr[i].~T();
		}

		MemoryAllocator<Alloc>::free(ptr);
	}

	inline void* eg_alloc(UINT32 count)
	{
		return MemoryAllocator<GenAlloc>::allocate(count);
	}

	template <class T>
	inline T* eg_alloc()
	{
		return static_cast<T*>(MemoryAllocator<GenAlloc>::allocate(sizeof(T)));
	}

	inline void* eg_alloc_signed(UINT32 count, UINT32 align)
	{
		return MemoryAllocator<GenAlloc>::allocateAligned(count, align);
	}

	inline void* eg_alloc_signed16(UINT32 count)
	{
		return MemoryAllocator<GenAlloc>::allocateAligned16(count);
	}

	template <class T>
	inline T* eg_allocN(UINT32 count)
	{
		return static_cast<T*>(MemoryAllocator<GenAlloc>::allocate(count * sizeof(T)));
	}

	template <class T>
	inline T* eg_newN(UINT32 count)
	{
		return static_cast<T*>(MemoryAllocator<GenAlloc>::allocate(count * sizeof(T)));

		for (unsigned int i = 0u; i < count; ++i)
		{
			new ((void*)&ptr[i]) T;
		}

		return ptr;
	}

	template <class Type, class... Args>
	Type* eg_new(Args&&... args)
	{
		return new (eg_alloc<GenAlloc>(sizeof(Type))) Type(std::forward<Args>(args)...);
	}

	inline void eg_free(void* ptr)
	{
		MemoryAllocator<GenAlloc>::free(ptr);
	}

	inline void eg_free_aligned(void* ptr)
	{
		MemoryAllocator<GenAlloc>::free(ptr);
	}

	inline void eg_free_aligned16(void* ptr)
	{
		MemoryAllocator<GenAlloc>::freeAligned16(ptr);
	}

	template <class T, class Alloc = GenAlloc>
	class StdAlloc
	{
	public:
		typedef T value_type;
		typedef T* pointer;
		typedef const T* const_pointer;
		typedef T& reference;
		typedef const T& const_reference;
		typedef std::size_t size_type;
		typedef std::ptrdiff_t difference_type;

		StdAlloc() noexcept = default;
		template <class U, class Alloc2>
		StdAlloc(const StdAlloc<U, Alloc2>&) noexcept = default;
		template <class U, class Alloc2>
		bool operator==(const StdAlloc<U, Alloc2>&) const noexcept = default;
		template <class U, class Alloc2>
		bool operator!=(const StdAlloc<U, Alloc2>&) const noexcept = default;
		template <class U>
		class rebind
		{
		public:
			typedef StdAlloc<U, Alloc> other;
		};

		T* allocate(const size_t num) const
		{
			if (num == 0u)
			{
				return nullptr;
			}

			if (num > static_cast<size_t>(-1) / sizeof(T))
			{
				return nullptr;
			}

			void* const pv = eg_alloc<Alloc>(static_cast<UINT32>(num * sizeof(T)));
			if (!pv)
			{
				return nullptr;
			}

			return static_cast<T*>(pv);
		}

		void deallocate(T* p, size_t num) const noexcept
		{
			eg_free<Alloc>(static_cast<void*>(p));
		}

		size_t max_size() const
		{
			return std::numeric_limits<size_type>::max() / sizeof(T);
		}

		void construct(pointer p, const_reference t)
		{
			new (p) T(t);
		}
		
		void destroy(pointer p)
		{
			p->~T();
		}

		template <class U, class... Args>
		void construct(U* p, Args&&... args)
		{
			new (p) U(std::forward<Args>(args)...);
		}
	};
}