#pragma once

#include "MemoryAllocator.h"

#include <string>

namespace eg
{
	template <typename T>
	using BasicString = std::basic_string<T, std::char_traits<T>, StdAlloc<T>>;

	template <typename T>
	using BasicStringStream = std::basic_stringstream<T, std::char_traits<T>, StdAlloc<T>>;

	typedef BasicString<wchar_t> WString;

	typedef BasicString<char> String;

	typedef BasicStringStream<wchar_t> WStringStream;

	typedef BasicStringStream<char> StringStream;
}