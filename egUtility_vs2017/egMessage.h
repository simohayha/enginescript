#pragma once

#include "egUtilityPrequisites.h"

namespace eg
{
	enum class MessageType
	{
		LogInfo,
		LogWarning,
		LogError
	};

	class EG_UTILITY_EXPORT Message
	{
	public:
		Message() = default;
		virtual ~Message() = default;

		MessageType Type;
		Map<String, String> Data;
	};
}