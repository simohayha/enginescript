#include "egDebug.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <iostream>

void logToIDEConsole(const eg::String& message)
{
	OutputDebugString(message.c_str());
	OutputDebugString("\n");

	std::cout << message << std::endl;
}

namespace eg
{
	void Debug::logDebug(const String& message)
	{
		logToIDEConsole(message);
	}

	void Debug::logWarning(const String& message)
	{
		logToIDEConsole(message);
	}

	void Debug::logError(const String& message)
	{
		logToIDEConsole(message);
	}

	void Debug::log(const String& message, int32 channel)
	{
		logToIDEConsole(message);
	}

	Log& Debug::getLog()
	{
		return m_log;
	}

	Debug& gDebug()
	{
		static Debug inst;
		return inst;
	}
}