#pragma once

#include "egUtilityPrequisites.h"
#include "egSystem.h"

namespace eg
{
	class EG_UTILITY_EXPORT MessageBus
	{
	public:
		MessageBus() = default;
		virtual ~MessageBus() = default;

		void sendMessage(Message* message);

	private:
		std::vector<System*> m_systems;
	};

	EG_UTILITY_EXPORT MessageBus& gMessageBus();
}
