#include "egConsole.h"
#include "egMessage.h"
#include "egDebug.h"

namespace eg
{
	Console::Console()
	{
	}

	Console::~Console()
	{
	}

	void Console::onHandleMessage(Message* message)
	{
		String log;

		switch (message->Type)
		{
		case MessageType::LogInfo:
		{
			log = message->Data["Log"];
			gDebug().logDebug(log);
		}
			break;
		case MessageType::LogWarning:
		{
			log = message->Data["Log"];
			gDebug().logWarning(log);
		}
			break;
		case MessageType::LogError:
		{
			log = message->Data["Log"];
			gDebug().logError(log);
		}
			break;
		}
	}
}