#pragma once

#include "egUtilityPrequisites.h"

namespace eg
{
	template <class T>
	class System
	{
	public:
		static T& instance()
		{
			if (!isStartedUp())
			{
				throw;
			}

			if (isDestroyed())
			{
				throw;
			}

			return *_instance();
		}

		static T* instancePtr()
		{
			if (!isStartedUp())
			{
				throw;
			}

			if (isDestroyed())
			{
				throw;
			}

			return _instance();
		}

		template <class... Args>
		static void startUp(Args&&... args)
		{
			if (isStartedUp())
			{
				throw;
			}

			_instance() = new T(std::forward<Args>(args)...);
			isStartedUp() = true;

			static_cast<System*>(_instance())->onStartUp();
		}

		template <class SubType, class... Args>
		static void startUp(Args&&... args)
		{
			static_cast(std::is_base_of<T, SubType>::value, "Provided type is not derived from type the Module is initialized with.");

			if (isStartedUp())
			{
				throw;
			}

			_instance() = new T(std::forward<Args>(args)...);
			isStartedUp() = true;

			static_cast<System*>(_instance())->onStartUp();
		}

		static void shutDown()
		{
			if (isDestroyed())
			{
				throw;
			}

			if (!isStartedUp())
			{
				throw;
			}

			static_cast<System*>(_instance())->onShutDown();

			delete _instance();
			isDestroyed() = true;
		}

		static bool isStarted()
		{
			return isStartedUp() && !isDestroyed();
		}

		static void handleMessage(Message* message)
		{
			static_cast<System*>(_instance())->onHandleMessage(message);
		}

	protected:
		MessageBus* m_messageBus;

	protected:
		System() :
			m_messageBus(nullptr)
		{
		}

		virtual ~System()
		{
			_instance() = nullptr;
			isDestroyed() = true;
		}

		System(const System&) = default;
		System& operator=(const System&) = default;

		virtual void onStartUp()
		{
		}

		virtual void onShutDown()
		{
		}

		virtual void onHandleMessage(Message* message)
		{
		}

		static T*& _instance()
		{
			static T* inst = nullptr;
			return inst;
		}

		static bool& isDestroyed()
		{
			static bool inst = false;
			return inst;
		}

		static bool& isStartedUp()
		{
			static bool inst = false;
			return inst;
		}
	};
}
