#pragma once

#include <thread>
#include <chrono>
#include <mutex>
#include <condition_variable>

namespace eg
{
	using Mutex = std::mutex;

	using RecursiveMutex = std::recursive_mutex;

	using Signal = std::condition_variable;

	using Thread = std::thread;

	using ThreadId = std::thread::id;

	using Lock = std::unique_lock<Mutex>;

	using RecursiveLock = std::unique_lock<RecursiveMutex>;
}