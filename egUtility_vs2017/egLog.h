#pragma once

#include "egUtilityPrequisites.h"

namespace eg
{
	class EG_UTILITY_EXPORT LogEntry
	{
	public:
		LogEntry() = default;
		LogEntry(const String& message, int32 channel);

		int32 getChannel() const;
		const String& getMessage() const;

	private:
		String m_message;
		int32 m_channel;
	};

	class EG_UTILITY_EXPORT Log
	{
	public:
		Log() = default;
		~Log() = default;

		void logMessage(const String& message, int32 channel);

	private:
		Queue<LogEntry> m_unreadEntries;
		mutable RecursiveMutex m_mutex;
	};
}