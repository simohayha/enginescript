#pragma once

#pragma warning (disable: 4251)

#include "egDefines.h"
#include "egThreadDefines.h"
#include "egStdHeaders.h"
#include "egUtilityForwardDecl.h"

#include "egTypes.h"

#include "egString.h"