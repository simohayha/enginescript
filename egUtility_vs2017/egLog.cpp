#include "egLog.h"

namespace eg
{
	LogEntry::LogEntry(const String& message, int32 channel) :
		m_message(message),
		m_channel(channel)
	{
	}

	int32 LogEntry::getChannel() const
	{
		return m_channel;
	}

	const String& LogEntry::getMessage() const
	{
		return m_message;
	}

	void Log::logMessage(const String& message, int32 channel)
	{
		RecursiveLock lock(m_mutex);

		m_unreadEntries.push(LogEntry(message, channel));
	}
}