#pragma once

#include <string>
#include <sstream>

namespace eg
{
	using String = std::string;

	using WString = std::wstring;

	using StringStream = std::stringstream;

	using WStringStream = std::wstringstream;
}