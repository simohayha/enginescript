#pragma once

#include "egUtilityPrequisites.h"
#include "egLog.h"

namespace eg
{
	enum class DebugChannel
	{
		Debug,
		Warning,
		Error,
		CompilerWarning,
		CompilerError
	};

	class EG_UTILITY_EXPORT Debug
	{
	public:
		Debug() = default;

		void logDebug(const String& message);
		void logWarning(const String& message);
		void logError(const String& message);
		void log(const String& message, int32 channel);

		Log& getLog();

	private:
		Log m_log;
	};

	EG_UTILITY_EXPORT Debug& gDebug();
}