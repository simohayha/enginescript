#pragma once

#include "egUtilityPrequisites.h"
#include "egSystem.h"

namespace eg
{
	class Console : System<Console>
	{
	public:
		Console();
		virtual ~Console();

	protected:
		virtual void onHandleMessage(Message* message) override;
	};
}