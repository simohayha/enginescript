#pragma once

#include "egCorePrequisites.h"
#include "egSystem.h"

namespace eg
{
	class EG_CORE_EXPORT CoreApplication : public System<CoreApplication>
	{
	public:
		CoreApplication() = default;
		virtual ~CoreApplication();

		void runMainLoop();

		void stopMainLoop();
	};
}