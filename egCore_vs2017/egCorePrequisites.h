#pragma once

#include "egUtilityPrequisites.h"

#if defined(EG_CORE_EXPORTS)
#define EG_CORE_EXPORT __declspec(dllexport)
#else
#define EG_CORE_EXPORT __declspec(dllimport)
#endif