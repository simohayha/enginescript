#include <exception>
#include <iostream>

#include "Application.h"
#include "GameSettings.h"

void runApplication();

using namespace eg;

int main(int ac, char* av[])
{
	try
	{
		runApplication();
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}

	return 0;
}

void runApplication()
{
	unsigned int resolutionWidth = 200u;
	unsigned int resolutionHeight = 200u;

	START_UP_DESC startUpDesc;
	startUpDesc.renderAPI = EG_RENDER_API_MODULE;
	startUpDesc.renderer = EG_RENDERER_MODULE;
	startUpDesc.audio = EG_AUDIO_MODULE;
	startUpDesc.physics = EG_PHYSICS_MODULE;
	startUpDesc.input = EG_INPUT_MODULE;
	startUpDesc.scripting = true;

	startUpDesc.primaryWindowDesc.videoMode = VideoMode(resolutionWidth, resolutionHeight);
	startUpDesc.primaryWindowDesc.title = "Game";
	startUpDesc.primaryWindowDesc.fullscreen = false;
	startUpDesc.primaryWindowDesc.hidden = false;
	startUpDesc.primaryWindowDesc.depthBuffer = false;

	Application::startUp(startUpDesc);

	Application::instance().runMainLoop();
	Application::instance().shutDown();
}