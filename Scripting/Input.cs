﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Scripting
{
    public sealed class Input
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void INTERNAL_get_mousePosition(out Vector3 value);

        public static Vector3 mousePosition
        {
            get
            {
                Vector3 result;
                INTERNAL_get_mousePosition(out result);
                return result;
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool GetKeyDownInt(int key);

        public static bool GetKeyDown(KeyCode key)
        {
            return GetKeyDownInt((int)key);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool GetKeyInt(int key);

        public static bool GetKey(KeyCode key)
        {
            return GetKeyInt((int)key);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern bool GetKeyUpInt(int key);

        public static bool GetKeyUp(KeyCode key)
        {
            return GetKeyUpInt((int)key);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool GetMouseButtonDown(int button);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool GetMouseButton(int button);
        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern bool GetMouseButtonUp(int button);

        public static extern bool anyKeyDown
        {
            [MethodImpl(MethodImplOptions.InternalCall)]
            get;
        }

        public static extern bool anyKey
        {
            [MethodImpl(MethodImplOptions.InternalCall)]
            get;
        }
    }
}
