﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Scripting
{
    public sealed class Debug
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_Log(LogType logType, string msg, Object obj);

        private static string GetString(object message)
        {
            return (message == null) ? "Null" : message.ToString();
        }

        public static void Log(object message)
        {
            Log(LogType.Log, message);
        }

        private static void Log(LogType logType, object message)
        {
            LogFormat(logType, null, "{0}", new object[]
            {
                GetString(message)
            });
        }

        private static void LogFormat(LogType logType, Object context, string format, params object[] args)
        {
            INTERNAL_Log(logType, string.Format(format, args), context);
        }
    }
}
