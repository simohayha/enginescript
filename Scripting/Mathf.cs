﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripting
{
    public struct Mathf
    {
        public static float Sqrt(float f)
        {
            return (float)Math.Sqrt((double)f);
        }
    }
}
