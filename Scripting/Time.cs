﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Scripting
{
    public sealed class Time
    {
        public static extern float deltaTime
        {
            [MethodImpl(MethodImplOptions.InternalCall)]
            get;
        }

        public static extern float timeScale
        {
            [MethodImpl(MethodImplOptions.InternalCall)]
            get;
            [MethodImpl(MethodImplOptions.InternalCall)]
            set;
        }

        public static extern float unscaledTime
        {
            [MethodImpl(MethodImplOptions.InternalCall)]
            get;
        }
    }
}
