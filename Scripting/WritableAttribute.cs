﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripting
{
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
    internal class WritableAttribute : Attribute
    {
    }
}
