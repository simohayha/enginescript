﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Scripting
{
    [StructLayout(LayoutKind.Sequential)]
    class ScriptableObject : Object
    {
        public ScriptableObject()
        {
            Internal_CreateScriptableObject(this);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void Internal_CreateScriptableObject([Writable] ScriptableObject self);

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern ScriptableObject CreateInstance(string className);

        public static ScriptableObject CreateInstance(Type type)
        {
            return CreateInstanceFromType(type);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern ScriptableObject CreateInstanceFromType(Type type);

        public static T CreateInstance<T>() where T : ScriptableObject
        {
            return (T)(CreateInstance(typeof(T)));
        }
    }
}
