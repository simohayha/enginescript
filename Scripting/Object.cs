﻿using Scripting.Components;
using Scripting.Internals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Scripting
{
    [StructLayout(LayoutKind.Sequential)]
    public class Object
    {
        private IntPtr cachedPtr;

        private IntPtr nativeHandle = (IntPtr)0;
        private int instanceID;

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal static extern bool DoesObjectWithInstanceIDExist(int instanceID);

        public int GetInstanceID()
        {
            return instanceID;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern Object Internal_CloneSingle(Object data);

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern Object Internal_CloneSingleWithParent(Object data, Transform parent, bool worldPositionStays);

        private static bool IsNativeObjectAlive(Object o)
        {
            return o.GetCachedPtr() != IntPtr.Zero || (!(o is MonoBehaviour) && !(o is ScriptableObject) && DoesObjectWithInstanceIDExist(o.GetInstanceID()));
        }

        public IntPtr GetCachedPtr()
        {
            return cachedPtr;
        }

        public static Object Instantiate(Object original, Transform parent, bool worldPositionStays)
        {
            if (parent == null)
            {
                return Internal_CloneSingle(original);
            }
            CheckNullArgument(original, "The Object you want to instantiate is null.");
            return Internal_CloneSingleWithParent(original, parent, worldPositionStays);
        }

        public static T Instantiate<T>(T original) where T : Object
        {
            CheckNullArgument(original, "The Object you want to instantiate is null.");
            return (T)Internal_CloneSingle(original);
        }

        private static void CheckNullArgument(object arg, string message)
        {
            if (arg == null)
            {
                throw new ArgumentException(message);
            }
        }

        public static void Destroy(Object obj)
        {
            float t = 0.0f;
            Destroy(obj, t);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        public static extern void Destroy(Object obj, [DefaultValue("0.0F")] float t);
    }
}
