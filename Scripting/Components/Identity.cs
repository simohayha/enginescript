﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Scripting.Components
{
    public class Identity : Component
    {
        public extern string name
        {
            [MethodImpl(MethodImplOptions.InternalCall)]
            get;
            [MethodImpl(MethodImplOptions.InternalCall)]
            set;
        }

        public override string ToString()
        {
            return "IDENTITY";
        }

        //public string name
        //{
        //    get
        //    {
        //        string result;
        //        INTERNAL_get_name(out result);
        //        return result;
        //    }
        //    set
        //    {
        //        INTERNAL_set_name(ref value);
        //    }
        //}

        //[MethodImpl(MethodImplOptions.InternalCall)]
        //private extern void INTERNAL_get_name(out string value);

        //[MethodImpl(MethodImplOptions.InternalCall)]
        //private extern void INTERNAL_set_name(ref string value);
    }
}
