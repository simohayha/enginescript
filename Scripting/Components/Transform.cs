﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Scripting.Components
{
    public class Transform : Component
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_position(out Vector3 value);

        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_position(ref Vector3 value);

        public Vector3 Position
        {
            get
            {
                Vector3 result;
                INTERNAL_get_position(out result);
                return result;
            }
            set
            {
                INTERNAL_set_position(ref value);
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_rotation(out Quaternion value);

        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_rotation(ref Quaternion value);

        public Quaternion Rotation
        {
            get
            {
                Quaternion result;
                INTERNAL_get_rotation(out result);
                return result;
            }
            set
            {
                INTERNAL_set_rotation(ref value);
            }
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_get_localScale(out Vector3 value);

        [MethodImpl(MethodImplOptions.InternalCall)]
        private extern void INTERNAL_set_localScale(ref Vector3 value);

        public Vector3 LocalScale
        {
            get
            {
                Vector3 result;
                INTERNAL_get_localScale(out result);
                return result;
            }
            set
            {
                INTERNAL_set_localScale(ref value);
            }
        }

        public void Rotate(Vector3 eulerAngle)
        {
            Space relativeTo = Space.Self;
            Rotate(eulerAngle, relativeTo);
        }

        public void Rotate(Vector3 axis, float angle)
        {
            Space relativeTo = Space.Self;
            Rotate(axis, angle, relativeTo);
        }

        public void Rotate(Vector3 axis, float angle, [DefaultValue("Space.Self")] Space relativeTo)
        {
            if (relativeTo == Space.Self)
            {
                RotateAroundInternal(TransformDirection(axis), angle * 0.0174532924f);
            }
            else
            {
                RotateAroundInternal(axis, angle * 0.0174532924f);
            }
        }

        public void Rotate(Vector3 eulerAngle, [DefaultValue("Space.Self")] Space relativeTo)
        {
            Quaternion rhs = Quaternion.Euler(eulerAngle.x, eulerAngle.y, eulerAngle.z);
            if (relativeTo == Space.Self)
            {
                Rotation *= rhs;
            }
            else
            {
                Rotation *= Quaternion.Inverse(Rotation) * rhs * Rotation;
            }
        }

        public Vector3 TransformDirection(Vector3 direction)
        {
            Vector3 result;
            INTERNAL_CALL_TransformDirection(this, ref direction, out result);
            return result;
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_TransformDirection(Transform self, ref Vector3 direction, out Vector3 result);

        internal void RotateAroundInternal(Vector3 axis, float angle)
        {
            INTERNAL_CALL_RotateAroundInternal(this, ref axis, angle);
        }

        [MethodImpl(MethodImplOptions.InternalCall)]
        private static extern void INTERNAL_CALL_RotateAroundInternal(Transform self, ref Vector3 axis, float angle);
    }
}
