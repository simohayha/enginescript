﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Scripting
{
    public class GameObject : Object
    {
        [MethodImpl(MethodImplOptions.InternalCall)]
        public extern Component GetComponent(Type type);

        [MethodImpl(MethodImplOptions.InternalCall)]
        internal extern void GetComponentFastPath(Type type, IntPtr oneFurtherThanResultValue);

        public unsafe T GetComponent<T>()
        {
            CastHelper<T> castHelper = default(CastHelper<T>);
            GetComponentFastPath(typeof(T), new IntPtr((void*)(&castHelper.onePointerFurtherThanT)));
            //return castHelper.t;
            return default(T);
        }
    }
}
