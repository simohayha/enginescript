﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripting.Internals
{
    [AttributeUsage(AttributeTargets.Parameter | AttributeTargets.GenericParameter)]
    public class DefaultValueAttribute : Attribute
    {
        private object DefaultValue;

        public object Value
        {
            get
            {
                return DefaultValue;
            }
        }

        public DefaultValueAttribute(string value)
        {
            DefaultValue = value;
        }

        public override bool Equals(object obj)
        {
            DefaultValueAttribute defaultValueAttribute = obj as DefaultValueAttribute;
            if (defaultValueAttribute == null)
            {
                return false;
            }
            if (DefaultValue == null)
            {
                return defaultValueAttribute.Value == null;
            }
            return DefaultValue.Equals(defaultValueAttribute.Value);
        }

        public override int GetHashCode()
        {
            if (DefaultValue == null)
            {
                return base.GetHashCode();
            }
            return DefaultValue.GetHashCode();
        }
    }
}
