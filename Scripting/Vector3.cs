﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripting
{
    [UsedByNativeCode]
    public struct Vector3
    {
        public Vector3(float x, float y)
        {
            this.x = x;
            this.y = y;
            z = 0.0f;
        }

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public float x;
        public float y;
        public float z;

        public override string ToString()
        {
            return string.Format("({0:F1}, {1:F1}, {2:F1})", new object[]
            {
                x,
                y,
                z
            });
        }

        public static Vector3 operator *(Vector3 a, float d)
        {
            return new Vector3(a.x * d, a.y * d, a.z * d);
        }

        public static Vector3 up
        {
            get
            {
                return new Vector3(0.0f, 1.0f, 0.0f);
            }
        }

        public static Vector3 back
        {
            get
            {
                return new Vector3(0.0f, 0.0f, -1.0f);
            }
        }

        public static Vector3 down
        {
            get
            {
                return new Vector3(0.0f, -1.0f, 0.0f);
            }
        }

        public static Vector3 forward
        {
            get
            {
                return new Vector3(0.0f, 0.0f, 1.0f);
            }
        }

        public static Vector3 left
        {
            get
            {
                return new Vector3(-1.0f, 0.0f, 0.0f);
            }
        }

        public static Vector3 right
        {
            get
            {
                return new Vector3(1.0f, 0.0f, 0.0f);
            }
        }

        public static Vector3 one
        {
            get
            {
                return new Vector3(1.0f, 1.0f, 1.0f);
            }
        }

        public static Vector3 zero
        {
            get
            {
                return new Vector3(0.0f, 0.0f, 0.0f);
            }
        }

        public static float Magnitude(Vector3 a)
        {
            return Mathf.Sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
        }

        public void Normalize()
        {
            float num = Vector3.Magnitude(this);
            if (num > float.Epsilon)
            {
                //this /= num;
            }
            else
            {
                this = Vector3.zero;
            }
        }
    }
}
