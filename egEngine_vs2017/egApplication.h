#pragma once

#include "egPrequisites.h"
#include "egCoreApplication.h"

namespace eg
{
	class EG_EXPORT Application : public CoreApplication
	{
	public:
		Application();
		virtual ~Application();
	};
}