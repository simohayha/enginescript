﻿using Scripting;
using Scripting.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assembly_CSharp
{
    class PrintPosition : MonoBehaviour
    {
        //private float speed = 10.0f;
        //private Vector3 rotation;
        private Transform transform;
        private float step = 1.0f;

        static float NextFloat(Random random)
        {
            double mantissa = (random.NextDouble() * 2.0) - 1.0;
            double exponent = Math.Pow(2.0, random.Next(-126, 128));
            return (float)(mantissa * exponent);
        }

        void Start()
        {
            transform = GetComponent<Transform>();
            //Debug.Log("Object ID: " + GetInstanceID());
            //
            //
            //Debug.Log("Transf ID: " + transform.GetInstanceID());
            //
            //var position = transform.Position;
            //var rotation = transform.Rotation;
            //var localScale = transform.LocalScale;
            //
            //Debug.Log(transform.Position);
            //Debug.Log(transform.Rotation);
            //Debug.Log(transform.LocalScale);

            //Random r = new Random(DateTime.Now.Millisecond);
            //int range = 100;
            //speed = (float)(r.NextDouble() * range);
            //
            //rotation = new Vector3
            //    (
            //    NextFloat(r),
            //    NextFloat(r),
            //    NextFloat(r)
            //    );
        }

        void Update()
        {
            //if (Input.GetMouseButtonDown(0))
            //{
            //    Debug.Log(Input.mousePosition);
            //}

            //if (Input.GetKeyDown(KeyCode.W))
            //{
            //    Debug.Log("W pressed");
            //}

            //Debug.Log("Update");
            //transform.Rotate(rotation, speed * Time.deltaTime);

            //Vector3 position = transform.Position;
            //position.x += step * Time.deltaTime;
            //if (position.x > 1.0f)
            //{
            //    step = -1.0f;
            //}
            //else if (position.x < -1.0f)
            //{
            //    step = 1.0f;
            //}
            //position.z += 1.0f * Time.deltaTime;
            //Debug.Log(position);
            //transform.Position = position;
            transform.Rotate(Vector3.right * 360.0f * Time.deltaTime);
        }
    }
}
