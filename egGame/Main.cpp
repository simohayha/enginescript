#include "egApplication.h"

void runApplication();

int main(int ac, char* av[])
{
	runApplication();
}

using namespace eg;

void runApplication()
{
	Application::startUp();

	Application::instance().runMainLoop();
	Application::shutDown();
}