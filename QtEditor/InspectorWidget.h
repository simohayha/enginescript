#pragma once

#include <QListWidget>

class InspectorWidget : public QListWidget
{
	Q_OBJECT

public:
	InspectorWidget(QWidget* parent = nullptr);
	~InspectorWidget();

public slots:
	void	onGameObjectSelected(const QModelIndex& index);
	void	onAssetSelected(const QModelIndex& index);
	void	onLockedChanged(int state);

private:
	bool	m_locked;
};