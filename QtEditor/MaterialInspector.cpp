#include <QLabel>
#include <QBoxLayout>
#include <QDebug>

#include "ObjectField.h"
#include "Material.h"
#include "Shader.h"
#include "MaterialInspector.h"

MaterialInspector::MaterialInspector(core::Material* material, QWidget* parent) :
	QWidget(parent),
	m_material(material)
{
	QVBoxLayout*	box = new QVBoxLayout(this);

	QLabel*	title = new QLabel(this);
	title->setText("Material");
	title->setStyleSheet("font-weight: bold;");
	box->addWidget(title);

	QLabel* materialLabel = new QLabel(this);
	materialLabel->setText("Shader");
	box->addWidget(materialLabel);
	ObjectField*	field = new ObjectField(m_material->m_shader, false, this);
	box->addWidget(field);

	connect(field, SIGNAL(changed(core::Object*)), this, SLOT(onShaderChanged(core::Object*)));

	setLayout(box);
}

MaterialInspector::~MaterialInspector()
{
}

void MaterialInspector::onShaderChanged(core::Object* newShader)
{
	if (!newShader)
	{
		return;
	}

	auto	shader = newShader->as<core::Shader>();
	if (shader)
	{
		m_material->m_shader = shader;
	}
}