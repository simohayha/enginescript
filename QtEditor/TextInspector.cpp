#include <QTextEdit>
#include <QBoxLayout>
#include <QLabel>

#include "TextInspector.h"

TextInspector::TextInspector(const std::string& text, QWidget* parent) :
	QWidget(parent)
{
	setupInspector(text, core::E_TEXTASSET);
}

TextInspector::TextInspector(const std::string& text, core::AssetType type, QWidget* parent) :
	QWidget(parent)
{
	setupInspector(text, type);
}

TextInspector::~TextInspector()
{
}

void TextInspector::setupInspector(const std::string& text, core::AssetType type)
{
	QVBoxLayout*	box = new QVBoxLayout(this);

	QLabel*	title = new QLabel(this);
	title->setText(core::AssetTypeName[type]);
	title->setStyleSheet("font-weight: bold;");
	box->addWidget(title);

	QTextEdit*	editor = new QTextEdit(this);
	editor->setText(text.c_str());
	editor->setReadOnly(true);
	editor->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	box->addWidget(editor);

	setLayout(box);
}

QSize TextInspector::sizeHint() const
{
	return QSize(10, 10);
}

QSize TextInspector::minimumSizeHint() const
{
	QWidget*	parent = parentWidget();
	return QSize(100, parent->geometry().height());
}