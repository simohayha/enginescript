#pragma once

#include <QWidget>

namespace core
{
	class CameraComponent;
}

class QDoubleSpinBox;

class CameraInspector : public QWidget
{
	Q_OBJECT
public:
	CameraInspector(core::CameraComponent* camera, QWidget* parent = nullptr);
	~CameraInspector();

protected slots:
	void	valueChanged(double d);

protected:
	virtual void	paintEvent(QPaintEvent*	evt);

private:
	int		m_objectId;

	QDoubleSpinBox*	m_aspect;
	QDoubleSpinBox*	m_near;
	QDoubleSpinBox*	m_far;
	QDoubleSpinBox*	m_fov;

};
