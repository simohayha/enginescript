#include <qstringlistmodel>
#include <QMimeData>
#include <QDataStream>
#include <QDebug>

#include "Asset.h"

#include "EngineInterface.h"
#include "IdentityComponent.h"
#include "TransformComponent.h"

#include "HierarchyTreeItem.h"
#include "HierarchyTreeModel.h"

HierarchyTreeModel::HierarchyTreeModel(const QStringList& headers, QObject* parent) :
	QAbstractItemModel(parent),
	m_rootItem(nullptr)
{
	QVector<QVariant>	rootData;
	for (QString header : headers)
	{
		rootData << header;
	}
	m_rootItem = new HierarchyTreeItem(rootData);

	connect(this, SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&, const QVector<int>&)), this, SLOT(dataChanged(const QModelIndex&, const QModelIndex&, const QVector<int>&)));
}

HierarchyTreeModel::~HierarchyTreeModel()
{
}

void HierarchyTreeModel::setData(std::vector<int>& data)
{
	if (m_rootItem)
	{
		delete m_rootItem;
		m_rootItem = nullptr;
	}

	QVector<QVariant> rootData;
	rootData << tr("GameObject") << tr("ID");
	m_rootItem = new HierarchyTreeItem(rootData);

	auto	engineInterface = EngineInterface::instance();

	for (auto gameObject : data)
	{
		auto newItem = addObject(gameObject);

		setDataRecursive(gameObject, newItem);
	}
}

void HierarchyTreeModel::setData(core::Asset* assetFolder)
{
	if (m_rootItem)
	{
		delete m_rootItem;
		m_rootItem = nullptr;
	}

	QVector<QVariant>	rootData;
	rootData << tr("Asset") << tr("ID");
	m_rootItem = new HierarchyTreeItem(rootData);

	for (auto child : assetFolder->getChilds())
	{
		auto	newItem = addAsset(child);

		setDataRecursive(child, newItem);
	}
}

HierarchyTreeItem* HierarchyTreeModel::addObject(int gameObject, HierarchyTreeItem* parent)
{
	auto				engineInterface = EngineInterface::instance();
	QVector<QVariant>	itemData;
	auto				identity = engineInterface->getComponent<core::IdentityComponent>(gameObject);
	if (!identity)
	{
		itemData << "(no name)" << gameObject;
	}
	else
	{
		itemData << identity->getName().c_str() << gameObject;
	}

	if (!parent)
	{
		parent = m_rootItem;
	}
	HierarchyTreeItem*	item = new HierarchyTreeItem(itemData, parent);
	parent->appendChild(item);
	return item;
}

HierarchyTreeItem* HierarchyTreeModel::addAsset(core::Asset* asset, HierarchyTreeItem* parent)
{
	QVector<QVariant>	itemData;
	itemData << asset->getFilename().c_str() << static_cast<int>(asset->getId());
	if (!parent)
	{
		parent = m_rootItem;
	}
	HierarchyTreeItem*	item = new HierarchyTreeItem(itemData, parent);
	parent->appendChild(item);
	return item;
}

QVariant HierarchyTreeModel::data(const QModelIndex& index, int role) const
{
	if (!index.isValid())
	{
		return QVariant();
	}

	if (role != Qt::DisplayRole && role != Qt::EditRole)
	{
		return QVariant();
	}

	HierarchyTreeItem*	item = getItem(index);

	return item->getData(index.column());
}

QVariant HierarchyTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
	{
		return m_rootItem->getData(section);
	}

	return QVariant();
}

QModelIndex HierarchyTreeModel::index(int row, int column, const QModelIndex& parent) const
{
	if (parent.isValid() && parent.column() != 0)
	{
		return QModelIndex();
	}

	HierarchyTreeItem*	parentItem = getItem(parent);

	HierarchyTreeItem*	childItem = parentItem->getChild(row);
	if (childItem)
	{
		return createIndex(row, column, childItem);
	}
	else
	{
		return QModelIndex();
	}
}

QModelIndex HierarchyTreeModel::parent(const QModelIndex& index) const
{
	if (!index.isValid())
	{
		return QModelIndex();
	}

	HierarchyTreeItem*	childItem = getItem(index);
	HierarchyTreeItem*	parentItem = childItem->getParent();

	if (!parentItem)
	{
		return createIndex(m_rootItem->getChildNumber(), 0, m_rootItem);
	}

	if (parentItem == m_rootItem)
	{
		return QModelIndex();
	}

	return createIndex(parentItem->getChildNumber(), 0, parentItem);
}

int HierarchyTreeModel::rowCount(const QModelIndex & parent) const
{
	HierarchyTreeItem*	parentItem = getItem(parent);

	return parentItem->getChildCount();
}

int HierarchyTreeModel::columnCount(const QModelIndex& parent) const
{
	return m_rootItem->getColumnCount();
}

Qt::ItemFlags HierarchyTreeModel::flags(const QModelIndex& index) const
{
	if (!index.isValid())
	{
		return 0;
	}

	if (index.column() > 0)
	{
		return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | QAbstractItemModel::flags(index);
	}
	else
	{
		return Qt::ItemIsEditable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | QAbstractItemModel::flags(index);
	}
}

bool HierarchyTreeModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
	if (role != Qt::EditRole)
	{
		return false;
	}

	HierarchyTreeItem*	item = getItem(index);
	bool				result = item->setData(index.column(), value);

	if (result)
	{
		emit dataChanged(index, index);
	}

	return result;
}

bool HierarchyTreeModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant & value, int role)
{
	if (role != Qt::EditRole || orientation != Qt::Horizontal)
	{
		return false;
	}

	bool	result = m_rootItem->setData(section, value);

	if (result)
	{
		emit headerDataChanged(orientation, section, section);
	}

	return result;
}

bool HierarchyTreeModel::insertColumns(int position, int columns, const QModelIndex& parent)
{
	bool	success;

	beginInsertColumns(parent, position, position + columns - 1);
	success = m_rootItem->insertColumns(position, columns);
	endInsertColumns();

	return success;
}

bool HierarchyTreeModel::removeColumns(int position, int columns, const QModelIndex& parent)
{
	bool	success;

	beginRemoveColumns(parent, position, position + columns - 1);
	success = m_rootItem->removeColumns(position, columns);
	endRemoveColumns();

	if (m_rootItem->getColumnCount() == 0)
	{
		removeRows(0, rowCount());
	}

	return success;
}

bool HierarchyTreeModel::insertRows(int position, int rows, const QModelIndex& parent)
{
	HierarchyTreeItem*	parentItem = getItem(parent);
	bool				success;

	beginInsertRows(parent, position, position + rows - 1);
	success = parentItem->insertChild(position, rows, m_rootItem->getColumnCount());
	endInsertRows();

	return success;
}

bool HierarchyTreeModel::removeRows(int position, int rows, const QModelIndex& parent)
{
	HierarchyTreeItem*	parentItem = getItem(parent);
	bool				success = true;

	beginRemoveRows(parent, position, position + rows - 1);
	success = parentItem->removeChild(position, rows);
	endRemoveRows();

	return success;
}

bool HierarchyTreeModel::moveRows(const QModelIndex& sourceParent, int sourceRow, int count, const QModelIndex& destinationParent, int destinationChild)
{
	HierarchyTreeItem*	sourceParentNode = getItem(sourceParent);
	HierarchyTreeItem*	destinationParentNode = getItem(destinationParent);
	HierarchyTreeItem*	childNode = sourceParentNode->getChild(sourceRow);

	// make sure the child isn't one of the parent nodes
	if (childNode == sourceParentNode || childNode == destinationParentNode)
	{
		return false;
	}

	if (sourceParentNode == destinationParentNode)
	{
		// if source and destination parents are the same, move elements locally
		beginMoveRows(sourceParent, sourceRow, sourceRow + count - 1, destinationParent, destinationChild);
		sourceParentNode->moveChild(sourceRow, count, destinationChild);
		endMoveRows();
		return true;
	}
	else
	{
		// otherwise, move the node under the parent
		beginMoveRows(sourceParent, sourceRow, sourceRow + count - 1, destinationParent, destinationChild);
		childNode->setParent(destinationParentNode, destinationChild);
		endMoveRows();
		return true;
	}

	return false;
}

Qt::DropActions HierarchyTreeModel::supportedDropActions() const
{
	return Qt::CopyAction | Qt::MoveAction;
}

QStringList HierarchyTreeModel::mimeTypes() const
{
	QStringList	types;
	types << "application/engineobject";
	return types;
}

QMimeData* HierarchyTreeModel::mimeData(const QModelIndexList& indexes) const
{
	if (indexes.size() != 2 || !indexes[0].isValid())
	{
		return nullptr;
	}

	auto	item = getItem(indexes[0]);
	if (!item)
	{
		return nullptr;
	}
	EngineInterface*	engineInterface = EngineInterface::instance();
	auto	type = engineInterface->getAssetType(item->getData(1).toInt());

	quintptr	nodeAddress = (quintptr)indexes[0].internalPointer();
	QByteArray	encodedData(QString::number(nodeAddress).toUtf8());

	QMimeData*	mimeData = new QMimeData();
	mimeData->setData("application/engineobject", encodedData);
	return mimeData;
}

bool HierarchyTreeModel::canDropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent) const
{
	Q_UNUSED(action);
	Q_UNUSED(row);
	Q_UNUSED(parent);

	if (!data->hasFormat("application/engineobject"))
	{
		return false;
	}

	if (column > 0)
	{
		return false;
	}

	return true;
}

// Found solution here
// https://www.gamedev.net/topic/670509-qt-5-moving-rows-for-scene-tree-qabstractitemmodel/
bool HierarchyTreeModel::dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent)
{
	if (!data || action == Qt::IgnoreAction)
	{
		return false;
	}

	QByteArray	encodedData = data->data("application/engineobject");
	HierarchyTreeItem*	item = (HierarchyTreeItem*)encodedData.toULongLong();
	if (!item)
	{
		return false;
	}

	QModelIndex	destinationParentIndex;
	HierarchyTreeItem*	parentNode = getItem(parent);
	if (parentNode)
	{
		destinationParentIndex = parent;
	}
	else
	{
		parentNode = getRoot();
		destinationParentIndex = createIndex(0, 1, getRoot());
	}

	if (row == -1)
	{
		row = parentNode->getChildNumber();
	}

	HierarchyTreeItem*	sourceParentNode = item->getParent();
	QModelIndex			sourceParent = createIndex(sourceParentNode->getChildNumber(), 0, sourceParentNode);
	moveRow(sourceParent, item->getChildNumber(), destinationParentIndex, row);

	// Change parent in engine
	EngineInterface*	engineInterface = EngineInterface::instance();
	
	int		draggedId = item->getData(1).toInt();
	auto	draggedType = engineInterface->getAssetType(draggedId);
	int		droppedId = parentNode->getData(1).toInt();
	auto	droppedType = engineInterface->getAssetType(droppedId);

	if (engineInterface->isTypeOf(draggedId, core::E_GAMEOBjECT) &&
		engineInterface->isTypeOf(droppedId, core::E_GAMEOBjECT))
	{
		core::TransformComponent*	draggedTransform = engineInterface->getComponent<core::TransformComponent>(draggedId);
		draggedTransform->setParent(droppedId);
	}
	else if (engineInterface->isTypeOf(draggedId, core::E_GAMEOBjECT) &&
			 engineInterface->isTypeOf(droppedId, core::E_DIRECTORY))
	{
		// Create a prefab
		draggedId = draggedId;
	}
	else if (engineInterface->isTypeOf(draggedId, core::E_DIRECTORY) &&
			 engineInterface->isTypeOf(droppedId, core::E_DIRECTORY))
	{
		// Perform a directory move
		core::Asset*	sourceAsset = dynamic_cast<core::Asset*>(engineInterface->getObject(draggedId));
		core::Asset*	destinationAsset = dynamic_cast<core::Asset*>(engineInterface->getObject(draggedId));
	}

	return true;
}

HierarchyTreeItem* HierarchyTreeModel::getRoot()
{
	return m_rootItem;
}

QModelIndex HierarchyTreeModel::getRootIndex()
{
	return createIndex(0, 0, m_rootItem);
}

QModelIndex HierarchyTreeModel::publicCreateIndex(int row, int col, void* data)
{
	return createIndex(row, col, data);
}

HierarchyTreeItem* HierarchyTreeModel::getItem(const QModelIndex& index) const
{
	if (index.isValid())
	{
		HierarchyTreeItem*	item = static_cast<HierarchyTreeItem*>(index.internalPointer());
		if (item)
		{
			return item;
		}
	}
	return m_rootItem;
}

void HierarchyTreeModel::setDataRecursive(int gameObject, HierarchyTreeItem* parent)
{
	auto	engineInterface = EngineInterface::instance();

	auto	transform = engineInterface->getComponent<core::TransformComponent>(gameObject);
	auto	childs = transform->getChilds();

	for (auto child : childs)
	{
		auto	newItem = addObject(child, parent);

		setDataRecursive(child, newItem);
	}
}

void HierarchyTreeModel::setDataRecursive(core::Asset* asset, HierarchyTreeItem* parent)
{
	for (auto child : asset->getChilds())
	{
		auto	newItem = addAsset(child, parent);

		setDataRecursive(child, newItem);
	}
}

void HierarchyTreeModel::dataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles)
{
	if (topLeft == bottomRight)
	{
		auto	engineInterface = EngineInterface::instance();

		auto	item = getItem(topLeft);
		if (item)
		{
			auto	obj = engineInterface->getObject(item->getData(1).toInt());
			if (obj)
			{
				obj->setName(item->getData(0).toString().toStdString());
			}
		}
	}
}
