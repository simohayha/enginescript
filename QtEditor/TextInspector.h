#pragma once

#include <QWidget>

#include <string>

#include "AssetType.h"

class TextInspector : public QWidget
{
	Q_OBJECT
public:
	TextInspector(const std::string& text, QWidget* parent = nullptr);
	TextInspector(const std::string& text, core::AssetType type, QWidget* parent = nullptr);
	~TextInspector();

	virtual QSize	sizeHint() const override;
	virtual QSize	minimumSizeHint() const override;

private:
	void	setupInspector(const std::string& text, core::AssetType type);
};