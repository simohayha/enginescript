#include <QDropEvent>
#include <QAction>
#include <QPoint>
#include <QMenu>
#include <QMimeData>
#include <QDrag>

#include "EngineInterface.h"
#include "HierarchyTreeModel.h"
#include "HierarchyTreeItem.h"
#include "HierarchyTree.h"

HierarchyTree::HierarchyTree(QWidget* parent)
	: QTreeView(parent),
	m_model(nullptr)
{
	QStringList headers;
	headers << tr("Name") << tr("Id");
	m_model = new HierarchyTreeModel(headers);

	setModel(m_model);

	EngineInterface*	engineInterface = EngineInterface::instance();
	std::vector<int>	gameObjects;
	engineInterface->getRootGameObjects(gameObjects);
	m_model->setData(gameObjects);

	setDragEnabled(true);
	setAcceptDrops(true);
	setDropIndicatorShown(true);

	setContextMenuPolicy(Qt::CustomContextMenu);

	connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(showContextMenu(const QPoint&)));
}

HierarchyTree::~HierarchyTree()
{
}

void HierarchyTree::showContextMenu(const QPoint& pos)
{
	QMenu	contextMenu(tr("Context menu"), this);

	QAction	action1("Add GameObject", this);
	connect(&action1, SIGNAL(triggered()), this, SLOT(addGameObject()));
	contextMenu.addAction(&action1);

	QAction	action2("Add Child", this);
	connect(&action2, SIGNAL(triggered()), this, SLOT(addGameObjectChild()));
	contextMenu.addAction(&action2);

	QPoint	p;
	p.setX(pos.x());
	p.setY(pos.y() + 25);
	contextMenu.exec(mapToGlobal(p));
}

// InsertRow
void HierarchyTree::addGameObject()
{
	QModelIndex			index = selectionModel()->currentIndex();
	QAbstractItemModel*	m = model();

	if (!m->insertRow(index.row() + 1, index.parent()))
	{
		return;
	}

	EngineInterface*	engineInterface = EngineInterface::instance();
	int	newObject = engineInterface->addGameObject(0);

	for (int column = 0; column < m->columnCount(index.parent()); ++column)
	{
		QModelIndex	child = m->index(index.row() + 1, column, index.parent());
		m->setData(child, QVariant("[No data]"), Qt::EditRole);
	}
}

// InsertChild
void HierarchyTree::addGameObjectChild()
{
	QModelIndex			index = selectionModel()->currentIndex();
	QAbstractItemModel*	m = model();

	if (m->columnCount(index) == 0)
	{
		if (!m->insertColumn(0, index))
		{
			return;
		}
	}

	if (!m->insertRow(0, index))
	{
		return;
	}

	EngineInterface*	engineInterface = EngineInterface::instance();
	int	newObject = engineInterface->addGameObject(index.data(1).toInt());

	for (int column = 0; column < m->columnCount(index); ++column) 
	{
		QModelIndex child = m->index(0, column, index);
		m->setData(child, QVariant("[No data]"), Qt::EditRole);
		if (!m->headerData(column, Qt::Horizontal).isValid())
		{
			m->setHeaderData(column, Qt::Horizontal, QVariant("[No header]"), Qt::EditRole);
		}
	}

	selectionModel()->setCurrentIndex(m->index(0, 0, index), QItemSelectionModel::ClearAndSelect);
}