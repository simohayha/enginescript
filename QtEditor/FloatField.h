#pragma once

#include <QAbstractSpinBox>

class FloatField : public QAbstractSpinBox
{
	Q_OBJECT
public:
	FloatField(QWidget* parent = nullptr);
	~FloatField();

	void	setRange(float min, float max);
	void	setSingleStep(float step);

	void	setValue(float f);

	bool	isFocussed() const;

	virtual void	stepBy(int steps) override;

signals:
	void	valueChanged(float f);

private slots:
	void	lineEditFocus(bool hasFocus);
	void	textChanged(const QString& text);
	void	editingFinished();

protected:
	virtual StepEnabled	stepEnabled() const override;

private:
	bool	m_hasFocus;

	float	m_min;
	float	m_max;
	float	m_singleStep;
};
