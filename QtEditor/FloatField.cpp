#include <QLineEdit>
#include <QTimer>

#include "LineEdit.h"
#include "FloatField.h"

FloatField::FloatField(QWidget* parent) :
	QAbstractSpinBox(parent),
	m_hasFocus(false),
	m_min(0.0f),
	m_max(100.0f),
	m_singleStep(0.1f)
{
	setLineEdit(new LineEdit(this));

	setWrapping(false);
	setAccelerated(true);

	connect(lineEdit(), SIGNAL(focussed(bool)), this, SLOT(lineEditFocus(bool)));
	connect(lineEdit(), SIGNAL(textChanged(const QString&)), this, SLOT(textChanged(const QString&)));
	connect(lineEdit(), SIGNAL(editingFinished()), this, SLOT(editingFinished()));
}

FloatField::~FloatField()
{
}

void FloatField::setRange(float min, float max)
{
	m_min = min;
	m_max = max;
}

void FloatField::setSingleStep(float step)
{
	m_singleStep = step;
}

void FloatField::setValue(float f)
{
	if (!m_hasFocus)
	{
		lineEdit()->setText(QString::number(f));
	}
}

bool FloatField::isFocussed() const
{
	return m_hasFocus;
}

void FloatField::stepBy(int steps)
{
	float	value = lineEdit()->text().toFloat();
	value += (m_singleStep * steps);
	lineEdit()->setText(QString::number(value));
}

void FloatField::lineEditFocus(bool hasFocus)
{
	if (hasFocus && !m_hasFocus)
	{
		QTimer::singleShot(0, [this]()
		{
			lineEdit()->selectAll();
		});
	}
	m_hasFocus = hasFocus;
}

void FloatField::textChanged(const QString& text)
{
	float	value = text.toFloat();
	emit valueChanged(value);
}

void FloatField::editingFinished()
{
	float	value = lineEdit()->text().toFloat();
	lineEdit()->blockSignals(true);
	lineEdit()->setText(QString::number(value));
	lineEdit()->blockSignals(false);
}

QAbstractSpinBox::StepEnabled FloatField::stepEnabled() const
{
	QAbstractSpinBox::StepEnabled	flags;
	flags |= StepDownEnabled;
	flags |= StepUpEnabled;
	return flags;
}
