#pragma once

#include "AssetType.h"

#include <QTreeView>

class HierarchyTreeModel;

class ProjectTree : public QTreeView
{
	Q_OBJECT
public:
	ProjectTree(QWidget* parent = nullptr);
	~ProjectTree();

public slots:
	void	showContextMenu(const QPoint&);

	void	createFolder();
	void	createCSharpScript();
	void	createMaterial();

	void	createAsset(core::AssetType type);

	void	onDoubleClick(const QModelIndex& index);

private:
	HierarchyTreeModel*	m_model;
};