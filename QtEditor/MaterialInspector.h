#pragma once

#include <QWidget>

namespace core
{
	class Material;
	class Object;
}

class MaterialInspector : public QWidget
{
	Q_OBJECT
public:
	MaterialInspector(core::Material* material, QWidget* parent = nullptr);
	~MaterialInspector();

private slots:
	void	onShaderChanged(core::Object* newShader);

private:
	core::Material*	m_material;
};