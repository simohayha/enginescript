#pragma once

#include <QWidget>

namespace core
{
	class TransformComponent;
}

class QDoubleSpinBox;
class FloatField;

class TransformInspector : public QWidget
{
	Q_OBJECT
public:
	TransformInspector(core::TransformComponent* transform, QWidget* parent = nullptr);
	~TransformInspector();

	virtual QSize	sizeHint() const override;
	virtual QSize	minimumSizeHint() const override;

protected slots:
	void	valueChanged(float f);

protected:
	virtual void	paintEvent(QPaintEvent*	evt);

private:
	int			m_objectId;

	FloatField*	m_positionX;
	FloatField*	m_positionY;
	FloatField*	m_positionZ;
	FloatField*	m_rotationX;
	FloatField*	m_rotationY;
	FloatField*	m_rotationZ;
	FloatField*	m_scaleX;
	FloatField*	m_scaleY;
	FloatField*	m_scaleZ;
};