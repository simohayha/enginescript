#pragma once

#include <qtime>
#include <qtimer>
#include <qwidget>
#include <qframe>

class D3DScene : public QFrame
{
	Q_OBJECT

public:
	explicit D3DScene(QWidget* parent = nullptr);

	virtual QPaintEngine*	paintEngine() const { return nullptr; }

protected:
	virtual void resizeEvent(QResizeEvent* evt);
	virtual void paintEvent(QPaintEvent* evt);

protected slots:
	void	update();

private:
	int		m_frameCount;
	QTime	m_time;

	QTimer	m_updateTimer;
	QTimer	m_repaintTimer;

};