#pragma once

#include <QWidget>

#include "Object.h"

class QLabel;
class QMimeData;

class ObjectField : public QWidget
{
	Q_OBJECT
public:
	ObjectField(core::Object* defaultValue, bool erasable = true, QWidget* parent = nullptr);
	~ObjectField();

protected:
	virtual void	dragEnterEvent(QDragEnterEvent* evt) override;
	virtual void	dragMoveEvent(QDragMoveEvent* evt) override;
	virtual void	dragLeaveEvent(QDragLeaveEvent* evt) override;
	virtual void	dropEvent(QDropEvent* evt) override;

signals:
	void	changed(core::Object* object);

private slots:
	void	onDelPressed();

private:
	QLabel*	m_name;

	bool	m_erasable;
};