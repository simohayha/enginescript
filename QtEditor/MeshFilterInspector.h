#pragma once

#include <QWidget>

namespace core
{
	class MeshFilterComponent;
}

class MeshFilterInspector : public QWidget
{
	Q_OBJECT
public:
	MeshFilterInspector(core::MeshFilterComponent* meshFilter, QWidget* parent = nullptr);
	~MeshFilterInspector();

private:
	int	m_objectId;
};
