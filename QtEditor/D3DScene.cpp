#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <qlayout>
#include <qgridlayout>

#include <sstream>

#include "EngineInterface.h"
#include "D3DScene.h"

D3DScene::D3DScene(QWidget* parent) :
	QFrame(parent),
	m_frameCount(0),
	m_updateTimer(this),
	m_repaintTimer(this)
{
	setUpdatesEnabled(true);

	setAttribute(Qt::WA_PaintOnScreen, true);
	setAttribute(Qt::WA_NativeWindow, true);

	auto	engineInterface = EngineInterface::instance();
	if (!engineInterface->initialize((HWND)winId(), 2, 2))
	{
		throw std::exception("Engine failed to initialize");
	}

	resize(800, 600);

	engineInterface->pause();

	connect(&m_updateTimer, SIGNAL(timeout()), this, SLOT(update()));
	m_updateTimer.start();

	connect(&m_repaintTimer, SIGNAL(timeout()), this, SLOT(repaint()));
	m_repaintTimer.start();
}

void D3DScene::resizeEvent(QResizeEvent* evt)
{
	EngineInterface::instance()->resize(width(), height());
}

void D3DScene::paintEvent(QPaintEvent* evt)
{
	EngineInterface::instance()->render();
}

void D3DScene::update()
{
	EngineInterface::instance()->update();
}
