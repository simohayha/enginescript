#include "HierarchyTreeItem.h"

HierarchyTreeItem::HierarchyTreeItem(const QVector<QVariant>& data, HierarchyTreeItem* parentItem)
{
	m_parentItem = parentItem;
	m_itemData = data;
}

HierarchyTreeItem::~HierarchyTreeItem()
{
}

void HierarchyTreeItem::appendChild(HierarchyTreeItem* item)
{
	m_childItems.append(item);
}

bool HierarchyTreeItem::removeChild(int position, int count)
{
	if (position < 0 || position + count > m_childItems.size())
	{
		return false;
	}

	for (int row = 0; row < count; ++row)
	{
		m_childItems.takeAt(position);
	}

	return true;
}

bool HierarchyTreeItem::deleteChild(int position, int count)
{
	if (position < 0 || position + count > m_childItems.size())
	{
		return false;
	}

	for (int row = 0; row < count; ++row)
	{
		delete m_childItems.takeAt(position);
	}

	return true;
}

void HierarchyTreeItem::moveChild(int row, int count, int destinationChild)
{
	HierarchyTreeItem*	child = getChild(row);
	removeChild(row, count);
	insertChild(destinationChild, count, getColumnCount());
}

void HierarchyTreeItem::setParent(HierarchyTreeItem* parent, int destinationChild)
{
	int row = getRow();
	m_parentItem->removeChild(row, 1);
	m_parentItem = parent;
	m_parentItem->appendChild(this);
}

bool HierarchyTreeItem::insertChild(int position, int count, int columns)
{
	if (position < 0 || position > m_childItems.size())
	{
		return false;
	}

	for (int row = 0; row < count; ++row)
	{
		QVector<QVariant>	data(columns);
		HierarchyTreeItem*	item = new HierarchyTreeItem(data, this);
		m_childItems.insert(position, item);
	}

	return true;
}

void HierarchyTreeItem::insertChild(HierarchyTreeItem * item, int position)
{
	m_childItems.insert(position, item);
}

bool HierarchyTreeItem::insertColumns(int position, int columns)
{
	if (position < 0 || position > m_itemData.size())
	{
		return false;
	}

	for (int column = 0; column < columns; ++column)
	{
		m_itemData.insert(position, QVariant());
	}

	for (HierarchyTreeItem* child : m_childItems)
	{
		child->insertColumns(position, columns);
	}

	return true;
}

bool HierarchyTreeItem::removeColumns(int position, int columns)
{
	if (position < 0 || position + columns > m_itemData.size())
	{
		return false;
	}

	for (int column = 0; column < columns; ++column)
	{
		m_itemData.remove(position);
	}

	for (HierarchyTreeItem* child : m_childItems)
	{
		child->removeColumns(position, columns);
	}

	return true;
}

HierarchyTreeItem* HierarchyTreeItem::getChild(int row)
{
	return m_childItems.value(row);
}

int HierarchyTreeItem::getChildCount() const
{
	return m_childItems.count();
}

int HierarchyTreeItem::getColumnCount() const
{
	return m_itemData.count();
}

QVariant HierarchyTreeItem::getData(int column) const
{
	return m_itemData.value(column);
}

int HierarchyTreeItem::getRow() const
{
	if (m_parentItem)
	{
		return m_parentItem->m_childItems.indexOf(const_cast<HierarchyTreeItem*>(this));
	}

	return 0;
}

HierarchyTreeItem* HierarchyTreeItem::getParentItem()
{
	return m_parentItem;
}

HierarchyTreeItem* HierarchyTreeItem::getParent()
{
	return m_parentItem;
}

int HierarchyTreeItem::getChildNumber() const
{
	if (m_parentItem)
	{
		return m_parentItem->m_childItems.indexOf(const_cast<HierarchyTreeItem*>(this));
	}

	return 0;
}

bool HierarchyTreeItem::setData(int column, const QVariant& value)
{
	if (column < 0 || column >= m_itemData.size())
	{
		return false;
	}

	m_itemData[column] = value;
	return true;
}
