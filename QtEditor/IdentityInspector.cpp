#include <QLineEdit>
#include <QGridLayout>
#include <QLabel>

#include "EngineInterface.h"
#include "IdentityComponent.h"
#include "IdentityInspector.h"

IdentityInspector::IdentityInspector(core::IdentityComponent* identity, QWidget* parent) :
	QWidget(parent),
	m_objectId(-1),
	m_nameInput(nullptr)
{
	m_objectId = identity->getOwner();

	QGridLayout*	grid = new QGridLayout(this);

	QLabel* identityLabel = new QLabel(this);
	identityLabel->setStyleSheet("font-weight: bold;");
	identityLabel->setText("Name");
	grid->addWidget(identityLabel, 0, 0);

	m_nameInput = new QLineEdit(this);
	m_nameInput->setText(tr(identity->getName().c_str()));
	grid->addWidget(m_nameInput, 1, 0);

	connect(m_nameInput, SIGNAL(textChanged(const QString&)), this, SLOT(textChanged(const QString&)));
}

IdentityInspector::~IdentityInspector()
{
}

void IdentityInspector::textChanged(const QString& value)
{
	auto	engineInterface = EngineInterface::instance();
	auto	identity = engineInterface->getComponent<core::IdentityComponent>(m_objectId);

	identity->setName(value.toStdString());
}