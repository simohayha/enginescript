#pragma once

#include <QWidget>
#include <QLabel>

class SceneWidget : public QWidget
{
public:
	SceneWidget(QWidget* parent = nullptr);

private:
	QLabel	m_fps;
};