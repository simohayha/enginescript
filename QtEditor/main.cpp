#include "QtEditor.h"
#include <QtWidgets/QApplication>

#include "DllEntry.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QtEditor w;

    w.show();

    return a.exec();
}
