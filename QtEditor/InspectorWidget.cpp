#include <QBoxLayout>
#include <QPushButton>

#include "EngineInterface.h"
#include "TransformComponent.h"
#include "CameraComponent.h"
#include "TransformInspector.h"
#include "IdentityInspector.h"
#include "MeshFilterInspector.h"
#include "CameraInspector.h"
#include "RenderInspector.h"
#include "TextInspector.h"
#include "MaterialInspector.h"
#include "HierarchyTreeItem.h"
#include "TextAsset.h"
#include "InspectorWidget.h"

InspectorWidget::InspectorWidget(QWidget* parent) :
	QListWidget(parent),
	m_locked(false)
{
	setStyleSheet("QListWidget { background: #e8eaed; } QListWidget::item { background: #e8eaed; } QListWidget::item:selected { background: #cdd2d8; }");
}

InspectorWidget::~InspectorWidget()
{
}

void InspectorWidget::onGameObjectSelected(const QModelIndex& index)
{
	if (m_locked)
	{
		return;
	}

	clear();

	auto	item = static_cast<HierarchyTreeItem*>(index.internalPointer());
	auto	id = item->getData(1).toInt();
	auto	engineInterface = EngineInterface::instance();

	{
		auto	identity = engineInterface->getComponent<core::IdentityComponent>(id);
		QListWidgetItem*	listItem;
		listItem = new QListWidgetItem(this);
		addItem(listItem);
		IdentityInspector*	identityInspector = new IdentityInspector(identity, this);
		listItem->setSizeHint(identityInspector->minimumSizeHint());
		setItemWidget(listItem, identityInspector);
	}

	{
		auto	transform = engineInterface->getComponent<core::TransformComponent>(id);
		QListWidgetItem*	listItem;
		listItem = new QListWidgetItem(this);
		addItem(listItem);
		TransformInspector*	transformInspector = new TransformInspector(transform, this);
		listItem->setSizeHint(transformInspector->minimumSizeHint());
		setItemWidget(listItem, transformInspector);
	}

	{
		auto	meshFilter = engineInterface->getComponent<core::MeshFilterComponent>(id);
		if (meshFilter)
		{
			QListWidgetItem*	listItem;
			listItem = new QListWidgetItem(this);
			addItem(listItem);
			MeshFilterInspector*	meshFilterInspector = new MeshFilterInspector(meshFilter, this);
			listItem->setSizeHint(meshFilterInspector->minimumSizeHint());
			setItemWidget(listItem, meshFilterInspector);
		}
	}

	{
		auto	camera = engineInterface->getComponent<core::CameraComponent>(id);
		if (camera)
		{
			QListWidgetItem*	listItem;
			listItem = new QListWidgetItem(this);
			addItem(listItem);
			CameraInspector*	cameraInspector = new CameraInspector(camera, this);
			listItem->setSizeHint(cameraInspector->minimumSizeHint());
			setItemWidget(listItem, cameraInspector);
		}
	}

	{
		auto	renderer = engineInterface->getComponent<core::RenderComponent>(id);
		if (renderer)
		{
			QListWidgetItem*	listItem;
			listItem = new QListWidgetItem(this);
			addItem(listItem);
			RenderInspector*	renderInspector = new RenderInspector(renderer, this);
			listItem->setSizeHint(renderInspector->minimumSizeHint());
			setItemWidget(listItem, renderInspector);
		}
	}

	{
		QPushButton*	addComponent = new QPushButton(this);
		addComponent->setText("Add component");
		addComponent->setMinimumHeight(25);
		QListWidgetItem*	listItem;
		listItem = new QListWidgetItem(this);
		addItem(listItem);
		setItemWidget(listItem, addComponent);
	}
}

void InspectorWidget::onAssetSelected(const QModelIndex& index)
{
	if (m_locked)
	{
		return;
	}

	clear();

	auto	item = static_cast<HierarchyTreeItem*>(index.internalPointer());
	auto	id = item->getData(1).toInt();
	auto	engineInterface = EngineInterface::instance();
	auto	type = engineInterface->getAssetType(id);

	if (type == core::E_MONOSCRIPT || type == core::E_SHADER)
	{
		auto	textAsset = engineInterface->getObject(id)->as<core::TextAsset>();
		if (textAsset)
		{
			std::string	text = textAsset->getText();
			QListWidgetItem*	listItem;
			listItem = new QListWidgetItem(this);
			addItem(listItem);
			TextInspector*	textInspector = new TextInspector(text, type, this);
			listItem->setSizeHint(textInspector->minimumSizeHint());
			setItemWidget(listItem, textInspector);
		}
	}
	else if (type == core::E_MATERIAL)
	{
		auto	material = engineInterface->getObject(id)->as<core::Material>();
		if (material)
		{
			QListWidgetItem*	listItem;
			listItem = new QListWidgetItem(this);
			addItem(listItem);
			MaterialInspector*	materialInspector = new MaterialInspector(material, this);
			listItem->setSizeHint(materialInspector->minimumSizeHint());
			setItemWidget(listItem, materialInspector);
		}
	}
}

void InspectorWidget::onLockedChanged(int state)
{
	if (state == Qt::Unchecked)
	{
		m_locked = false;
	}
	else if (state == Qt::Checked)
	{
		m_locked = true;
	}
}
