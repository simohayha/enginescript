#include <QMenu>

#include "Asset.h"
#include "EngineInterface.h"
#include "HierarchyTreeModel.h"
#include "HierarchyTreeItem.h"
#include "ProjectTree.h"

ProjectTree::ProjectTree(QWidget* parent) :
	QTreeView(parent),
	m_model(nullptr)
{
	QStringList	headers;
	headers << tr("Name") << tr("Id");
	m_model = new HierarchyTreeModel(headers);

	setModel(m_model);

	EngineInterface*	engineInterface = EngineInterface::instance();
	core::Asset*		assetFolder = engineInterface->getAssetFolder();
	m_model->setData(assetFolder);

	setDragEnabled(true);
	setAcceptDrops(true);
	setDropIndicatorShown(true);

	setContextMenuPolicy(Qt::CustomContextMenu);

	connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(showContextMenu(const QPoint&)));
	connect(this, SIGNAL(doubleClicked(const QModelIndex&)), this, SLOT(onDoubleClick(const QModelIndex&)));
}

ProjectTree::~ProjectTree()
{
}

void ProjectTree::showContextMenu(const QPoint& pos)
{
	QMenu	contextMenu(tr("Contxt menu"), this);

	QMenu*	createMenu = contextMenu.addMenu("Create");

	QAction	createDirectoryAction("Folder", createMenu);
	connect(&createDirectoryAction, SIGNAL(triggered()), this, SLOT(createFolder()));
	createMenu->addAction(&createDirectoryAction);

	createMenu->addSeparator();

	QAction	createCSharpScript("C# Script", createMenu);
	connect(&createCSharpScript, SIGNAL(triggered()), this, SLOT(createCSharpScript()));
	createMenu->addAction(&createCSharpScript);

	createMenu->addSeparator();

	QAction	createMaterial("Material", createMenu);
	connect(&createMaterial, SIGNAL(triggered()), this, SLOT(createMaterial()));
	createMenu->addAction(&createMaterial);

	QPoint	p;
	p.setX(pos.x());
	p.setY(pos.y() + 25);
	contextMenu.exec(mapToGlobal(p));
}

void ProjectTree::createFolder()
{
	createAsset(core::E_DIRECTORY);
}

void ProjectTree::createCSharpScript()
{
	createAsset(core::E_MONOSCRIPT);
}

void ProjectTree::createMaterial()
{
	createAsset(core::E_MATERIAL);
}

void ProjectTree::createAsset(core::AssetType type)
{
	QModelIndex			index = selectionModel()->currentIndex();
	QAbstractItemModel*	m = model();

	if (!m->insertRow(index.row() + 1, index.parent()))
	{
		return;
	}

	EngineInterface*	engineInterface = EngineInterface::instance();
	int	newObject = engineInterface->createAsset(type, nullptr);

	QModelIndex	child = m->index(index.row() + 1, 0, index.parent());
	m->setData(child, engineInterface->getObject(newObject)->getName().c_str());
	child = m->index(index.row() + 1, 1, index.parent());
	m->setData(child, newObject);
}

void ProjectTree::onDoubleClick(const QModelIndex& index)
{
	auto	name = index.data().toString().toStdString();
	auto	id = index.sibling(index.row(), 1).data().toInt();

	EngineInterface*	engineInterface = EngineInterface::instance();
	if (engineInterface->getAssetType(id) == core::E_SCENE)
	{
		engineInterface->loadScene(id);
	}
}
