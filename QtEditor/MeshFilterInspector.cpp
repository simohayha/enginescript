#include <QBoxLayout>
#include <QLabel>

#include "MeshFilterComponent.h"
#include "MeshFilterInspector.h"

MeshFilterInspector::MeshFilterInspector(core::MeshFilterComponent* meshFilter, QWidget* parent) :
	QWidget(parent),
	m_objectId(-1)
{
	m_objectId = meshFilter->getOwner();

	QGridLayout*	grid = new QGridLayout(this);

	QLabel* meshFilterLabel = new QLabel(this);
	meshFilterLabel->setStyleSheet("font-weight: bold;");
	meshFilterLabel->setText("Mesh Filter");
	grid->addWidget(meshFilterLabel, 0, 0);
}

MeshFilterInspector::~MeshFilterInspector()
{
}
