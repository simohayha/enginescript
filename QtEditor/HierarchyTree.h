#pragma once

#include <QTreeView>

class HierarchyTreeModel;

class HierarchyTree : public QTreeView
{
	Q_OBJECT

public:
	HierarchyTree(QWidget* parent = nullptr);
	~HierarchyTree();

public slots:
	void	showContextMenu(const QPoint&);
	void	addGameObject();
	void	addGameObjectChild();

private:
	HierarchyTreeModel*	m_model;
};
