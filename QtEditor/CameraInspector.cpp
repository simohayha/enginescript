#include <QBoxLayout>
#include <QLabel>
#include <QDoubleSpinBox>

#include "EngineInterface.h"
#include "CameraComponent.h"
#include "CameraInspector.h"

CameraInspector::CameraInspector(core::CameraComponent* camera, QWidget* parent) :
	QWidget(parent),
	m_objectId(-1),
	m_aspect(nullptr),
	m_near(nullptr),
	m_far(nullptr),
	m_fov(nullptr)
{
	m_objectId = camera->getOwner();

	QGridLayout*	grid = new QGridLayout(this);

	{
		QLabel* cameraLabel = new QLabel(this);
		cameraLabel->setStyleSheet("font-weight: bold;");
		cameraLabel->setText("Camera");
		grid->addWidget(cameraLabel, 0, 0);
	}

	{
		QLabel* aspectLabel = new QLabel(this);
		aspectLabel->setText("Aspect");
		grid->addWidget(aspectLabel, 1, 0);

		m_aspect = new QDoubleSpinBox(this);
		m_aspect->setSingleStep(0.01);
		m_aspect->setMinimum(0.01);
		//m_aspect->setValue(camera->getAspect());
		grid->addWidget(m_aspect, 1, 1);

		connect(m_aspect, SIGNAL(valueChanged(double)), this, SLOT(valueChanged(double)));
	}

	{
		QLabel* nearLabel = new QLabel(this);
		nearLabel->setText("Near");
		grid->addWidget(nearLabel, 2, 0);

		m_near = new QDoubleSpinBox(this);
		m_near->setRange(0.01, 100000.0);
		m_near->setMinimum(0.01);
		//m_near->setValue(camera->getNear());
		grid->addWidget(m_near, 2, 1);

		connect(m_near, SIGNAL(valueChanged(double)), this, SLOT(valueChanged(double)));
	}

	{
		QLabel* farLabel = new QLabel(this);
		farLabel->setText("Far");
		grid->addWidget(farLabel, 3, 0);

		m_far = new QDoubleSpinBox(this);
		m_far->setRange(0.01, 100000.0);
		m_far->setMinimum(0.01);
		//m_far->setValue(camera->getFar());
		grid->addWidget(m_far, 3, 1);

		connect(m_far, SIGNAL(valueChanged(double)), this, SLOT(valueChanged(double)));
	}

	{
		QLabel* fovLabel = new QLabel(this);
		fovLabel->setText("Fov");
		grid->addWidget(fovLabel, 4, 0);

		m_fov = new QDoubleSpinBox(this);
		m_fov->setSingleStep(0.01);
		m_fov->setMinimum(0.01);
		//m_fov->setValue(camera->getFov());
		grid->addWidget(m_fov, 4, 1);

		connect(m_fov, SIGNAL(valueChanged(double)), this, SLOT(valueChanged(double)));
	}
}

CameraInspector::~CameraInspector()
{
}

void CameraInspector::paintEvent(QPaintEvent* evt)
{
	auto	engineInterface = EngineInterface::instance();
	auto	camera = engineInterface->getComponent<core::CameraComponent>(m_objectId);

	m_aspect->setValue(camera->getAspect());
	m_near->setValue(camera->getNear());
	m_far->setValue(camera->getFar());
	m_fov->setValue(camera->getFov());
}

void CameraInspector::valueChanged(double d)
{
	auto	engineInterface = EngineInterface::instance();
	auto	camera = engineInterface->getComponent<core::CameraComponent>(m_objectId);

	auto	s = sender();

	if (s == m_aspect)
	{
		camera->setAspect(d);
	}
	else if (s == m_near)
	{
		camera->setNear(d);
	}
	else if (s == m_far)
	{
		camera->setFar(d);
	}
	else if (s == m_fov)
	{
		camera->setFov(d);
	}
}