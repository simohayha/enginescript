#include <QBoxLayout>
#include <QLabel>
#include <QComboBox>

#include "EngineInterface.h"
#include "ObjectField.h"
#include "RenderComponent.h"
#include "RenderInspector.h"

RenderInspector::RenderInspector(core::RenderComponent* renderer, QWidget* parent) :
	QWidget(parent),
	m_objectId(-1)
{
	m_objectId = renderer->getOwner();
	
	QGridLayout*	grid = new QGridLayout(this);

	QLabel* renderLabel = new QLabel(this);
	renderLabel->setStyleSheet("font-weight: bold;");
	renderLabel->setText("Renderer");
	grid->addWidget(renderLabel, 0, 0);

	QLabel* materialLabel = new QLabel(this);
	materialLabel->setText("Material");
	grid->addWidget(materialLabel, 1, 0);
	ObjectField*	field = new ObjectField(renderer->getMaterial(), this);
	grid->addWidget(field, 2, 0);

	m_renderMode = new QComboBox(this);
	m_renderMode->addItem("Solid", core::E_RENDERMODE_SOLID);
	m_renderMode->addItem("Wireframe", core::E_RENDERMODE_WIREFRAME);
	grid->addWidget(m_renderMode, 3, 0);

	connect(field, SIGNAL(changed(core::Object*)), this, SLOT(onMaterialChanged(core::Object*)));
	connect(m_renderMode, SIGNAL(currentIndexChanged(int)), this, SLOT(onRenderModeIndexChanged(int)));

	setLayout(grid);
}

RenderInspector::~RenderInspector()
{
}

void RenderInspector::onRenderModeIndexChanged(int index)
{
	QVariant	data = m_renderMode->itemData(index);
	auto		mode = static_cast<core::RenderMode>(data.toInt());
	auto		engineInterface = EngineInterface::instance();
	auto		renderer = engineInterface->getComponent<core::RenderComponent>(m_objectId);
	if (renderer)
	{
		renderer->setRenderMode(mode);
	}
}

void RenderInspector::onMaterialChanged(core::Object* newMaterial)
{
	auto	engineInterface = EngineInterface::instance();
	auto	renderer = engineInterface->getComponent<core::RenderComponent>(m_objectId);
	if (renderer)
	{
		auto	material = newMaterial->as<core::Material>();
		renderer->setMaterial(material);
	}
}