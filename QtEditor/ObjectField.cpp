#include <QLabel>
#include <QHBoxLayout>
#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QDragLeaveEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QPushButton>

#include "EngineInterface.h"
#include "HierarchyTreeItem.h"
#include "ObjectField.h"

ObjectField::ObjectField(core::Object* defaultValue, bool erasable, QWidget* parent) :
	QWidget(parent),
	m_name(nullptr),
	m_erasable(erasable)
{
	setStyleSheet("background: #f9fbfc");

	setAcceptDrops(true);
	setAutoFillBackground(true);

	QHBoxLayout*	layout = new QHBoxLayout();

	m_name = new QLabel(this);
	m_name->setMargin(5);
	m_name->setText("None");
	layout->addWidget(m_name);

	if (defaultValue)
	{
		QString	name;
		name.append(defaultValue->getName().c_str());
		name.append(" (");
		name.append(core::AssetTypeName[defaultValue->getAssetType()]);
		name.append(")");

		m_name->setText(name);
	}

	if (erasable)
	{
		QPushButton*	supprButton = new QPushButton(this);
		supprButton->setText("Del");
		supprButton->setMaximumWidth(20);
		layout->addWidget(supprButton);
		connect(supprButton, SIGNAL(pressed()), this, SLOT(onDelPressed()));
	}

	QPushButton*	moreButton = new QPushButton(this);
	moreButton->setText("...");
	moreButton->setMaximumWidth(20);
	layout->addWidget(moreButton);

	setLayout(layout);
}

ObjectField::~ObjectField()
{
}

void ObjectField::dragEnterEvent(QDragEnterEvent* evt)
{
	evt->acceptProposedAction();
}

void ObjectField::dragMoveEvent(QDragMoveEvent* evt)
{
	evt->acceptProposedAction();
}

void ObjectField::dragLeaveEvent(QDragLeaveEvent* evt)
{
}

void ObjectField::dropEvent(QDropEvent* evt)
{
	const QMimeData*	mimeData = evt->mimeData();

	if (mimeData->hasFormat("application/engineobject"))
	{
		QByteArray	encodedData = mimeData->data("application/engineobject");
		HierarchyTreeItem*	item = (HierarchyTreeItem*)encodedData.toULongLong();
		if (!item)
		{
			return;
		}
		int	id = item->getData(1).toInt();

		EngineInterface*	engineInterface = EngineInterface::instance();
		auto	type = engineInterface->getAssetType(id);
		auto	obj = engineInterface->getObject(id);

		QString	name;
		name.append(obj->getName().c_str());
		name.append(" (");
		name.append(core::AssetTypeName[type]);
		name.append(")");

		m_name->setText(name);

		emit changed(obj);
	}
}

void ObjectField::onDelPressed()
{
	m_name->setText("None");
	emit changed(nullptr);
}
