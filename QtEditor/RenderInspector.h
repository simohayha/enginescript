#pragma once

#include <QWidget>

class QComboBox;

namespace core
{
	class RenderComponent;
	class Object;
}

class RenderInspector : public QWidget
{
	Q_OBJECT
public:
	RenderInspector(core::RenderComponent* renderer, QWidget* parent = nullptr);
	~RenderInspector();

private slots:
	void	onMaterialChanged(core::Object* newMaterial);
	void	onRenderModeIndexChanged(int index);

private:
	int			m_objectId;
	QComboBox*	m_renderMode;
};