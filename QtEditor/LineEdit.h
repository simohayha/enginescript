#pragma once

#include <QLineEdit>

class LineEdit : public QLineEdit
{
	Q_OBJECT
public:
	LineEdit(QWidget* parent = nullptr);
	~LineEdit();

signals:
	void	focussed(bool hasFocus);

protected:
	virtual void	focusInEvent(QFocusEvent* evt) override;
	virtual void	focusOutEvent(QFocusEvent* evt) override;
};