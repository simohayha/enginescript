#pragma once

#include <vector>

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

namespace core
{
	class Asset;
}

class HierarchyTreeItem;

class HierarchyTreeModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	HierarchyTreeModel(const QStringList& headers,  QObject* parent = nullptr);
	~HierarchyTreeModel();

	void				setData(std::vector<int>& data);
	void				setData(core::Asset* assetFolder);

	HierarchyTreeItem*	addObject(int gameObject, HierarchyTreeItem* parent = nullptr);
	HierarchyTreeItem*	addAsset(core::Asset* asset, HierarchyTreeItem* parent = nullptr);

	virtual QVariant	data(const QModelIndex& index, int role) const override;
	virtual QVariant	headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	virtual QModelIndex	index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
	virtual QModelIndex	parent(const QModelIndex& index) const override;

	virtual int			rowCount(const QModelIndex& parent = QModelIndex()) const override;
	virtual int			columnCount(const QModelIndex& parent = QModelIndex()) const override;

	virtual Qt::ItemFlags	flags(const QModelIndex& index) const override;
	virtual bool			setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
	virtual bool			setHeaderData(int section, Qt::Orientation orientation, const QVariant& value, int role = Qt::EditRole) override;

	virtual bool			insertColumns(int position, int columns, const QModelIndex& parent = QModelIndex()) override;
	virtual bool			removeColumns(int position, int columns, const QModelIndex& parent = QModelIndex()) override;
	virtual bool			insertRows(int position, int rows, const QModelIndex& parent = QModelIndex()) override;
	virtual bool			removeRows(int position, int rows, const QModelIndex& parent = QModelIndex()) override;
	virtual bool			moveRows(const QModelIndex &sourceParent, int sourceRow, int count,	const QModelIndex &destinationParent, int destinationChild) override;

	virtual Qt::DropActions	supportedDropActions() const override;
	virtual QStringList		mimeTypes() const override;
	virtual QMimeData*		mimeData(const QModelIndexList& indexes) const override;
	virtual bool			canDropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent) const override;
	virtual bool			dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent) override;

	HierarchyTreeItem*	getRoot();
	QModelIndex			getRootIndex();
	QModelIndex			publicCreateIndex(int row, int col, void* data);

public slots:
	void	dataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles = QVector<int>());

private:
	HierarchyTreeItem*	m_rootItem;

	HierarchyTreeItem*	getItem(const QModelIndex& index) const;

	void	setDataRecursive(int gameObject, HierarchyTreeItem* parent);
	void	setDataRecursive(core::Asset* asset, HierarchyTreeItem* parent);
};