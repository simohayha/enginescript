#include <functional>

#include <QSettings>
#include <QByteArray>
#include <QDir>
#include <QPushButton>
#include <QDebug>

#include "EngineInterface.h"

#include "QtEditor.h"

QtEditor::QtEditor(QWidget* parent, Qt::WindowFlags flags)
	: QMainWindow(parent, flags)
{
	EngineInterface::instance()->setLoggerCallback(std::bind(&QtEditor::loggingCallback, this, std::placeholders::_1, std::placeholders::_2));

	ui.setupUi(this);

	connect(ui.actionSave, SIGNAL(triggered()), this, SLOT(save()));

	QPushButton*	button = new QPushButton(this);
	button->setText("Show console");
	statusBar()->addPermanentWidget(button);

	connect(ui.HierarchyTreeView, SIGNAL(clicked(const QModelIndex&)), ui.InspectorListView, SLOT(onGameObjectSelected(const QModelIndex&)));
	connect(ui.ProjectTreeView, SIGNAL(clicked(const QModelIndex&)), ui.InspectorListView, SLOT(onAssetSelected(const QModelIndex&)));
	connect(ui.Locked, SIGNAL(stateChanged(int)), ui.InspectorListView, SLOT(onLockedChanged(int)));
	connect(button, SIGNAL(pressed()), this, SLOT(openConsole()));

	m_settingFile = QDir::currentPath() + "/DefaultLayout.ini";
	loadSettings();
}

void QtEditor::closeEvent(QCloseEvent* evt)
{
	saveSettings();
	QMainWindow::closeEvent(evt);
}

void QtEditor::save()
{
	EngineInterface::instance()->save();
}

void QtEditor::loggingCallback(core::LogLevel level, std::string message)
{
	qDebug() << message.c_str();
	statusBar()->showMessage(tr(message.c_str()));
}

void QtEditor::loadSettings()
{
	QSettings	settings(m_settingFile, QSettings::IniFormat);
	restoreGeometry(settings.value("geometry").toByteArray());
	restoreState(settings.value("windowState").toByteArray());
}

void QtEditor::saveSettings()
{
	QSettings	settings(m_settingFile, QSettings::IniFormat);
	settings.setValue("geometry", saveGeometry());
	settings.setValue("windowState", saveState());
	settings.sync();
}

void QtEditor::openConsole()
{
	QDockWidget*	console = new QDockWidget(this);
	console->setFloating(true);
	console->show();
}