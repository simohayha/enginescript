#include "Engine.h"
#include "DllEntry.h"
#include "ObjectFactory.hpp"
#include "EngineData.hpp"
#include "EngineInterface.h"

EngineInterface* EngineInterface::m_instance = nullptr;

EngineInterface::EngineInterface() :
	m_engine(nullptr)
{
}

EngineInterface::~EngineInterface()
{
}

EngineInterface* EngineInterface::instance()
{
	if (!m_instance)
	{
		m_instance = new EngineInterface();
	}
	return m_instance;
}

CUnmanaged* EngineInterface::engine()
{
	return m_engine.get();
}

const CUnmanaged* EngineInterface::engine() const
{
	return m_engine.get();
}

bool EngineInterface::initialize(HWND parent, unsigned int width, unsigned int height)
{
	m_engine.reset(new(std::nothrow) CUnmanaged(nullptr));
	if (!m_engine)
	{
		return false;
	}

	if (!m_engine->initialize(parent, width, height))
	{
		return false;
	}

	return true;
}

void EngineInterface::clean()
{
	m_engine.reset(nullptr);
}

void EngineInterface::pause()
{
	m_engine->pause();
}

void EngineInterface::update()
{
	m_engine->update();
}

void EngineInterface::render()
{
	m_engine->render();
}

void EngineInterface::frame()
{
	m_engine->frame();
}

void EngineInterface::resize(unsigned int width, unsigned int height)
{
	m_engine->resize(height, width);
}

void EngineInterface::getRootGameObjects(std::vector<int>& gameObjects)
{
	m_engine->getRootGameObjects(gameObjects);
}

int EngineInterface::addGameObject(int parent)
{
	return m_engine->addGameObject(parent);
}

core::Asset* EngineInterface::getAssetFolder()
{
	return m_engine->getAssetFolder();
}

int EngineInterface::createAsset(core::AssetType type, core::Asset* parent)
{
	return core::ObjectFactory::createAsset(type, parent)->getId();
}

int EngineInterface::createAsset(core::AssetType type, int parent)
{
	core::Asset*	parentPtr = nullptr;
	if (parent != -1)
	{
		parentPtr = dynamic_cast<core::Asset*>(core::EngineData::getObject(parent));
	}
	return createAsset(type, parentPtr);
}

core::AssetType EngineInterface::getAssetType(int objectId)
{
	return m_engine->getAssetType(objectId);
}

core::Object* EngineInterface::getObject(int objectId)
{
	return m_engine->getObject(objectId);
}

void EngineInterface::setLoggerCallback(std::function<void(core::LogLevel, std::string)> callback)
{
	core::Log::get()->setLoggingCallback(callback);
}

bool EngineInterface::isTypeOf(int objectId, core::AssetType type)
{
	return core::EngineData::isTypeOf(objectId, type);
}

bool EngineInterface::isTypeChildOf(int objectId, core::AssetType type)
{
	bool	directType = isTypeOf(objectId, type);
	if (directType)
	{
		return true;
	}
	else
	{
		return core::EngineData::isTypeChildOf(objectId, type);
	}
}

void EngineInterface::save()
{
	core::EngineData::save();
}

void EngineInterface::loadScene(int id)
{
}
