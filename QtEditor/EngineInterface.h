#pragma once

#include <vector>
#include <memory>

#include "Log.h"
#include "DllEntry.h"

namespace core
{
	class Asset;
	class Object;
}

class EngineInterface
{
public:
	EngineInterface();
	~EngineInterface();

	static EngineInterface*	instance();

	CUnmanaged*			engine();
	const CUnmanaged*	engine() const;

	bool				initialize(HWND parent, unsigned int width, unsigned int height);
	void				clean();

	void				pause();
	void				update();
	void				render();
	void				frame();
	void				resize(unsigned int width, unsigned int height);

	void				getRootGameObjects(std::vector<int>& gameObjects);
	int					addGameObject(int parent);
	core::Asset*		getAssetFolder();
	int					createAsset(core::AssetType type, core::Asset* parent = nullptr);
	int					createAsset(core::AssetType type, int parent = -1);

	template <class T>
	T*	getComponent(int objectId)
	{
		return m_engine->getComponent<T>(objectId);
	}

	core::AssetType		getAssetType(int objectId);
	core::Object*		getObject(int objectId);

	void				setLoggerCallback(std::function<void(core::LogLevel, std::string)> callback);

	bool				isTypeOf(int objectId, core::AssetType type);
	bool				isTypeChildOf(int objectId, core::AssetType type);

	void				save();
	void				loadScene(int id);

private:
	static EngineInterface*	m_instance;

	std::unique_ptr<CUnmanaged>	m_engine;
};