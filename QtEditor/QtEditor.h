#pragma once

#include <QtWidgets/QMainWindow>
#include <QString>

#include "EngineInterface.h"

#include "ui_QtEditor.h"

class QtEditor : public QMainWindow
{
    Q_OBJECT

public:
    QtEditor(QWidget* parent = nullptr, Qt::WindowFlags flags = 0);

protected:
	virtual void closeEvent(QCloseEvent* evt) override;

protected slots:
	void	openConsole();
	void	save();

private:
    Ui::QtEditorClass ui;

	QString	m_settingFile;

	void	loggingCallback(core::LogLevel, std::string);

	void	loadSettings();
	void	saveSettings();
};
