#include <climits>

#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QSignalMapper>
#include <QDoubleSpinBox>

#include "EngineInterface.h"
#include "TransformComponent.h"
#include "FloatField.h"
#include "TransformInspector.h"

#undef min
#undef max

TransformInspector::TransformInspector(core::TransformComponent* transform, QWidget* parent) :
	QWidget(parent),
	m_objectId(-1),
	m_positionX(nullptr),
	m_positionY(nullptr),
	m_positionZ(nullptr),
	m_rotationX(nullptr),
	m_rotationY(nullptr),
	m_rotationZ(nullptr),
	m_scaleX(nullptr),
	m_scaleY(nullptr),
	m_scaleZ(nullptr)
{
	m_objectId = transform->getOwner();

	QGridLayout*	grid = new QGridLayout(this);

	QLabel* transformLabel = new QLabel(this);
	transformLabel->setText("Transform");
	transformLabel->setStyleSheet("font-weight: bold;");
	grid->addWidget(transformLabel, 0, 0);

	{
		QLabel* positionLabel = new QLabel(this);
		positionLabel->setText("Position");
		grid->addWidget(positionLabel, 1, 0);
		m_positionX = new FloatField(this);
		grid->addWidget(m_positionX, 1, 1);
		m_positionY = new FloatField(this);
		grid->addWidget(m_positionY, 1, 2);
		m_positionZ = new FloatField(this);
		grid->addWidget(m_positionZ, 1, 3);

		connect(m_positionX, SIGNAL(valueChanged(float)), this, SLOT(valueChanged(float)));
		connect(m_positionY, SIGNAL(valueChanged(float)), this, SLOT(valueChanged(float)));
		connect(m_positionZ, SIGNAL(valueChanged(float)), this, SLOT(valueChanged(float)));

		m_positionX->setRange(-500000.0f, 500000.0f);
		m_positionY->setRange(-500000.0f, 500000.0f);
		m_positionZ->setRange(-500000.0f, 500000.0f);
	}

	{
		QLabel* rotationLabel = new QLabel(this);
		rotationLabel->setText("Rotation");
		grid->addWidget(rotationLabel, 2, 0);
		m_rotationX = new FloatField(this);
		grid->addWidget(m_rotationX, 2, 1);
		m_rotationY = new FloatField(this);
		grid->addWidget(m_rotationY, 2, 2);
		m_rotationZ = new FloatField(this);
		grid->addWidget(m_rotationZ, 2, 3);

		connect(m_rotationX, SIGNAL(valueChanged(float)), this, SLOT(valueChanged(float)));
		connect(m_rotationY, SIGNAL(valueChanged(float)), this, SLOT(valueChanged(float)));
		connect(m_rotationZ, SIGNAL(valueChanged(float)), this, SLOT(valueChanged(float)));

		m_rotationX->setRange(-500000.0f, 500000.0f);
		m_rotationY->setRange(-500000.0f, 500000.0f);
		m_rotationZ->setRange(-500000.0f, 500000.0f);
	}

	{
		QLabel* scaleLabel = new QLabel(this);
		scaleLabel->setText("Scale");
		grid->addWidget(scaleLabel, 3, 0);
		m_scaleX = new FloatField(this);
		grid->addWidget(m_scaleX, 3, 1);
		m_scaleY = new FloatField(this);
		grid->addWidget(m_scaleY, 3, 2);
		m_scaleZ = new FloatField(this);
		grid->addWidget(m_scaleZ, 3, 3);

		connect(m_scaleX, SIGNAL(valueChanged(float)), this, SLOT(valueChanged(float)));
		connect(m_scaleY, SIGNAL(valueChanged(float)), this, SLOT(valueChanged(float)));
		connect(m_scaleZ, SIGNAL(valueChanged(float)), this, SLOT(valueChanged(float)));

		m_scaleX->setRange(-500000.0f, 500000.0f);
		m_scaleY->setRange(-500000.0f, 500000.0f);
		m_scaleZ->setRange(-500000.0f, 500000.0f);
	}

	setLayout(grid);
}

TransformInspector::~TransformInspector()
{
}

QSize TransformInspector::sizeHint() const
{
	return QSize(10, 10);
}

QSize TransformInspector::minimumSizeHint() const
{
	return QSize(100, 125);
}

void TransformInspector::paintEvent(QPaintEvent* evt)
{
	auto	engineInterface = EngineInterface::instance();
	auto	transform = engineInterface->getComponent<core::TransformComponent>(m_objectId);

	m_positionX->blockSignals(true);
	m_positionY->blockSignals(true);
	m_positionZ->blockSignals(true);
	m_rotationX->blockSignals(true);
	m_rotationY->blockSignals(true);
	m_rotationZ->blockSignals(true);
	m_scaleX->blockSignals(true);
	m_scaleY->blockSignals(true);
	m_scaleZ->blockSignals(true);

	m_positionX->setValue(transform->getPosition().x);
	m_positionY->setValue(transform->getPosition().y);
	m_positionZ->setValue(transform->getPosition().z);

	m_rotationX->setValue(transform->getRotation().x);
	m_rotationY->setValue(transform->getRotation().y);
	m_rotationZ->setValue(transform->getRotation().z);

	m_scaleX->setValue(transform->getScale().x);
	m_scaleY->setValue(transform->getScale().y);
	m_scaleZ->setValue(transform->getScale().z);

	m_positionX->blockSignals(false);
	m_positionY->blockSignals(false);
	m_positionZ->blockSignals(false);
	m_rotationX->blockSignals(false);
	m_rotationY->blockSignals(false);
	m_rotationZ->blockSignals(false);
	m_scaleX->blockSignals(false);
	m_scaleY->blockSignals(false);
	m_scaleZ->blockSignals(false);
}

void TransformInspector::valueChanged(float f)
{
	auto	engineInterface = EngineInterface::instance();
	auto	transform = engineInterface->getComponent<core::TransformComponent>(m_objectId);

	auto	s = sender();

	bool			rotationUpdate = false;
	core::Vector3	eulerAngle = transform->getEulerAngle();

	if (s == m_positionX)
	{
		transform->getPosition().x = f;
	}
	else if (s == m_positionY)
	{
		transform->getPosition().y = f;
	}
	else if (s == m_positionZ)
	{
		transform->getPosition().z = f;
	}
	else if (s == m_rotationX)
	{
		transform->getRotation().x = f;
		//eulerAngle.x = f;
		//rotationUpdate = true;
	}
	else if (s == m_rotationY)
	{
		transform->getRotation().y = f;
		//eulerAngle.y = f;
		//rotationUpdate = true;
	}
	else if (s == m_rotationZ)
	{
		transform->getRotation().z = f;
		//eulerAngle.z = f;
		//rotationUpdate = true;
	}
	else if (s == m_scaleX)
	{
		transform->getScale().x = f;
	}
	else if (s == m_scaleY)
	{
		transform->getScale().y = f;
	}
	else if (s == m_scaleZ)
	{
		transform->getScale().z = f;
	}

	//float	deg2rad = (DirectX::XM_PI * 2.0f) / 360.0f;
	//if (rotationUpdate)
	//{
	//	transform->getRotation() = core::Quaternion::CreateFromYawPitchRoll(eulerAngle.x * deg2rad, eulerAngle.y * deg2rad, eulerAngle.z * deg2rad);
	//}

	//m_positionX->setValue(transform->getPosition().x);
	//m_positionY->setValue(transform->getPosition().y);
	//m_positionZ->setValue(transform->getPosition().z);

	//m_rotationX->setValue(transform->getRotation().x);
	//m_rotationY->setValue(transform->getRotation().y);
	//m_rotationZ->setValue(transform->getRotation().z);

	//m_scaleX->setValue(transform->getScale().x);
	//m_scaleY->setValue(transform->getScale().y);
	//m_scaleZ->setValue(transform->getScale().z);
}