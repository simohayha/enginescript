#pragma once

#include <QVariant>
#include <QVector>

class HierarchyTreeItem
{
public:
	HierarchyTreeItem(const QVector<QVariant>& data, HierarchyTreeItem* parentItem = nullptr);
	~HierarchyTreeItem();

	void				appendChild(HierarchyTreeItem* item);
	bool				removeChild(int position, int count);
	bool				deleteChild(int position, int count);
	void				moveChild(int row, int count, int destinationChild);
	void				setParent(HierarchyTreeItem* parent, int destinationChild);
	bool				insertChild(int position, int count, int columns);
	void				insertChild(HierarchyTreeItem* item, int position);
	bool				insertColumns(int position, int columns);
	bool				removeColumns(int position, int columns);


	HierarchyTreeItem*	getChild(int row);
	int					getChildCount() const;
	int					getColumnCount() const;
	QVariant			getData(int column) const;
	int					getRow() const;
	HierarchyTreeItem*	getParentItem();
	HierarchyTreeItem*	getParent();
	int					getChildNumber() const;
	bool				setData(int column, const QVariant& value);

private:
	QList<HierarchyTreeItem*>	m_childItems;
	QVector<QVariant>			m_itemData;
	HierarchyTreeItem*			m_parentItem;
};