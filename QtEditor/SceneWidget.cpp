#include <QGridLayout>
#include <QStackedLayout>

#include "D3DScene.h"
#include "SceneWidget.h"

SceneWidget::SceneWidget(QWidget* parent) :
	QWidget(parent)
{
	QStackedLayout*	grid = new QStackedLayout(this);
	setLayout(grid);

	D3DScene*		d3dScene = new D3DScene(this);
	grid->addWidget(d3dScene);
}