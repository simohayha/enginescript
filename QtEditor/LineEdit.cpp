#include "LineEdit.h"

LineEdit::LineEdit(QWidget* parent) :
	QLineEdit(parent)
{
}

LineEdit::~LineEdit()
{
}

void LineEdit::focusInEvent(QFocusEvent* evt)
{
	QLineEdit::focusInEvent(evt);
	emit(focussed(true));
}

void LineEdit::focusOutEvent(QFocusEvent* evt)
{
	QLineEdit::focusOutEvent(evt);
	emit(focussed(false));
}