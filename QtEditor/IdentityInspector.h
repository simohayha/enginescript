#pragma once

#include <QWidget>

namespace core
{
	class IdentityComponent;
}

class QLineEdit;

class IdentityInspector : public QWidget
{
	Q_OBJECT
public:
	IdentityInspector(core::IdentityComponent* identity, QWidget* parent = nullptr);
	~IdentityInspector();

protected slots:
	void	textChanged(const QString& value);

private:
	int			m_objectId;

	QLineEdit*	m_nameInput;
};