#include <iostream>

#include "DllEntry.h"

class MyFeedback : public CUnmanaged::Feedback
{
	virtual void onClick() override
	{
	}
};

int CALLBACK WinMain(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE hPrevInstance,
	_In_ LPSTR     lpCmdLine,
	_In_ int       nCmdShow
)
{
	MyFeedback*	feedback = new MyFeedback();

	try
	{
		CUnmanaged*	unmanaged = new CUnmanaged(feedback);
		unmanaged->initialize(hInstance, nCmdShow);
		unmanaged->pause();
		while (true)
		{
			unmanaged->run(nullptr);
			//Sleep(1);
		}
	}
	catch (const std::exception& ex)
	{
		std::cout << ex.what() << '\n';
	}

	return 0;
}