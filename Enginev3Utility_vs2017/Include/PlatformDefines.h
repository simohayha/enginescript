#pragma once

#define EG_PLATFORM_WIN32 1

#if defined(__WIN32__) || defined(_WIN32)
#define EG_PLATFORM EG_PLATFORM_WIN32
#endif

#if EG_PLATFORM == EG_PLATFORM_WIN32
#if defined(EG_UTILITY_EXPORTS)
#define EG_UTILITY_EXPORT __declspec(dllexport)
#else
#define EG_UTILITY_EXPORT __declspec(dllimport)
#endif
#endif