#pragma once

#include "PrequisitesUtil.h"

#if EG_PLATFORM == EG_PLATFORM_WIN32
struct HINSTANCE__;
typedef struct HINSTANCE__* hInstance;
#endif

#if EG_PLATFORM == EG_PLATFORM_WIN32
#define DYNLIB_HANDLE hInstance
#define DYNLIB_LOAD(a) LoadLibraryEx(a, NULL, LOAD_WITH_ALTERED_SEARCH_PATH)
#define DYNLIB_GETSYM(a, b) GetProcAddress(a, b)
#define DYNLIB_UNLOAD(a) !FreeLibrary(a)
#endif

namespace eg
{
	class EG_UTILITY_EXPORT DynLib
	{
	public:
		static const char* EXTENSION;

		DynLib(const String& name);
		~DynLib();

		void load();

		void unload();

		const String& getName() const;

		void* getSymbol(const String& strName) const;

	protected:
		friend class DynLibManager;

		String dynLibError();

	protected:
		String m_name;
		DYNLIB_HANDLE m_hInst;
	};
}