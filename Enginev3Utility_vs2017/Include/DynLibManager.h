#pragma once

#include "PrequisitesUtil.h"
#include "Module.h"

namespace eg
{
	class EG_UTILITY_EXPORT DynLibManager : public Module<DynLibManager>
	{
	public:
		DynLibManager();
		virtual ~DynLibManager();

		DynLib* load(const String& name);

		void unload(DynLib* lib);

	protected:
		Map<String, DynLib*> m_loadedLibraries;
	};

	EG_UTILITY_EXPORT DynLibManager& gDynLibManager();
}