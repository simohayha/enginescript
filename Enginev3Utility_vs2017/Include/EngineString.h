#pragma once

#include <string>
#include <sstream>

#include "PlatformDefines.h"

namespace eg
{
	typedef std::string String;
	typedef std::wstring WString;
	typedef std::stringstream StringStream;
	typedef std::wstringstream WStringStream;
}

namespace eg
{
	class EG_UTILITY_EXPORT StringUtil
	{
	public:
		static const String BLANK;

		static const WString WBLANK;
	};
}