#pragma once

#include "PrequisitesUtil.h"

namespace eg
{
	template <class T>
	class Module
	{
	public:
		static T& instance()
		{
			if (!isStartedUp())
			{
				throw;
			}

			if (isDestroyed())
			{
				throw;
			}

			return *_instance();
		}

		static T* instancePtr()
		{
			if (!isStartedUp())
			{
				throw;
			}

			if (isDestroyed())
			{
				throw;
			}

			return _instance();
		}

		template <class... Args>
		static void startUp(Args&&... args)
		{
			if (isStartedUp())
			{
				throw;
			}

			_instance() = new T(std::forward<Args>(args)...);
			isStartedUp() = true;

			static_cast<Module*>(_instance())->onStartUp();
		}

		template <class SubType, class... Args>
		static void startUp(Args&&... args)
		{
			static_assert(std::is_base_of<T, SubType>::value, "Provided type is not derived from type the Module is initialized with.");

			if (isStartedUp())
			{
				throw;
			}

			_instance() = new SubType(std::forward<Args>(args)...);
			isStartedUp() = true;

			static_cast<Module*>(_instance())->onStartUp();
		}

		static void shutDown()
		{
			if (isDestroyed())
			{
				throw;
			}

			if (!isStartedUp())
			{
				throw;
			}

			static_cast<Module*>(_instance())->onShutDown();

			delete _instance();
			isDestroyed() = true;
		}

		static bool isStarted()
		{
			return isStartedUp() && !isDestroyed();
		}

	protected:
		Module() = default;

		virtual ~Module()
		{
			_instance();
			isDestroyed() = true;
		}

		Module(const Module&) = default;
		Module& operator=(const Module&) = default;

		virtual void onStartUp()
		{
		}

		virtual void onShutDown()
		{
		}

		static T*& _instance()
		{
			static T* inst = nullptr;
			return inst;
		}

		static bool& isDestroyed()
		{
			static bool inst = false;
			return inst;
		}

		static bool& isStartedUp()
		{
			static bool inst = false;
			return inst;
		}
	};
}