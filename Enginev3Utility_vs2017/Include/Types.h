#pragma once

#include "PlatformDefines.h"

namespace eg
{
	typedef char INT8;
	typedef unsigned char UINT8;
	typedef short INT16;
	typedef unsigned short UINT16;
	typedef int INT32;
	typedef unsigned int UINT32;
	typedef __int64 INT64;
	typedef unsigned __int64 UINT64;
}