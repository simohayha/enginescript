#pragma once

namespace eg
{
	enum class WindowBorder
	{
		Normal,
		None,
		Fixed
	};

	class DynLib;
	class DynLibManager;
}