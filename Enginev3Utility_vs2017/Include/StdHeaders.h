#pragma once

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cstdarg>
#include <cmath>

#include <memory>

// STL containers
#include <vector>
#include <stack>
#include <map>
#include <string>
#include <set>
#include <list>
#include <deque>
#include <queue>
#include <bitset>
#include <array>

#include <unordered_map>
#include <unordered_set>

// STL algorithms & functions
#include <algorithm>
#include <functional>
#include <limits>

// C++ Stream stuff
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>

#include <EnumClassHash.h>

namespace eg
{
	template <typename Key>
	using HashType = typename std::conditional<std::is_enum<Key>::value, EnumClassHash, std::hash<Key>>::type;

	template <typename T>
	using Deque = std::deque<T>;

	template <typename T>
	using Vector = std::vector<T>;

	template <typename T>
	using List = std::list<T>;

	template <typename T>
	using Stack = std::stack<T>;

	template <typename T>
	using Queue = std::queue<T>;

	template <typename T, typename P = std::less<T>>
	using Set = std::set<T, P>;

	template <typename K, typename V, typename P = std::less<K>>
	using Map = std::map<K, V, P>;

	template <typename K, typename V, typename P = std::less<K>>
	using MultiMap = std::multimap<K, V, P>;

	template <typename T, typename H = HashType<T>, typename C = std::equal_to<T>>
	using UnorderedSet = std::unordered_set<T, H, C>;

	template <typename K, typename V, typename H = HashType<T>, typename C = std::equal_to<T>>
	using UnorderedMap = std::unordered_map<K, V, H, C>;

	template <typename T>
	using SPtr = std::shared_ptr<T>;

	template <typename T>
	using UPtr = std::unique_ptr<T>;
}