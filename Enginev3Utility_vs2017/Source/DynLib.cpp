#include "DynLib.h"

#if EG_PLATFORM == EG_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#if !defined(NOMINMAX) && defined(_MSC_VER)
#define NOMINMAX
#endif
#include <Windows.h>
#endif

namespace eg
{
#if EG_PLATFORM == EG_PLATFORM_WIN32
	const char* DynLib::EXTENSION = "dll";
#endif

	DynLib::DynLib(const String& name) :
		m_name(name),
		m_hInst(nullptr)
	{
		load();
	}

	DynLib::~DynLib()
	{
	}

	void DynLib::load()
	{
		if (m_hInst)
		{
			return;
		}

		m_hInst = static_cast<DYNLIB_HANDLE>(DYNLIB_LOAD(m_name.c_str()));

		if (!m_hInst)
		{
			throw;
		}
	}

	void DynLib::unload()
	{
		if (!m_hInst)
		{
			return;
		}

		if (DYNLIB_UNLOAD(m_hInst))
		{
			throw;
		}
	}

	const String& DynLib::getName() const
	{
		return m_name;
	}

	void* DynLib::getSymbol(const String& strName) const
	{
		if (!m_hInst)
		{
			return nullptr;
		}

		return static_cast<void*>(DYNLIB_GETSYM(m_hInst, strName.c_str()));
	}

	String DynLib::dynLibError()
	{
#if EG_PLATFORM == EG_PLATFORM_WIN32
		LPVOID lpMsgBuf;
		FormatMessage
		(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_STRING |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			reinterpret_cast<LPTSTR>(&lpMsgBuf),
			0,
			NULL
		);
		String ret = static_cast<char*>(lpMsgBuf);
		LocalFree(lpMsgBuf);
		return ret;
#endif
	}

}