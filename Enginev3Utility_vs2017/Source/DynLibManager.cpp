#include "DynLibManager.h"
#include "DynLib.h"

namespace eg
{
	DynLibManager::DynLibManager()
	{
	}

	DynLibManager::~DynLibManager()
	{
		for (auto& entry : m_loadedLibraries)
		{
			entry.second->unload();
			delete entry.second;
		}
	}

	DynLib* DynLibManager::load(const String& name)
	{
		String filename = name;
		const UINT32 length = static_cast<UINT32>(filename.length());
		const String extension = String(".") + DynLib::EXTENSION;
		const UINT32 extLength = static_cast<UINT32>(extension.length());
		if (length <= extLength || filename.substr(length - extLength) != extension)
		{
			filename += extension;
		}

		auto iterFind = m_loadedLibraries.find(filename);
		if (iterFind != m_loadedLibraries.end())
		{
			return iterFind->second;
		}
		else
		{
			DynLib* newLib = new DynLib(filename);
			m_loadedLibraries[filename] = newLib;

			return newLib;
		}
	}

	void DynLibManager::unload(DynLib* lib)
	{
		auto iterFind = m_loadedLibraries.find(lib->getName());
		if (iterFind != m_loadedLibraries.end())
		{
			m_loadedLibraries.erase(iterFind);
		}

		lib->unload();
		delete lib;
	}

	DynLibManager& gDynLibManager()
	{
		return DynLibManager::instance();
	}
}