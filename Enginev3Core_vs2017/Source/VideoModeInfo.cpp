#include "VideoModeInfo.h"

namespace eg
{
	VideoMode::VideoMode(UINT32 width, UINT32 height, float refreshRate, UINT32 outputIdx) :
		m_width(width),
		m_height(height),
		m_refreshRate(refreshRate),
		m_outputIdx(outputIdx)
	{
	}

	bool VideoMode::operator==(const VideoMode& other) const
	{
		return m_width == other.m_width && m_height == other.m_height &&
			m_outputIdx == other.m_outputIdx && m_refreshRate == other.m_refreshRate;
	}
}