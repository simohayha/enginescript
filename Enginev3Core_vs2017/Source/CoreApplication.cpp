#include "CoreApplication.h"

#include "DynLib.h"
#include "DynLibManager.h"

namespace eg
{
	CoreApplication::CoreApplication(START_UP_DESC desc) :
		m_startUpDesc(desc)
	{
	}

	CoreApplication::~CoreApplication()
	{
	}

	void CoreApplication::runMainLoop()
	{
	}

	void CoreApplication::stopMainLoop()
	{
	}

	void CoreApplication::onStartUp()
	{
	}

	void CoreApplication::preUpdate()
	{
	}

	void CoreApplication::postUpdate()
	{
	}

	void CoreApplication::startUpRenderer()
	{
	}

	void* CoreApplication::loadPlugin(const String& pluginName, DynLib** library, void* passTrough)
	{
		DynLib* loadedLibrary = gDynLibManager().load(pluginName);
		if (library)
		{
			*library = loadedLibrary;
		}

		void* retVal = nullptr;
		if (loadedLibrary)
		{
			if (!passTrough)
			{
				typedef void* (*LoadPluginFunc)();

				LoadPluginFunc loadPluginFunc = static_cast<LoadPluginFunc>(loadedLibrary->getSymbol("loadPlugin"));

				if (loadPluginFunc)
				{
					retVal = loadPluginFunc();
				}
			}
			else
			{
				typedef void* (*LoadPluginFunc)(void*);

				LoadPluginFunc loadPluginFunc = static_cast<LoadPluginFunc>(loadedLibrary->getSymbol("loadPlugin"));

				if (loadPluginFunc)
				{
					retVal = loadPluginFunc(passTrough);
				}
			}

			UpdatePluginFunc updatePluginFunc = static_cast<UpdatePluginFunc>(loadedLibrary->getSymbol("updatePlugin"));

			if (updatePluginFunc)
			{
				m_pluginUpdateFunctions[loadedLibrary] = updatePluginFunc;
			}
		}

		return retVal;
	}

	void CoreApplication::unloadPlugin(DynLib* library)
	{
		typedef void(*UnloadPluginFunc)();

		UnloadPluginFunc unloadPluginFunc = static_cast<UnloadPluginFunc>(library->getSymbol("unloadPlugin"));

		if (unloadPluginFunc)
		{
			unloadPluginFunc();
		}

		m_pluginUpdateFunctions.erase(library);
		gDynLibManager().unload(library);
	}

	CoreApplication& gCoreApplication()
	{
		return CoreApplication::instance();
	}
}