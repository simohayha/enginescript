#pragma once

#include "CorePrequisites.h"

namespace eg
{
	class EG_CORE_EXPORT VideoMode
	{
	public:
		VideoMode() = default;
		VideoMode(UINT32 width, UINT32 height, float refreshRate = 60.0f, UINT32 outputIdx = 0u);
		virtual ~VideoMode() = default;

		bool operator==(const VideoMode& other) const;

		UINT32 getWidth() const
		{
			return m_width;
		}

		UINT32 getHeight() const
		{
			return m_height;
		}

		virtual float getRefreshRate() const
		{
			return m_refreshRate;
		}

		UINT32 getOutputIdx() const
		{
			return m_isCustom;
		}

	protected:
		UINT32 m_width = 1280u;
		UINT32 m_height = 720u;
		float m_refreshRate = 60.0f;
		UINT32 m_outputIdx = 0u;
		bool m_isCustom = true;
	};
}