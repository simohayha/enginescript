#pragma once

#include "CorePrequisites.h"
#include "VideoModeInfo.h"

namespace eg
{
	struct EG_CORE_EXPORT RENDER_WINDOW_DESC
	{
		RENDER_WINDOW_DESC() :
			fullscreen(false),
			vsync(false),
			vsyncInterval(1u),
			hidden(false),
			depthBuffer(true),
			multisampleCount(0u),
			multisampleHint(""),
			gamma(false),
			left(-1),
			top(-1),
			title(""),
			border(WindowBorder::Normal),
			outerDimensions(false),
			enableDoubleClick(true),
			toolWindow(false),
			modal(false),
			hideUntilSwap(false)
		{
		}

		VideoMode videoMode;
		bool fullscreen;
		bool vsync;
		UINT32 vsyncInterval;
		bool hidden;
		bool depthBuffer;
		UINT32 multisampleCount;
		String multisampleHint;
		bool gamma;
		INT32 left;
		INT32 top;
		String title;
		WindowBorder border;
		bool outerDimensions;
		bool enableDoubleClick;
		bool toolWindow;
		bool modal;
		bool hideUntilSwap;
	};
}