#pragma once

#include "CorePrequisites.h"
#include "Module.h"
#include "RenderWindow.h"

namespace eg
{
	struct START_UP_DESC
	{
		String renderAPI;
		String renderer;
		String physics;
		String audio;
		String input;
		bool scripting = false;

		RENDER_WINDOW_DESC primaryWindowDesc;

		Vector<String> importers;
	};

	class EG_CORE_EXPORT CoreApplication : public Module<CoreApplication>
	{
	public:
		CoreApplication(START_UP_DESC desc);
		virtual ~CoreApplication();

		void runMainLoop();

		void stopMainLoop();

	protected:
		void onStartUp() override;

		virtual void preUpdate();

		virtual void postUpdate();

		virtual void startUpRenderer();

		void* loadPlugin(const String& pluginName, DynLib** library = nullptr, void* passTrough = nullptr);

		void unloadPlugin(DynLib* library);

	protected:
		typedef void(*UpdatePluginFunc)();

		Map<DynLib*, UpdatePluginFunc> m_pluginUpdateFunctions;

		START_UP_DESC m_startUpDesc;
	};

	EG_CORE_EXPORT CoreApplication& gCoreApplication();
}